function init() {
	$.urlParam = function (name) {
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
		return (results !== null) ? results[1] || 0 : false;
	}
	$("#searchBtn").click(function() {
		$(".marked").removeClass("marked");
		var fields = $("#searchForm").serializeArray();
		var count = 0;
		$(".entry").each(function(){
			var entry = $(this);
			var visible = true;
			var fulltext = entry.data("fulltext")
			var body = entry.data("body")
		    $.each(fields, function(j, field) {
		    	var itemFieldValue = entry.data(field.name)
		    	var isFullText = fulltext && fulltext.indexOf(field.name) >= 0; 
		        if (field.value !== "" && itemFieldValue !== undefined 
		        	&& ((isFullText && (""+itemFieldValue).indexOf(field.value) < 0) || (!isFullText && itemFieldValue != field.value))) {
		        	visible = false;
		        }
		        
		        var isBody = body && body.indexOf(field.name) >= 0;
		        if (isBody && field.value !== "") {
					var textCnt = entry.find("a:contains('" + $('select[name="' + body + '"] option:selected').text().replace(/ \(\d+\)/g, "") + "')")
					if (!textCnt.length) {
						visible = false;
					} else {
						textCnt.addClass("marked");
					}
		        }
		    });
			if (visible) {
				entry.show();
				count++;
			} else {
				entry.hide();
			}
		});
		if (console && console.log) {
			console.log("COUNT: " + count);
		}
	});
	$("#resetBtn").click(function() {
		$("#searchForm").find("input, select").val("");
		$("#searchBtn").trigger("click");
	});
	initSearchForm();
}

function initSearchForm() {
	var filtered = false;
	var fields = $("#searchForm").serializeArray();
	$.each(fields, function(j, field) {
		var param = $.urlParam(field.name);
		if (param) {
			$("#searchForm :input[name='" + field.name + "']").val(param);
			filtered = true;
		}
	});
	if (filtered) {
		$("#searchBtn").trigger("click");
	}
}

function initTables(btnMaxMinLabel) {
	$(".head .right").append("<button class='btn_swap'>" + btnMaxMinLabel + "</button>");
	
	$("td.swap").each(function() {
		$(this).html("<div>"+$(this).html()+"</div>");
	});
	
	$("td.swap").click(function() {
		if ($(this).hasClass('limit')) {
			$(this).removeClass('limit')
		} else {
			$(this).addClass('limit')
		}
	});
	
	$("img.swap").click(function(e) {
		e.preventDefault();
		if ($(this).hasClass('small')) {
			$(this).removeClass('small')
		} else {
			$(this).addClass('small')
		}
	});

	var tbl = $('.datatable');
	$(".btn_swap").click(function() {
		var swapped = $(this).attr("swapped") == 'true';
		$("img.swap").each(function() {
			if (swapped) {
				$(this).removeClass('small')
			} else {
				$(this).addClass('small')
			}
		});
		$("td.swap").each(function() {
			if (swapped) {
				$(this).removeClass('limit')
			} else {
				$(this).addClass('limit')
			}
		});
		$(this).attr("swapped", !swapped);
	});
	
	var tfoot = $("<tfoot><tr></tr></tfoot>");
	tbl.find('thead').after(tfoot);
	var footer = tfoot.find("tr");
	tbl.find('thead th').each( function () {
        var title = $(this).text();
        var style = $(this).attr("style");
		footer.append('<td style="'+(!!style ? style : "")+'"><input type="text"/></td>');
    } );
	
	var fixedCols = tbl.data("fixed-cols") || 2;
	var table = tbl.DataTable({
        paging: false,
        autoWidth: false,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel'
        ],
		fixedHeader: {
			footer: false,
			header: true
		},
		fixedColumns: {
			left: fixedCols
		}
    });
    
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
}