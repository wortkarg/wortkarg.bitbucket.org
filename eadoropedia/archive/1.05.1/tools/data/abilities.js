abilityMap = {
	1: {
		id: 1,
		name: '�����',
		number: 1,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2: {
		id: 2,
		name: '�����',
		number: 2,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	3: {
		id: 3,
		name: '����������',
		number: 3,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	4: {
		id: 4,
		name: '������',
		number: 4,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	5: {
		id: 5,
		name: '������ �� ��������',
		number: 5,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	6: {
		id: 6,
		name: '�������������',
		number: 6,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	7: {
		id: 7,
		name: '��������',
		number: 7,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	8: {
		id: 8,
		name: '������������� �����',
		number: 8,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	9: {
		id: 9,
		name: '��������� ��������',
		number: 9,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	10: {
		id: 10,
		name: '����� ��������',
		number: 10,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	11: {
		id: 11,
		name: '������������',
		number: 11,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	12: {
		id: 12,
		name: '������ ���',
		number: 12,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	13: {
		id: 13,
		name: '�� ��������� ����',
		number: 13,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	14: {
		id: 14,
		name: '��������',
		number: 14,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	15: {
		id: 15,
		name: '�������������',
		number: 15,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	16: {
		id: 16,
		name: '������ ����',
		number: 16,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	17: {
		id: 17,
		name: '����������� �������',
		number: 17,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	18: {
		id: 18,
		name: '������������',
		number: 18,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	19: {
		id: 19,
		name: '��������������',
		number: 19,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	20: {
		id: 20,
		name: '�������������� �������',
		number: 20,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	21: {
		id: 21,
		name: '�������������� ���',
		number: 21,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	22: {
		id: 22,
		name: '���������',
		number: 22,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	23: {
		id: 23,
		name: '���� ��������',
		number: 23,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	24: {
		id: 24,
		name: '������������',
		number: 24,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	25: {
		id: 25,
		name: '�������',
		number: 25,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	26: {
		id: 26,
		name: '��������',
		number: 26,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	27: {
		id: 27,
		name: '���������� ����',
		number: 27,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	28: {
		id: 28,
		name: '���������� �������',
		number: 28,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	29: {
		id: 29,
		name: '����-������',
		number: 29,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	30: {
		id: 30,
		name: '��������� � �����������',
		number: 30,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	31: {
		id: 31,
		name: '������ ���',
		number: 31,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	32: {
		id: 32,
		name: '������ ����',
		number: 32,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	33: {
		id: 33,
		name: '������ ������',
		number: 33,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	34: {
		id: 34,
		name: '������ �����',
		number: 34,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	35: {
		id: 35,
		name: '������ ������',
		number: 35,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	36: {
		id: 36,
		name: '����������',
		number: 36,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	37: {
		id: 37,
		name: '����� � �������',
		number: 37,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	38: {
		id: 38,
		name: '�� ���������',
		number: 38,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	39: {
		id: 39,
		name: '����������� ����',
		number: 39,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	40: {
		id: 40,
		name: '���������� ����',
		number: 40,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	41: {
		id: 41,
		name: '�������� �����',
		number: 41,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	42: {
		id: 42,
		name: '��������� � ���',
		number: 42,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	43: {
		id: 43,
		name: '�����',
		number: 43,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	44: {
		id: 44,
		name: '����� ��������',
		number: 44,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	45: {
		id: 45,
		name: '����������� �����',
		number: 45,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	46: {
		id: 46,
		name: '����������',
		number: 46,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	47: {
		id: 47,
		name: '�������',
		number: 47,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	48: {
		id: 48,
		name: '�����������',
		number: 48,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	49: {
		id: 49,
		name: '�������',
		number: 49,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	50: {
		id: 50,
		name: '����������',
		number: 50,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	51: {
		id: 51,
		name: '��������� ����',
		number: 51,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	52: {
		id: 52,
		name: '����������� ������',
		number: 52,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	53: {
		id: 53,
		name: '�����������',
		number: 53,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	54: {
		id: 54,
		name: '�������',
		number: 54,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	55: {
		id: 55,
		name: '�����',
		number: 55,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	56: {
		id: 56,
		name: '�������',
		number: 56,
		numeric: true,
		effect: false,
		percent: true,
		unique: false
	}, 
	57: {
		id: 57,
		name: '��������� ���',
		number: 57,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	58: {
		id: 58,
		name: '���������',
		number: 58,
		numeric: true,
		effect: false,
		percent: true,
		unique: false
	}, 
	59: {
		id: 59,
		name: '����������� ����',
		number: 59,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	60: {
		id: 60,
		name: '�������� ������',
		number: 60,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	61: {
		id: 61,
		name: '���������� ���',
		number: 61,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	62: {
		id: 62,
		name: '����� ����',
		number: 62,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	63: {
		id: 63,
		name: '������������',
		number: 63,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	64: {
		id: 64,
		name: '���������� ������',
		number: 64,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	65: {
		id: 65,
		name: '����� �����',
		number: 65,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	66: {
		id: 66,
		name: '�������� �����',
		number: 66,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	67: {
		id: 67,
		name: '�����',
		number: 67,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	68: {
		id: 68,
		name: '����������',
		number: 68,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	69: {
		id: 69,
		name: '������ �������',
		number: 69,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	76: {
		id: 70,
		name: '������ ����',
		number: 76,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	77: {
		id: 71,
		name: '������ �������',
		number: 77,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	78: {
		id: 72,
		name: '�������� �������',
		number: 78,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	79: {
		id: 73,
		name: '�������� ����������',
		number: 79,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	80: {
		id: 74,
		name: '�������� �������',
		number: 80,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	81: {
		id: 75,
		name: '����� ������',
		number: 81,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	82: {
		id: 76,
		name: '����� ����������',
		number: 82,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	83: {
		id: 77,
		name: '�������� ����������',
		number: 83,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	84: {
		id: 78,
		name: '����� �����',
		number: 84,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	85: {
		id: 79,
		name: '����������� �����',
		number: 85,
		numeric: true,
		effect: false,
		percent: true,
		unique: false
	}, 
	88: {
		id: 80,
		name: '�� ������������',
		number: 88,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	905: {
		id: 81,
		name: '���� ����������',
		number: 905,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	906: {
		id: 82,
		name: '������������ ����������',
		number: 906,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	907: {
		id: 83,
		name: '���� �������',
		number: 907,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	908: {
		id: 84,
		name: '����������� �������������',
		number: 908,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	909: {
		id: 85,
		name: '���� ������� ������',
		number: 909,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	980: {
		id: 86,
		name: '������� ������',
		number: 980,
		numeric: false,
		effect: true,
		percent: false,
		unique: false
	}, 
	981: {
		id: 87,
		name: '��������',
		number: 981,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	982: {
		id: 88,
		name: '������������ ���������',
		number: 982,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	983: {
		id: 89,
		name: '�������� ���� ��������',
		number: 983,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	984: {
		id: 90,
		name: '������ ��������',
		number: 984,
		numeric: false,
		effect: true,
		percent: false,
		unique: false
	}, 
	985: {
		id: 91,
		name: '������ �� �������',
		number: 985,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	990: {
		id: 92,
		name: '���� �������',
		number: 990,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	991: {
		id: 93,
		name: '���� �����',
		number: 991,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	992: {
		id: 94,
		name: '���� �������',
		number: 992,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	993: {
		id: 95,
		name: '���������� ������',
		number: 993,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	994: {
		id: 96,
		name: '�������� ������',
		number: 994,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	995: {
		id: 97,
		name: '�������� ������ ���������',
		number: 995,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	996: {
		id: 98,
		name: '������������ ���������',
		number: 996,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	997: {
		id: 99,
		name: '���������� ����� �������',
		number: 997,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	998: {
		id: 100,
		name: '�����������',
		number: 998,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	999: {
		id: 101,
		name: '����������',
		number: 999,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	90: {
		id: 102,
		name: '���������',
		number: 90,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	103: {
		id: 103,
		name: '�������',
		number: 103,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	2010: {
		id: 104,
		name: '�������� "���������"',
		number: 2010,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2021: {
		id: 105,
		name: '�������� "����������"',
		number: 2021,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2023: {
		id: 106,
		name: '�������� "�������� ���"',
		number: 2023,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2011: {
		id: 107,
		name: '�������� "�������������"',
		number: 2011,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2038: {
		id: 108,
		name: '�������� "���������"',
		number: 2038,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2009: {
		id: 109,
		name: '�������� "��������� ���"',
		number: 2009,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2013: {
		id: 110,
		name: '�������� "�����"',
		number: 2013,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2014: {
		id: 111,
		name: '�������� "������� �������"',
		number: 2014,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2003: {
		id: 112,
		name: '�������� "�������������"',
		number: 2003,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2019: {
		id: 113,
		name: '�������� "������� ����"',
		number: 2019,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2016: {
		id: 114,
		name: '�������� "����� ����"',
		number: 2016,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2057: {
		id: 115,
		name: '�������� "��������� �����"',
		number: 2057,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	116: {
		id: 116,
		name: '������������',
		number: 116,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	117: {
		id: 117,
		name: '�����',
		number: 117,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	118: {
		id: 118,
		name: '��������',
		number: 118,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	119: {
		id: 119,
		name: '������� �����',
		number: 119,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	86: {
		id: 120,
		name: '������� �� �������',
		number: 86,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	87: {
		id: 121,
		name: '����� ���������',
		number: 87,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	2017: {
		id: 122,
		name: '�������� "���������� �������"',
		number: 2017,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2015: {
		id: 123,
		name: '�������� "������� �����"',
		number: 2015,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2008: {
		id: 124,
		name: '�������� "���������"',
		number: 2008,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2007: {
		id: 125,
		name: '�������� "����������"',
		number: 2007,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	94: {
		id: 126,
		name: '������ � ��������',
		number: 94,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	987: {
		id: 127,
		name: '����� ��������',
		number: 987,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	2037: {
		id: 128,
		name: '�������� "���������"',
		number: 2037,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2052: {
		id: 129,
		name: '�������� "����� ������"',
		number: 2052,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2049: {
		id: 130,
		name: '�������� "�����������"',
		number: 2049,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2056: {
		id: 131,
		name: '�������� "����� ���������"',
		number: 2056,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	95: {
		id: 132,
		name: '�������� �������',
		number: 95,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	96: {
		id: 133,
		name: '�������� �����',
		number: 96,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	97: {
		id: 134,
		name: '������������',
		number: 97,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	98: {
		id: 135,
		name: '����������',
		number: 98,
		numeric: false,
		effect: true,
		percent: false,
		unique: false
	}, 
	910: {
		id: 136,
		name: '��� ����������',
		number: 910,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	1019: {
		id: 1019,
		name: '������',
		number: 1019,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1020: {
		id: 1020,
		name: '�����',
		number: 1020,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	75: {
		id: 10075,
		name: '�������������� ���',
		number: 75,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1021: {
		id: 1021,
		name: '���',
		number: 1021,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	70: {
		id: 10070,
		name: '����������� ��� ����� ��������',
		number: 70,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1036: {
		id: 1036,
		name: '�����',
		number: 1036,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1039: {
		id: 1039,
		name: '��������',
		number: 1039,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1037: {
		id: 1037,
		name: '׸��',
		number: 1037,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1038: {
		id: 1038,
		name: '������ ������',
		number: 1038,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	93: {
		id: 10093,
		name: '������ ����������/������������. �������� ������������ ������� ���',
		number: 93,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	91: {
		id: 10091,
		name: 'Ҹ���� ����',
		number: 91,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1056: {
		id: 1056,
		name: '�����',
		number: 1056,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	89: {
		id: 10089,
		name: '���������� �����������',
		number: 89,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	73: {
		id: 10073,
		name: '����������',
		number: 73,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1055: {
		id: 1055,
		name: '�������',
		number: 1055,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	71: {
		id: 10071,
		name: '��������������� �������� �����������',
		number: 71,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	72: {
		id: 10072,
		name: '��������������� �������� ������������� �������',
		number: 72,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	74: {
		id: 10074,
		name: '������������',
		number: 74,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1064: {
		id: 1064,
		name: '������',
		number: 1064,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1065: {
		id: 1065,
		name: '������',
		number: 1065,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1060: {
		id: 1060,
		name: '������',
		number: 1060,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1013: {
		id: 1013,
		name: '������',
		number: 1013,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	92: {
		id: 10092,
		name: '����� ������ ����',
		number: 92,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1071: {
		id: 1071,
		name: '�������� �����',
		number: 1071,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1070: {
		id: 1070,
		name: '������',
		number: 1070,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}
}