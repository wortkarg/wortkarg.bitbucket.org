medalMap = {
	1: {
		id: 1,
		name: '������ �� ������',
		goldSpent: 3,
		gemSpent: 0,
		rarity: 0,
		effects: [
			{
				upgType: 12,
				power: 4
			}, 
			{
				upgType: 2,
				power: 1
			}
		]
	}, 
	2: {
		id: 2,
		name: '������ �� ���������',
		goldSpent: 4,
		gemSpent: 0,
		rarity: 1,
		effects: [
			{
				upgType: 1,
				power: 4
			}, 
			{
				upgType: 4,
				power: 1
			}, 
			{
				upgType: 3,
				power: 1
			}
		]
	}, 
	3: {
		id: 3,
		name: '����� ��������',
		goldSpent: 8,
		gemSpent: 0,
		rarity: 1,
		effects: [
			{
				upgType: 8,
				power: 1
			}, 
			{
				upgType: 77,
				power: 1
			}
		]
	}, 
	4: {
		id: 4,
		name: '����� �� ���� � ������',
		goldSpent: 5,
		gemSpent: 0,
		rarity: 1,
		effects: [
			{
				upgType: 2,
				power: 2
			}, 
			{
				upgType: 3,
				power: 2
			}
		]
	}, 
	5: {
		id: 5,
		name: '������ �� ������',
		goldSpent: 3,
		gemSpent: 0,
		rarity: 0,
		effects: [
			{
				upgType: 11,
				power: 3
			}, 
			{
				upgType: 3,
				power: 1
			}
		]
	}, 
	6: {
		id: 6,
		name: '���� �������',
		goldSpent: 0,
		gemSpent: 0,
		rarity: 0,
		effects: [
			{
				upgType: 85,
				power: 100
			}
		]
	}, 
	7: {
		id: 7,
		name: '������ ����',
		goldSpent: 5,
		gemSpent: 1,
		rarity: 1,
		effects: [
			{
				upgType: 95,
				power: 1
			}, 
			{
				upgType: 8,
				power: 1
			}
		]
	}, 
	8: {
		id: 8,
		name: '����� �����',
		goldSpent: 10,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 2,
				power: 2
			}, 
			{
				upgType: 3,
				power: 2
			}, 
			{
				upgType: 4,
				power: 1
			}, 
			{
				upgType: 5,
				power: 1
			}, 
			{
				upgType: 6,
				power: 1
			}
		]
	}, 
	9: {
		id: 9,
		name: '������ ��������',
		goldSpent: 3,
		gemSpent: 1,
		rarity: 1,
		effects: [
			{
				upgType: 10,
				power: 1
			}, 
			{
				upgType: 24,
				power: 2
			}
		]
	}, 
	10: {
		id: 10,
		name: '����� ����������',
		goldSpent: 10,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 2,
				power: 2
			}, 
			{
				upgType: 3,
				power: 1
			}, 
			{
				upgType: 12,
				power: 3
			}, 
			{
				upgType: 1,
				power: 5
			}
		]
	}, 
	11: {
		id: 11,
		name: '������ �� ��������',
		goldSpent: 3,
		gemSpent: 0,
		rarity: 0,
		effects: [
			{
				upgType: 5,
				power: 2
			}, 
			{
				upgType: 11,
				power: 2
			}
		]
	}, 
	12: {
		id: 12,
		name: '����� ���������',
		goldSpent: 4,
		gemSpent: 0,
		rarity: 0,
		effects: [
			{
				upgType: 4,
				power: 2
			}, 
			{
				upgType: 3,
				power: 1
			}
		]
	}, 
	13: {
		id: 13,
		name: '������ �� ���������������',
		goldSpent: 3,
		gemSpent: 0,
		rarity: 0,
		effects: [
			{
				upgType: 6,
				power: 2
			}, 
			{
				upgType: 12,
				power: 2
			}
		]
	}, 
	14: {
		id: 14,
		name: '���� �������',
		goldSpent: 4,
		gemSpent: 1,
		rarity: 1,
		effects: [
			{
				upgType: 10,
				power: 2
			}, 
			{
				upgType: 11,
				power: 2
			}
		]
	}, 
	15: {
		id: 15,
		name: '�������� ������',
		goldSpent: 7,
		gemSpent: 1,
		rarity: 2,
		effects: [
			{
				upgType: 10,
				power: 1
			}, 
			{
				upgType: 6,
				power: 1
			}, 
			{
				upgType: 60,
				power: 3
			}
		]
	}
}