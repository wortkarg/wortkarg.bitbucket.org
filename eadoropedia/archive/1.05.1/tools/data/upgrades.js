upgradeMap = {
	1: {
		id: 1,
		name: '����� +1',
		onlyOnce: false,
		upgType: 1,
		quantity: 1,
		components: [1],
		quantities: [1]
	},
	2: {
		id: 2,
		name: '����� +2',
		onlyOnce: false,
		upgType: 1,
		quantity: 2,
		components: [1],
		quantities: [2]
	},
	3: {
		id: 3,
		name: '����� +3',
		onlyOnce: false,
		upgType: 1,
		quantity: 3,
		components: [1],
		quantities: [3]
	},
	4: {
		id: 4,
		name: '����� +1',
		onlyOnce: false,
		upgType: 2,
		quantity: 1,
		components: [2],
		quantities: [1]
	},
	5: {
		id: 5,
		name: '����� +2',
		onlyOnce: false,
		upgType: 2,
		quantity: 2,
		components: [2],
		quantities: [2]
	},
	6: {
		id: 6,
		name: '����� +3',
		onlyOnce: false,
		upgType: 2,
		quantity: 3,
		components: [2],
		quantities: [3]
	},
	7: {
		id: 7,
		name: '���������� +1',
		onlyOnce: false,
		upgType: 3,
		quantity: 1,
		components: [3],
		quantities: [1]
	},
	8: {
		id: 8,
		name: '���������� +2',
		onlyOnce: false,
		upgType: 3,
		quantity: 2,
		components: [3],
		quantities: [2]
	},
	9: {
		id: 9,
		name: '���������� +3',
		onlyOnce: false,
		upgType: 3,
		quantity: 3,
		components: [3],
		quantities: [3]
	},
	10: {
		id: 10,
		name: '������ +1',
		onlyOnce: false,
		upgType: 4,
		quantity: 1,
		components: [4],
		quantities: [1]
	},
	11: {
		id: 11,
		name: '������ +2',
		onlyOnce: false,
		upgType: 4,
		quantity: 2,
		components: [4],
		quantities: [2]
	},
	12: {
		id: 12,
		name: '������ +3',
		onlyOnce: false,
		upgType: 4,
		quantity: 3,
		components: [4],
		quantities: [3]
	},
	13: {
		id: 13,
		name: '������ �� �������� +1',
		onlyOnce: false,
		upgType: 5,
		quantity: 1,
		components: [5],
		quantities: [1]
	},
	14: {
		id: 14,
		name: '������ �� �������� +2',
		onlyOnce: false,
		upgType: 5,
		quantity: 2,
		components: [5],
		quantities: [2]
	},
	15: {
		id: 15,
		name: '������ �� �������� +3',
		onlyOnce: false,
		upgType: 5,
		quantity: 3,
		components: [5],
		quantities: [3]
	},
	16: {
		id: 16,
		name: '������������� +1',
		onlyOnce: false,
		upgType: 6,
		quantity: 1,
		components: [6],
		quantities: [1]
	},
	17: {
		id: 17,
		name: '������������� +2',
		onlyOnce: false,
		upgType: 6,
		quantity: 2,
		components: [6],
		quantities: [2]
	},
	18: {
		id: 18,
		name: '������������� +3',
		onlyOnce: false,
		upgType: 6,
		quantity: 3,
		components: [6],
		quantities: [3]
	},
	19: {
		id: 19,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 7,
		quantity: 1,
		components: [7],
		quantities: [1]
	},
	20: {
		id: 20,
		name: '������������� ����� +1',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		components: [8],
		quantities: [1]
	},
	21: {
		id: 21,
		name: '������������� ����� +2',
		onlyOnce: false,
		upgType: 8,
		quantity: 2,
		components: [8],
		quantities: [2]
	},
	22: {
		id: 22,
		name: '������������� ����� +3',
		onlyOnce: false,
		upgType: 8,
		quantity: 3,
		components: [8],
		quantities: [3]
	},
	23: {
		id: 23,
		name: '��������� �������� +1',
		onlyOnce: false,
		upgType: 9,
		quantity: 1,
		components: [9],
		quantities: [1]
	},
	24: {
		id: 24,
		name: '��������� �������� +2',
		onlyOnce: false,
		upgType: 9,
		quantity: 2,
		components: [9],
		quantities: [2]
	},
	25: {
		id: 25,
		name: '����� +4',
		onlyOnce: false,
		upgType: 1,
		quantity: 4,
		components: [1],
		quantities: [4]
	},
	26: {
		id: 26,
		name: '������������ +1',
		onlyOnce: false,
		upgType: 24,
		quantity: 1,
		neededAbility: 24,
		components: [24],
		quantities: [1]
	},
	27: {
		id: 27,
		name: '����� �������� +1',
		onlyOnce: false,
		upgType: 10,
		quantity: 1,
		components: [10],
		quantities: [1]
	},
	28: {
		id: 28,
		name: '����� �������� +2',
		onlyOnce: false,
		upgType: 10,
		quantity: 2,
		components: [10],
		quantities: [2]
	},
	29: {
		id: 29,
		name: '����� �������� +3',
		onlyOnce: false,
		upgType: 10,
		quantity: 3,
		components: [10],
		quantities: [3]
	},
	30: {
		id: 30,
		name: '������������ +1',
		onlyOnce: false,
		upgType: 11,
		quantity: 1,
		components: [11],
		quantities: [1]
	},
	31: {
		id: 31,
		name: '������������ +2',
		onlyOnce: false,
		upgType: 11,
		quantity: 2,
		components: [11],
		quantities: [2]
	},
	32: {
		id: 32,
		name: '������������ +3',
		onlyOnce: false,
		upgType: 11,
		quantity: 3,
		components: [11],
		quantities: [3]
	},
	33: {
		id: 33,
		name: '������ ��� +1',
		onlyOnce: false,
		upgType: 12,
		quantity: 1,
		components: [12],
		quantities: [1]
	},
	34: {
		id: 34,
		name: '������ ��� +2',
		onlyOnce: false,
		upgType: 12,
		quantity: 2,
		components: [12],
		quantities: [2]
	},
	35: {
		id: 35,
		name: '������ ��� +3',
		onlyOnce: false,
		upgType: 12,
		quantity: 3,
		components: [12],
		quantities: [3]
	},
	36: {
		id: 36,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 1,
		quantity: 1,
		components: [1, 11],
		quantities: [1, 1]
	},
	37: {
		id: 37,
		name: '�������� +2',
		onlyOnce: false,
		upgType: 1,
		quantity: 2,
		components: [1, 11],
		quantities: [2, 2]
	},
	38: {
		id: 38,
		name: '���� +1',
		onlyOnce: false,
		upgType: 2,
		quantity: 1,
		components: [2, 3],
		quantities: [1, 1]
	},
	39: {
		id: 39,
		name: '���� +2',
		onlyOnce: false,
		upgType: 2,
		quantity: 2,
		components: [2, 3],
		quantities: [2, 2]
	},
	40: {
		id: 40,
		name: '����� +1',
		onlyOnce: false,
		upgType: 4,
		quantity: 1,
		components: [4, 5],
		quantities: [1, 1]
	},
	41: {
		id: 41,
		name: '����� +2',
		onlyOnce: false,
		upgType: 4,
		quantity: 2,
		components: [4, 5],
		quantities: [2, 2]
	},
	42: {
		id: 42,
		name: '���� ���� +1',
		onlyOnce: false,
		upgType: 6,
		quantity: 1,
		components: [6, 12],
		quantities: [1, 1]
	},
	43: {
		id: 43,
		name: '���� ���� +2',
		onlyOnce: false,
		upgType: 6,
		quantity: 2,
		components: [6, 12],
		quantities: [2, 2]
	},
	44: {
		id: 44,
		name: '��������',
		onlyOnce: false,
		upgType: 14,
		quantity: 1,
		components: [14],
		quantities: [1]
	},
	45: {
		id: 45,
		name: '�������������',
		onlyOnce: false,
		upgType: 15,
		quantity: 1,
		components: [15],
		quantities: [1]
	},
	46: {
		id: 46,
		name: '������ ����',
		onlyOnce: false,
		upgType: 16,
		quantity: 1,
		components: [16],
		quantities: [1]
	},
	47: {
		id: 47,
		name: '����������� �������',
		onlyOnce: false,
		upgType: 17,
		quantity: 1,
		components: [17],
		quantities: [1]
	},
	48: {
		id: 48,
		name: '������������',
		onlyOnce: false,
		upgType: 18,
		quantity: 1,
		components: [18],
		quantities: [1]
	},
	49: {
		id: 49,
		name: '��������������',
		onlyOnce: false,
		upgType: 19,
		quantity: 1,
		components: [19],
		quantities: [1]
	},
	50: {
		id: 50,
		name: '�������������� �������',
		onlyOnce: true,
		upgType: 20,
		quantity: 5,
		components: [20],
		quantities: [5]
	},
	51: {
		id: 51,
		name: '�������������� ��� +1',
		onlyOnce: false,
		upgType: 21,
		quantity: 1,
		components: [21],
		quantities: [1]
	},
	52: {
		id: 52,
		name: '��������� +1',
		onlyOnce: false,
		upgType: 22,
		quantity: 1,
		components: [22],
		quantities: [1]
	},
	53: {
		id: 53,
		name: '���� ��������',
		onlyOnce: true,
		upgType: 23,
		quantity: 3,
		components: [23],
		quantities: [3]
	},
	54: {
		id: 54,
		name: '������������ +3',
		onlyOnce: false,
		upgType: 24,
		quantity: 3,
		components: [24],
		quantities: [3]
	},
	55: {
		id: 55,
		name: '������� +2',
		onlyOnce: false,
		upgType: 25,
		quantity: 2,
		components: [25],
		quantities: [2]
	},
	56: {
		id: 56,
		name: '��������',
		onlyOnce: false,
		upgType: 26,
		quantity: 1,
		components: [26],
		quantities: [1]
	},
	57: {
		id: 57,
		name: '���������� ����',
		onlyOnce: false,
		upgType: 27,
		quantity: 1,
		components: [27],
		quantities: [1]
	},
	58: {
		id: 58,
		name: '���������� �������',
		onlyOnce: false,
		upgType: 28,
		quantity: 1,
		components: [28],
		quantities: [1]
	},
	59: {
		id: 59,
		name: '����-������',
		onlyOnce: true,
		upgType: 29,
		quantity: 3,
		components: [29],
		quantities: [3]
	},
	60: {
		id: 60,
		name: '��������� � �����������',
		onlyOnce: false,
		upgType: 30,
		quantity: 1,
		components: [30],
		quantities: [1]
	},
	61: {
		id: 61,
		name: '������� ������',
		onlyOnce: true,
		upgType: 13,
		quantity: 1,
		components: [13, 19, 18, 42],
		quantities: [1, 1, 1, 1]
	},
	62: {
		id: 62,
		name: '������ ���� (8)',
		onlyOnce: true,
		upgType: 32,
		quantity: 8,
		components: [32],
		quantities: [8]
	},
	63: {
		id: 63,
		name: '������ ������ (8)',
		onlyOnce: true,
		upgType: 33,
		quantity: 8,
		components: [33],
		quantities: [8]
	},
	64: {
		id: 64,
		name: '������ ����� (8)',
		onlyOnce: true,
		upgType: 34,
		quantity: 8,
		components: [34],
		quantities: [8]
	},
	65: {
		id: 65,
		name: '������ ������ +1',
		onlyOnce: false,
		upgType: 35,
		quantity: 1,
		components: [35],
		quantities: [1]
	},
	66: {
		id: 66,
		name: '������������ (4)',
		onlyOnce: true,
		upgType: 24,
		quantity: 4,
		components: [24],
		quantities: [4]
	},
	67: {
		id: 67,
		name: '����� � �������',
		onlyOnce: true,
		upgType: 37,
		quantity: 1,
		components: [37],
		quantities: [1]
	},
	68: {
		id: 68,
		name: '�� ���������',
		onlyOnce: true,
		upgType: 38,
		quantity: 1,
		components: [38],
		quantities: [1]
	},
	69: {
		id: 69,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 39,
		quantity: 1,
		components: [39],
		quantities: [1]
	},
	70: {
		id: 70,
		name: '���������� ���� +1',
		onlyOnce: false,
		upgType: 40,
		quantity: 1,
		components: [40],
		quantities: [1]
	},
	71: {
		id: 71,
		name: '�������� ����� +1',
		onlyOnce: false,
		upgType: 41,
		quantity: 1,
		components: [41],
		quantities: [1]
	},
	72: {
		id: 72,
		name: '��������� � ���',
		onlyOnce: true,
		upgType: 42,
		quantity: 1,
		components: [42],
		quantities: [1]
	},
	73: {
		id: 73,
		name: '����� (4)',
		onlyOnce: true,
		upgType: 43,
		quantity: 4,
		components: [43],
		quantities: [4]
	},
	74: {
		id: 74,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 44,
		quantity: 3,
		components: [44],
		quantities: [3]
	},
	75: {
		id: 75,
		name: '����������� �����',
		onlyOnce: true,
		upgType: 45,
		quantity: 3,
		components: [45],
		quantities: [3]
	},
	76: {
		id: 76,
		name: '���������� (4)',
		onlyOnce: true,
		upgType: 46,
		quantity: 4,
		components: [46],
		quantities: [4]
	},
	77: {
		id: 77,
		name: '�������',
		onlyOnce: true,
		upgType: 47,
		quantity: 6,
		components: [47],
		quantities: [6]
	},
	78: {
		id: 78,
		name: '�������� ���� (2)',
		onlyOnce: true,
		upgType: 41,
		quantity: 2,
		components: [41],
		quantities: [2]
	},
	79: {
		id: 79,
		name: '����������� (10)',
		onlyOnce: true,
		upgType: 49,
		quantity: 10,
		components: [49],
		quantities: [10]
	},
	80: {
		id: 80,
		name: '���������� +1',
		onlyOnce: false,
		upgType: 50,
		quantity: 1,
		components: [50],
		quantities: [1]
	},
	81: {
		id: 81,
		name: '��������� ����',
		onlyOnce: true,
		upgType: 51,
		quantity: 1,
		components: [51],
		quantities: [1]
	},
	82: {
		id: 82,
		name: '����������� ������',
		onlyOnce: true,
		upgType: 52,
		quantity: 3,
		components: [52],
		quantities: [3]
	},
	83: {
		id: 83,
		name: '����������� (2)',
		onlyOnce: true,
		upgType: 53,
		quantity: 2,
		components: [53],
		quantities: [2]
	},
	84: {
		id: 84,
		name: '�������',
		onlyOnce: false,
		upgType: 54,
		quantity: 4,
		components: [54],
		quantities: [4]
	},
	85: {
		id: 85,
		name: '����������� +2',
		onlyOnce: false,
		upgType: 53,
		quantity: 2,
		neededAbility: 53,
		components: [53],
		quantities: [2]
	},
	86: {
		id: 86,
		name: '������� 10',
		onlyOnce: false,
		upgType: 56,
		quantity: 10,
		components: [56],
		quantities: [10]
	},
	87: {
		id: 87,
		name: '������� 15',
		onlyOnce: false,
		upgType: 56,
		quantity: 15,
		components: [56],
		quantities: [15]
	},
	88: {
		id: 88,
		name: '��������� 10',
		onlyOnce: false,
		upgType: 58,
		quantity: 10,
		components: [58],
		quantities: [10]
	},
	89: {
		id: 89,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 59,
		quantity: 4,
		components: [59],
		quantities: [4]
	},
	90: {
		id: 90,
		name: '�������� ������ (2)',
		onlyOnce: true,
		upgType: 60,
		quantity: 2,
		components: [60],
		quantities: [2]
	},
	91: {
		id: 91,
		name: '�������� ������ +1',
		onlyOnce: false,
		upgType: 60,
		quantity: 1,
		neededAbility: 60,
		components: [60],
		quantities: [1]
	},
	92: {
		id: 92,
		name: '����� ����������',
		onlyOnce: true,
		upgType: 62,
		quantity: 3,
		components: [62],
		quantities: [3]
	},
	93: {
		id: 93,
		name: '������� +1',
		onlyOnce: false,
		upgType: 25,
		quantity: 1,
		neededAbility: 25,
		components: [25],
		quantities: [1]
	},
	94: {
		id: 94,
		name: '������������ (7)',
		onlyOnce: true,
		upgType: 24,
		quantity: 7,
		components: [24],
		quantities: [7]
	},
	95: {
		id: 95,
		name: '������� (5)',
		onlyOnce: true,
		upgType: 25,
		quantity: 5,
		components: [25],
		quantities: [5]
	},
	96: {
		id: 96,
		name: '������ ������ +3',
		onlyOnce: false,
		upgType: 35,
		quantity: 3,
		components: [35],
		quantities: [3]
	},
	97: {
		id: 97,
		name: '����������� (3)',
		onlyOnce: true,
		upgType: 48,
		quantity: 3,
		components: [48],
		quantities: [3]
	},
	98: {
		id: 98,
		name: '����������� +1',
		onlyOnce: false,
		upgType: 48,
		quantity: 1,
		components: [48],
		quantities: [1]
	},
	99: {
		id: 99,
		name: '����� (2)',
		onlyOnce: true,
		upgType: 55,
		quantity: 2,
		components: [55],
		quantities: [2]
	},
	100: {
		id: 100,
		name: '����� +1',
		onlyOnce: false,
		upgType: 55,
		quantity: 1,
		components: [55],
		quantities: [1]
	},
	101: {
		id: 101,
		name: '����� (3)',
		onlyOnce: true,
		upgType: 55,
		quantity: 3,
		components: [55],
		quantities: [3]
	},
	102: {
		id: 102,
		name: '��������� ���� (5)',
		onlyOnce: true,
		upgType: 57,
		quantity: 5,
		components: [57],
		quantities: [5]
	},
	103: {
		id: 103,
		name: '���������� ��� (1)',
		onlyOnce: true,
		upgType: 61,
		quantity: 1,
		components: [61],
		quantities: [1]
	},
	104: {
		id: 104,
		name: '������������+2',
		onlyOnce: true,
		upgType: 63,
		quantity: 2,
		components: [63],
		quantities: [2]
	},
	105: {
		id: 105,
		name: '������������ (15)',
		onlyOnce: true,
		upgType: 63,
		quantity: 15,
		components: [63],
		quantities: [15]
	},
	106: {
		id: 106,
		name: '�������� �������',
		onlyOnce: false,
		upgType: 80,
		quantity: 1,
		components: [80],
		quantities: [1]
	},
	107: {
		id: 107,
		name: '����� ���� ������',
		onlyOnce: true,
		upgType: 65,
		quantity: 1,
		components: [65],
		quantities: [1]
	},
	108: {
		id: 108,
		name: '�������� ����� (4)',
		onlyOnce: true,
		upgType: 66,
		quantity: 4,
		components: [66],
		quantities: [4]
	},
	109: {
		id: 109,
		name: '�����',
		onlyOnce: false,
		upgType: 67,
		quantity: 3,
		components: [67],
		quantities: [3]
	},
	110: {
		id: 110,
		name: '������ ������ +6',
		onlyOnce: false,
		upgType: 35,
		quantity: 6,
		components: [35],
		quantities: [6]
	},
	111: {
		id: 111,
		name: '������������ (10)',
		onlyOnce: true,
		upgType: 63,
		quantity: 10,
		components: [63],
		quantities: [10]
	},
	112: {
		id: 112,
		name: '���������� (50)',
		onlyOnce: true,
		upgType: 68,
		quantity: 50,
		components: [68],
		quantities: [50]
	},
	113: {
		id: 113,
		name: '���������� (3)',
		onlyOnce: true,
		upgType: 50,
		quantity: 3,
		components: [50],
		quantities: [3]
	},
	114: {
		id: 114,
		name: '������� ������� (3)',
		onlyOnce: true,
		upgType: 69,
		quantity: 3,
		components: [69],
		quantities: [3]
	},
	115: {
		id: 115,
		name: '����� ���������',
		onlyOnce: true,
		upgType: 2010,
		quantity: 1,
		components: [2010],
		quantities: [1]
	},
	116: {
		id: 116,
		name: '����� ����������',
		onlyOnce: true,
		upgType: 2021,
		quantity: 1,
		components: [2021],
		quantities: [1]
	},
	117: {
		id: 117,
		name: '�������� ���',
		onlyOnce: true,
		upgType: 2023,
		quantity: 4,
		components: [2023],
		quantities: [4]
	},
	118: {
		id: 118,
		name: '�������������',
		onlyOnce: true,
		upgType: 2011,
		quantity: 1,
		components: [2011],
		quantities: [1]
	},
	119: {
		id: 119,
		name: '���������',
		onlyOnce: true,
		upgType: 2038,
		quantity: 2,
		components: [2038],
		quantities: [2]
	},
	120: {
		id: 120,
		name: '��������� ���',
		onlyOnce: true,
		upgType: 2009,
		quantity: 1,
		components: [2009],
		quantities: [1]
	},
	121: {
		id: 121,
		name: '�����',
		onlyOnce: true,
		upgType: 2013,
		quantity: 1,
		components: [2013],
		quantities: [1]
	},
	122: {
		id: 122,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 2014,
		quantity: 2,
		components: [2014],
		quantities: [2]
	},
	123: {
		id: 123,
		name: '�������������',
		onlyOnce: true,
		upgType: 2003,
		quantity: 1,
		components: [2003],
		quantities: [1]
	},
	124: {
		id: 124,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2019,
		quantity: 1,
		components: [2019],
		quantities: [1]
	},
	125: {
		id: 125,
		name: '������ ���',
		onlyOnce: true,
		upgType: 2016,
		quantity: 1,
		components: [2016],
		quantities: [1]
	},
	126: {
		id: 126,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2057,
		quantity: 3,
		components: [2057],
		quantities: [3]
	},
	127: {
		id: 127,
		name: '������ ���� +1',
		onlyOnce: true,
		upgType: 76,
		quantity: 1,
		components: [76],
		quantities: [1]
	},
	128: {
		id: 128,
		name: '������ ������� +1',
		onlyOnce: true,
		upgType: 77,
		quantity: 1,
		components: [77],
		quantities: [1]
	},
	129: {
		id: 129,
		name: '������ ���� 2',
		onlyOnce: true,
		upgType: 76,
		quantity: 2,
		components: [76],
		quantities: [2]
	},
	130: {
		id: 130,
		name: '������ ������� 2',
		onlyOnce: true,
		upgType: 77,
		quantity: 2,
		components: [77],
		quantities: [2]
	},
	131: {
		id: 131,
		name: '��������� ���� +1',
		onlyOnce: false,
		upgType: 57,
		quantity: 1,
		neededAbility: 57,
		components: [57],
		quantities: [1]
	},
	132: {
		id: 132,
		name: '�������� ������� +1',
		onlyOnce: false,
		upgType: 78,
		quantity: 1,
		components: [78],
		quantities: [1]
	},
	133: {
		id: 133,
		name: '�������� ���� (4)',
		onlyOnce: true,
		upgType: 41,
		quantity: 4,
		components: [41],
		quantities: [4]
	},
	134: {
		id: 134,
		name: '���� ��������',
		onlyOnce: true,
		upgType: 23,
		quantity: 1,
		components: [23],
		quantities: [1]
	},
	135: {
		id: 135,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 2017,
		quantity: 2,
		components: [2017],
		quantities: [2]
	},
	136: {
		id: 136,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2015,
		quantity: 2,
		components: [2015],
		quantities: [2]
	},
	137: {
		id: 137,
		name: '���������� ������',
		onlyOnce: true,
		upgType: 64,
		quantity: 1,
		components: [64],
		quantities: [1]
	},
	138: {
		id: 138,
		name: '���������',
		onlyOnce: true,
		upgType: 2008,
		quantity: 2,
		components: [2008],
		quantities: [2]
	},
	139: {
		id: 139,
		name: '����������',
		onlyOnce: true,
		upgType: 2007,
		quantity: 1,
		components: [2007],
		quantities: [1]
	},
	140: {
		id: 140,
		name: '�� +1',
		onlyOnce: false,
		upgType: 41,
		quantity: 1,
		components: [41],
		quantities: [1]
	},
	141: {
		id: 141,
		name: '���������',
		onlyOnce: true,
		upgType: 2037,
		quantity: 3,
		components: [2037],
		quantities: [3]
	},
	142: {
		id: 142,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2052,
		quantity: 4,
		components: [2052],
		quantities: [4]
	},
	143: {
		id: 143,
		name: '�����������',
		onlyOnce: true,
		upgType: 2049,
		quantity: 4,
		components: [2049],
		quantities: [4]
	},
	144: {
		id: 144,
		name: '����� ���������',
		onlyOnce: true,
		upgType: 2056,
		quantity: 4,
		components: [2056],
		quantities: [4]
	},
	145: {
		id: 145,
		name: '���������� (6)',
		onlyOnce: true,
		upgType: 46,
		quantity: 6,
		components: [46],
		quantities: [6]
	},
	146: {
		id: 146,
		name: '���������� +1',
		onlyOnce: false,
		upgType: 46,
		quantity: 1,
		components: [46, 8],
		quantities: [1, 1]
	},
	147: {
		id: 147,
		name: '�������� ����� (2)',
		onlyOnce: true,
		upgType: 96,
		quantity: 2,
		components: [96],
		quantities: [2]
	},
	148: {
		id: 148,
		name: '�������� ����� +1',
		onlyOnce: false,
		upgType: 96,
		quantity: 1,
		components: [96],
		quantities: [1]
	},
	149: {
		id: 149,
		name: '�������� ������� (2)',
		onlyOnce: true,
		upgType: 95,
		quantity: 2,
		components: [95],
		quantities: [2]
	},
	150: {
		id: 150,
		name: '�������� ������� +1',
		onlyOnce: false,
		upgType: 95,
		quantity: 1,
		components: [95],
		quantities: [1]
	},
	151: {
		id: 151,
		name: '�������� ������� (2)',
		onlyOnce: true,
		upgType: 78,
		quantity: 2,
		components: [78],
		quantities: [2]
	},
	152: {
		id: 152,
		name: '������������',
		onlyOnce: true,
		upgType: 97,
		quantity: 1,
		components: [97],
		quantities: [1]
	}
}