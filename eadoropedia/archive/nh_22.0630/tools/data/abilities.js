abilityMap = {
	1: {
		id: 1,
		name: '�����',
		number: 1,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2: {
		id: 2,
		name: '�����',
		number: 2,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	3: {
		id: 3,
		name: '����������',
		number: 3,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	4: {
		id: 4,
		name: '������',
		number: 4,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	5: {
		id: 5,
		name: '������ �� ��������',
		number: 5,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	6: {
		id: 6,
		name: '�������������',
		number: 6,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	7: {
		id: 7,
		name: '��������',
		number: 7,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	8: {
		id: 8,
		name: '������������� �����',
		number: 8,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	9: {
		id: 9,
		name: '��������� ��������',
		number: 9,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	10: {
		id: 10,
		name: '����� ��������',
		number: 10,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	11: {
		id: 11,
		name: '������������',
		number: 11,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	12: {
		id: 12,
		name: '������ ���',
		number: 12,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	13: {
		id: 13,
		name: '�� ��������� ����',
		number: 13,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	14: {
		id: 14,
		name: '��������',
		number: 14,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	15: {
		id: 15,
		name: '�������������',
		number: 15,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	16: {
		id: 16,
		name: '������ ����',
		number: 16,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	31: {
		id: 17,
		name: '����������� �������',
		number: 31,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	18: {
		id: 18,
		name: '������������',
		number: 18,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	19: {
		id: 19,
		name: '��������������',
		number: 19,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	20: {
		id: 20,
		name: '�������������� �������',
		number: 20,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	21: {
		id: 21,
		name: '�������������� ���',
		number: 21,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	22: {
		id: 22,
		name: '���������',
		number: 22,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	23: {
		id: 23,
		name: '���� ��������',
		number: 23,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	24: {
		id: 24,
		name: '������������',
		number: 24,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	25: {
		id: 25,
		name: '������ �������',
		number: 25,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	26: {
		id: 26,
		name: '��������',
		number: 26,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	27: {
		id: 27,
		name: '���������� ����',
		number: 27,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	28: {
		id: 28,
		name: '���������� �������',
		number: 28,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	29: {
		id: 29,
		name: '����-������',
		number: 29,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	110: {
		id: 30,
		name: '��������� � �����������',
		number: 110,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	213: {
		id: 31,
		name: '���������� ����������',
		number: 213,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	32: {
		id: 32,
		name: '������ ����',
		number: 32,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	33: {
		id: 33,
		name: '������ ������',
		number: 33,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	34: {
		id: 34,
		name: '������ �����',
		number: 34,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	35: {
		id: 35,
		name: '������ ������',
		number: 35,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	36: {
		id: 36,
		name: '����������',
		number: 36,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	37: {
		id: 37,
		name: '����� � �������',
		number: 37,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	38: {
		id: 38,
		name: '�� ���������',
		number: 38,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	30: {
		id: 39,
		name: '����������� ����',
		number: 30,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	39: {
		id: 40,
		name: '���������� ����',
		number: 39,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	41: {
		id: 41,
		name: '�������� �����',
		number: 41,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	108: {
		id: 42,
		name: '��������� � ���',
		number: 108,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	96: {
		id: 43,
		name: '�����',
		number: 96,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	44: {
		id: 44,
		name: '����� ��������',
		number: 44,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	45: {
		id: 45,
		name: '����������� �����',
		number: 45,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	46: {
		id: 46,
		name: '����������',
		number: 46,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	47: {
		id: 47,
		name: '�������',
		number: 47,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	48: {
		id: 48,
		name: '�����������',
		number: 48,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	226: {
		id: 49,
		name: '�������',
		number: 226,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	50: {
		id: 50,
		name: '����������',
		number: 50,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	127: {
		id: 51,
		name: '��������� ����',
		number: 127,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	52: {
		id: 52,
		name: '����������� ������',
		number: 52,
		numeric: true,
		effect: true,
		percent: 0,
		unique: true
	}, 
	53: {
		id: 53,
		name: '�����������',
		number: 53,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	54: {
		id: 54,
		name: '�������',
		number: 54,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	55: {
		id: 55,
		name: '�����',
		number: 55,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	56: {
		id: 56,
		name: '������',
		number: 56,
		numeric: true,
		effect: false,
		percent: 1,
		unique: false
	}, 
	57: {
		id: 57,
		name: '��������� ���',
		number: 57,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	58: {
		id: 58,
		name: '���������',
		number: 58,
		numeric: true,
		effect: false,
		percent: 1,
		unique: false
	}, 
	59: {
		id: 59,
		name: '����������� ����',
		number: 59,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	60: {
		id: 60,
		name: '������������ ������',
		number: 60,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	120: {
		id: 61,
		name: '���������� ���',
		number: 120,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	62: {
		id: 62,
		name: '����� ����',
		number: 62,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	63: {
		id: 63,
		name: '������������',
		number: 63,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	64: {
		id: 64,
		name: '���������� ������',
		number: 64,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	65: {
		id: 65,
		name: '����� �����',
		number: 65,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	66: {
		id: 66,
		name: '�������� �����',
		number: 66,
		numeric: true,
		effect: true,
		percent: 0,
		unique: true
	}, 
	67: {
		id: 67,
		name: '�����',
		number: 67,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	68: {
		id: 68,
		name: '����������',
		number: 68,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	69: {
		id: 69,
		name: '������ �������',
		number: 69,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	76: {
		id: 70,
		name: '������ ����',
		number: 76,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	77: {
		id: 71,
		name: '������ �������',
		number: 77,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	42: {
		id: 72,
		name: '�������� �������',
		number: 42,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	179: {
		id: 73,
		name: '�������� ����������',
		number: 179,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	178: {
		id: 74,
		name: '�������� �������',
		number: 178,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	81: {
		id: 75,
		name: '����� ������',
		number: 81,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	82: {
		id: 76,
		name: '����� ����������',
		number: 82,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	83: {
		id: 77,
		name: '�������� ����������',
		number: 83,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	84: {
		id: 78,
		name: '����� ��������',
		number: 84,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	85: {
		id: 79,
		name: '����������� �����',
		number: 85,
		numeric: false,
		effect: false,
		percent: 1,
		unique: false
	}, 
	88: {
		id: 80,
		name: '�� ������������',
		number: 88,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	905: {
		id: 81,
		name: '���� ����������',
		number: 905,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	906: {
		id: 82,
		name: '������������ ����������',
		number: 906,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	907: {
		id: 83,
		name: '���� �������',
		number: 907,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	908: {
		id: 84,
		name: '����������� �������������',
		number: 908,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	909: {
		id: 85,
		name: '���� ������� ������',
		number: 909,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	980: {
		id: 86,
		name: '������� ������',
		number: 980,
		numeric: false,
		effect: true,
		percent: 0,
		unique: false
	}, 
	981: {
		id: 87,
		name: '��������',
		number: 981,
		numeric: true,
		effect: true,
		percent: 0,
		unique: true
	}, 
	982: {
		id: 88,
		name: '������������ ���������',
		number: 982,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	983: {
		id: 89,
		name: '�������� ���� ��������',
		number: 983,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	984: {
		id: 90,
		name: '������� ����������',
		number: 984,
		numeric: false,
		effect: true,
		percent: 0,
		unique: false
	}, 
	985: {
		id: 91,
		name: '������ �� �������',
		number: 985,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	990: {
		id: 92,
		name: '���� �������',
		number: 990,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	991: {
		id: 93,
		name: '���� �����',
		number: 991,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	992: {
		id: 94,
		name: '���� �������',
		number: 992,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	993: {
		id: 95,
		name: '���������� ������',
		number: 993,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	994: {
		id: 96,
		name: '�������� ������',
		number: 994,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	995: {
		id: 97,
		name: '�������� ������ ���������',
		number: 995,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	996: {
		id: 98,
		name: '������������ ���������',
		number: 996,
		numeric: true,
		effect: true,
		percent: 0,
		unique: true
	}, 
	997: {
		id: 99,
		name: '���������� ����� �������',
		number: 997,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	998: {
		id: 100,
		name: '�����������',
		number: 998,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	999: {
		id: 101,
		name: '����������',
		number: 999,
		numeric: true,
		effect: true,
		percent: 0,
		unique: true
	}, 
	90: {
		id: 102,
		name: '���������',
		number: 90,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	100: {
		id: 103,
		name: '�������',
		number: 100,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	2010: {
		id: 104,
		name: '�������� "�����"',
		number: 2010,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2021: {
		id: 105,
		name: '�������� "����������"',
		number: 2021,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2023: {
		id: 106,
		name: '�������� "�����"',
		number: 2023,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2011: {
		id: 107,
		name: '�������� "�������������"',
		number: 2011,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2038: {
		id: 108,
		name: '�������� "���������"',
		number: 2038,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2009: {
		id: 109,
		name: '�������� "��������� ���"',
		number: 2009,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2013: {
		id: 110,
		name: '�������� "�����"',
		number: 2013,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2014: {
		id: 111,
		name: '�������� "������� �������"',
		number: 2014,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2003: {
		id: 112,
		name: '�������� "�������������"',
		number: 2003,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2019: {
		id: 113,
		name: '�������� "������� ����"',
		number: 2019,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2016: {
		id: 114,
		name: '�������� "����� ����"',
		number: 2016,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2057: {
		id: 115,
		name: '�������� "��������� �����"',
		number: 2057,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	116: {
		id: 116,
		name: '������������',
		number: 116,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	117: {
		id: 117,
		name: '�����',
		number: 117,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	118: {
		id: 118,
		name: '��������',
		number: 118,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	119: {
		id: 119,
		name: '������� �����',
		number: 119,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	86: {
		id: 120,
		name: '������� �� �������',
		number: 86,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	87: {
		id: 121,
		name: '����� ���������',
		number: 87,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	2017: {
		id: 122,
		name: '�������� "���������� �������"',
		number: 2017,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2015: {
		id: 123,
		name: '�������� "������� �����"',
		number: 2015,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2008: {
		id: 124,
		name: '�������� "���������"',
		number: 2008,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2007: {
		id: 125,
		name: '�������� "��������� �����"',
		number: 2007,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	94: {
		id: 126,
		name: '������ � ��������',
		number: 94,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	987: {
		id: 127,
		name: '����� ��������',
		number: 987,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	2037: {
		id: 128,
		name: '�������� "���������"',
		number: 2037,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2052: {
		id: 129,
		name: '�������� "����� ������"',
		number: 2052,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2049: {
		id: 130,
		name: '�������� "�����������"',
		number: 2049,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2056: {
		id: 131,
		name: '�������� "����� ���������"',
		number: 2056,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	95: {
		id: 132,
		name: '�������� �������',
		number: 95,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	43: {
		id: 133,
		name: '�������� �����',
		number: 43,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	97: {
		id: 134,
		name: '������������',
		number: 97,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	98: {
		id: 135,
		name: '����������',
		number: 98,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	910: {
		id: 136,
		name: '��� ����������',
		number: 910,
		numeric: false,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2028: {
		id: 137,
		name: '�������� �����',
		number: 2028,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2055: {
		id: 138,
		name: '�������� ��������',
		number: 2055,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2012: {
		id: 139,
		name: '�������� "�������"',
		number: 2012,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2078: {
		id: 140,
		name: '���� "����"',
		number: 2078,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2079: {
		id: 141,
		name: '���� "����"',
		number: 2079,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2080: {
		id: 142,
		name: '���� "����"',
		number: 2080,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2081: {
		id: 143,
		name: '�������� "����������� ������"',
		number: 2081,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2082: {
		id: 144,
		name: '�������� "��������� ������"',
		number: 2082,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2083: {
		id: 145,
		name: '������ ���',
		number: 2083,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2084: {
		id: 146,
		name: '�������� "�������� ������"',
		number: 2084,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2085: {
		id: 147,
		name: '�������� "����������������"',
		number: 2085,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2086: {
		id: 148,
		name: '�������� "�������� �����"',
		number: 2086,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2087: {
		id: 149,
		name: '�������� "������ ������"',
		number: 2087,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2088: {
		id: 150,
		name: '�������� "������"',
		number: 2088,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2089: {
		id: 151,
		name: '�������� "����������"',
		number: 2089,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2090: {
		id: 152,
		name: '�������� "���������� �����"',
		number: 2090,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2091: {
		id: 153,
		name: '�������� "���������"',
		number: 2091,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	101: {
		id: 154,
		name: '��������� � ����',
		number: 101,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2092: {
		id: 155,
		name: '�������� "������ ������"',
		number: 2092,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2093: {
		id: 156,
		name: '�������� "��������� ���"',
		number: 2093,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2094: {
		id: 157,
		name: '�������� "�������� ��� ��"',
		number: 2094,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2095: {
		id: 158,
		name: '�������� "�������� ���� ��"',
		number: 2095,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2096: {
		id: 159,
		name: '�������� "���� �����"',
		number: 2096,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	140: {
		id: 160,
		name: '��������� �������',
		number: 140,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	141: {
		id: 161,
		name: '�������� �������',
		number: 141,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2099: {
		id: 162,
		name: '�������� "���� ����������"',
		number: 2099,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2100: {
		id: 163,
		name: '�������� "���� �������"',
		number: 2100,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2101: {
		id: 164,
		name: '�������� "���� ��������������"',
		number: 2101,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2102: {
		id: 165,
		name: '�������� "���� ������"',
		number: 2102,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2103: {
		id: 166,
		name: '�������� "�������� ����"',
		number: 2103,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2104: {
		id: 167,
		name: '�������� "������� �������"',
		number: 2104,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2105: {
		id: 168,
		name: '�������� "��� �����"',
		number: 2105,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2106: {
		id: 169,
		name: '�������� "�������� ������"',
		number: 2106,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2107: {
		id: 170,
		name: '�������� "������"',
		number: 2107,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2367: {
		id: 171,
		name: '�������� "��������� �����"',
		number: 2367,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2109: {
		id: 172,
		name: '�������� "����� ������"',
		number: 2109,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2110: {
		id: 173,
		name: '�������� "����� ����"',
		number: 2110,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2111: {
		id: 174,
		name: '�������� "����� �����"',
		number: 2111,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2112: {
		id: 175,
		name: '�������� "�������"',
		number: 2112,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2113: {
		id: 176,
		name: '�������� "����������"',
		number: 2113,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2114: {
		id: 177,
		name: '�������� "���������� �����"',
		number: 2114,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2115: {
		id: 178,
		name: '�������� "����"',
		number: 2115,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2116: {
		id: 179,
		name: '�������� "������"',
		number: 2116,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2117: {
		id: 180,
		name: '����� ���� - ���������',
		number: 2117,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2118: {
		id: 181,
		name: '"����� ���� - ���������',
		number: 2118,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2119: {
		id: 182,
		name: '����� ���� - ����',
		number: 2119,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2120: {
		id: 183,
		name: '�������� "������ ���� ����"',
		number: 2120,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2121: {
		id: 184,
		name: '�������� "������� ������"',
		number: 2121,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2122: {
		id: 185,
		name: '�������� "���������"',
		number: 2122,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2123: {
		id: 186,
		name: '�������� "������� �����"',
		number: 2123,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2124: {
		id: 187,
		name: '�������� "����������� �������"',
		number: 2124,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2125: {
		id: 188,
		name: '�������� "������ �����"',
		number: 2125,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2126: {
		id: 189,
		name: '�������� "������� �������"',
		number: 2126,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2127: {
		id: 190,
		name: '�������� "������������ ������"',
		number: 2127,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2128: {
		id: 191,
		name: '�������� "������ �������"',
		number: 2128,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2129: {
		id: 192,
		name: '�������� "��� �������"',
		number: 2129,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2130: {
		id: 193,
		name: '�������� "�����"',
		number: 2130,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2131: {
		id: 194,
		name: '�������� "����������"',
		number: 2131,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2132: {
		id: 195,
		name: '�������� "�����"',
		number: 2132,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2137: {
		id: 196,
		name: '�������� "��� �������"',
		number: 2137,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2133: {
		id: 197,
		name: '�������� "�������� ������"',
		number: 2133,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2370: {
		id: 198,
		name: '�������� "���������� ������"',
		number: 2370,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2371: {
		id: 199,
		name: '�������� "���������� �����"',
		number: 2371,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2372: {
		id: 200,
		name: '�������� "���������� ����"',
		number: 2372,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2134: {
		id: 201,
		name: '�������� "����� �����"',
		number: 2134,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2138: {
		id: 202,
		name: '�������� "�������������"',
		number: 2138,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2140: {
		id: 203,
		name: '�������� "���������"',
		number: 2140,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2039: {
		id: 204,
		name: '�������� "����� �����"',
		number: 2039,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2051: {
		id: 205,
		name: '�������� "����� �������"',
		number: 2051,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2050: {
		id: 206,
		name: '�������� "����� ������"',
		number: 2050,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2058: {
		id: 207,
		name: '�������� "����� �����"',
		number: 2058,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2141: {
		id: 208,
		name: '�������� "���������������"',
		number: 2141,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2142: {
		id: 209,
		name: '�������� "�������� ����"',
		number: 2142,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2143: {
		id: 210,
		name: '�������� "������ �������"',
		number: 2143,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2144: {
		id: 211,
		name: '�������� "���������"',
		number: 2144,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2145: {
		id: 212,
		name: '�������� "������������ ������"',
		number: 2145,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2146: {
		id: 213,
		name: '�������� "����� ����"',
		number: 2146,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2147: {
		id: 214,
		name: '�������� "�������� �����������"',
		number: 2147,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2148: {
		id: 215,
		name: '�������� "���������"',
		number: 2148,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2149: {
		id: 216,
		name: '�������� "������������ ������"',
		number: 2149,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2150: {
		id: 217,
		name: '�������� "������������ ����"',
		number: 2150,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2151: {
		id: 218,
		name: '�������� "������ �����"',
		number: 2151,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2152: {
		id: 219,
		name: '�������� "������������ �����"',
		number: 2152,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2153: {
		id: 220,
		name: '�������� "������ �������"',
		number: 2153,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2074: {
		id: 221,
		name: '�������� "���������� ���"',
		number: 2074,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2154: {
		id: 222,
		name: '�������� "����� �������"',
		number: 2154,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2155: {
		id: 223,
		name: '�������� "�������� �����"',
		number: 2155,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2156: {
		id: 224,
		name: '�������� "������� ������"',
		number: 2156,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2157: {
		id: 225,
		name: '�������� "������"',
		number: 2157,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	102: {
		id: 226,
		name: '��������� � ������',
		number: 102,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	103: {
		id: 227,
		name: '��������� � �������',
		number: 103,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	139: {
		id: 228,
		name: '�������������',
		number: 139,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	104: {
		id: 229,
		name: '��������� � ���������',
		number: 104,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2159: {
		id: 230,
		name: '�������� "�������� ����"',
		number: 2159,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2160: {
		id: 231,
		name: '�������� "���� ������"',
		number: 2160,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	49: {
		id: 232,
		name: '���� �����',
		number: 49,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2162: {
		id: 233,
		name: '�������� "��������������"',
		number: 2162,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2163: {
		id: 234,
		name: '�������� "������� �����"',
		number: 2163,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2164: {
		id: 235,
		name: '�������� "�������������"',
		number: 2164,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2165: {
		id: 236,
		name: '�������� "�������� �����"',
		number: 2165,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2166: {
		id: 237,
		name: '�������� "����� �������������"',
		number: 2166,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2167: {
		id: 238,
		name: '�������� "��������� �����"',
		number: 2167,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2168: {
		id: 239,
		name: '�������� "������ ���� �������"',
		number: 2168,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2169: {
		id: 240,
		name: '�������� "������ ���� �����"',
		number: 2169,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2170: {
		id: 241,
		name: '�������� "������ ���� ����"',
		number: 2170,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2171: {
		id: 242,
		name: '�������� "������� ����� ������"',
		number: 2171,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2172: {
		id: 243,
		name: '�������� "����� ������� ������"',
		number: 2172,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2173: {
		id: 244,
		name: '�������� "���� ���������"',
		number: 2173,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2035: {
		id: 245,
		name: '�������� "����� ������ ������"',
		number: 2035,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2174: {
		id: 246,
		name: '�������� "��������� � �������"',
		number: 2174,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2175: {
		id: 247,
		name: '�������� "��������� � �����"',
		number: 2175,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2176: {
		id: 248,
		name: '�������� "��������� � �����"',
		number: 2176,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2024: {
		id: 249,
		name: '�������� "�������� ������"',
		number: 2024,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2048: {
		id: 250,
		name: '�������� "����� ������ ���"',
		number: 2048,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2177: {
		id: 251,
		name: '�������� "������ �������"',
		number: 2177,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2178: {
		id: 252,
		name: '�������� "��������"',
		number: 2178,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2179: {
		id: 253,
		name: '�������� "�������� �����"',
		number: 2179,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2180: {
		id: 254,
		name: '�������� "������"',
		number: 2180,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2181: {
		id: 255,
		name: '�������� "������������ ������"',
		number: 2181,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2182: {
		id: 256,
		name: '�������� "��������� ����"',
		number: 2182,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2183: {
		id: 257,
		name: '�������� "������� �����"',
		number: 2183,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	260: {
		id: 258,
		name: '������� �����',
		number: 260,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	261: {
		id: 259,
		name: '���� �����',
		number: 261,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	262: {
		id: 260,
		name: '������������� ���',
		number: 262,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	263: {
		id: 261,
		name: '����� ���������',
		number: 263,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	264: {
		id: 262,
		name: '�������� ���',
		number: 264,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	265: {
		id: 263,
		name: '���-���',
		number: 265,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	266: {
		id: 264,
		name: '����������� ���������',
		number: 266,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	267: {
		id: 265,
		name: '������ ����',
		number: 267,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	2184: {
		id: 266,
		name: '�������� "������ �����"',
		number: 2184,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2185: {
		id: 267,
		name: '�������� "�������� ������"',
		number: 2185,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	142: {
		id: 268,
		name: '�����������',
		number: 142,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2187: {
		id: 269,
		name: '�������� "��������"',
		number: 2187,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2188: {
		id: 270,
		name: '�������� "���������"',
		number: 2188,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2189: {
		id: 271,
		name: '�������� "������� ����"',
		number: 2189,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2190: {
		id: 272,
		name: '�������� "����������� �������"',
		number: 2190,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2191: {
		id: 273,
		name: '�������� "����� ������"',
		number: 2191,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2047: {
		id: 274,
		name: '�������� "���������"',
		number: 2047,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2192: {
		id: 275,
		name: '�������� "�������"',
		number: 2192,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2193: {
		id: 276,
		name: '�������� "�������� ���"',
		number: 2193,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	314: {
		id: 277,
		name: '�������� "������� �����"',
		number: 314,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	315: {
		id: 278,
		name: '�������� "������� �����"',
		number: 315,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2196: {
		id: 279,
		name: '�������� "�������� �����������"',
		number: 2196,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2197: {
		id: 280,
		name: '�������� "�������� ����"',
		number: 2197,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2337: {
		id: 281,
		name: '�������� "������ ��������"',
		number: 2337,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2098: {
		id: 282,
		name: '�������� "������ ����"',
		number: 2098,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2198: {
		id: 283,
		name: '�������� "������� ����"',
		number: 2198,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	300: {
		id: 284,
		name: '�������� "���������� �������"',
		number: 300,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2200: {
		id: 285,
		name: '�������� "���� ����"',
		number: 2200,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2097: {
		id: 286,
		name: '�������� "���������� ���"',
		number: 2097,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2201: {
		id: 287,
		name: '�������� "Ҹ���� �����������"',
		number: 2201,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2207: {
		id: 288,
		name: '�������� "Ҹ���� �����������"',
		number: 2207,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2208: {
		id: 289,
		name: '�������� "������ ����"',
		number: 2208,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2203: {
		id: 290,
		name: '�������� "�������� �����"',
		number: 2203,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2210: {
		id: 291,
		name: '�������� "��������� ������"',
		number: 2210,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2211: {
		id: 292,
		name: '"�������������� ������"',
		number: 2211,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2213: {
		id: 293,
		name: '�������� "������ �������"',
		number: 2213,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2224: {
		id: 294,
		name: '�������� "���������� �����"',
		number: 2224,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2226: {
		id: 295,
		name: '���� "�����"',
		number: 2226,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2227: {
		id: 296,
		name: '���� "�����"',
		number: 2227,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2204: {
		id: 297,
		name: '�������� "�������� �����"',
		number: 2204,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2205: {
		id: 298,
		name: '�������� "�������� �������"',
		number: 2205,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2061: {
		id: 299,
		name: '�������� "�������� �������"',
		number: 2061,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2214: {
		id: 300,
		name: '�������� "���� ������"',
		number: 2214,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2229: {
		id: 301,
		name: '�������� "�������� �������"',
		number: 2229,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2206: {
		id: 302,
		name: '�������� "�������� ������"',
		number: 2206,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2230: {
		id: 303,
		name: '�������� "����������� ������"',
		number: 2230,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2231: {
		id: 304,
		name: '�������� "�������� ��������"',
		number: 2231,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2034: {
		id: 305,
		name: '�������� "������� �����"',
		number: 2034,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2045: {
		id: 306,
		name: '�������� "������� ������"',
		number: 2045,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2232: {
		id: 307,
		name: '�������� "����� �������"',
		number: 2232,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2233: {
		id: 308,
		name: '�������� "����� �������������"',
		number: 2233,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2234: {
		id: 309,
		name: '�������� "�������� ���"',
		number: 2234,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2069: {
		id: 310,
		name: '�������� "�����"',
		number: 2069,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2043: {
		id: 311,
		name: '�������� "Ҹ���� ����"',
		number: 2043,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	165: {
		id: 312,
		name: '������� ������',
		number: 165,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2237: {
		id: 313,
		name: '�������� "��������� �����"',
		number: 2237,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2238: {
		id: 314,
		name: '�������� "������� �����"',
		number: 2238,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2244: {
		id: 315,
		name: '�������� "����������"',
		number: 2244,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2245: {
		id: 316,
		name: '�������� "������ �������"',
		number: 2245,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2252: {
		id: 317,
		name: '�������� "�����������"',
		number: 2252,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2253: {
		id: 318,
		name: '�������� "Ҹ���� �����"',
		number: 2253,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	166: {
		id: 319,
		name: '����������',
		number: 166,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2254: {
		id: 320,
		name: '�������� "������� ����"',
		number: 2254,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2255: {
		id: 321,
		name: '�������� "������� ������"',
		number: 2255,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2256: {
		id: 322,
		name: '�������� "���������� �����"',
		number: 2256,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2257: {
		id: 323,
		name: '�������� "��������� �����"',
		number: 2257,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2258: {
		id: 324,
		name: '�������� "���� ����������"',
		number: 2258,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2261: {
		id: 325,
		name: '������������ ���',
		number: 2261,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2356: {
		id: 326,
		name: '������������ ���',
		number: 2356,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	122: {
		id: 327,
		name: '�������� �������',
		number: 122,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	125: {
		id: 328,
		name: '�������� ������',
		number: 125,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	210: {
		id: 329,
		name: '�������� ������',
		number: 210,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2355: {
		id: 330,
		name: '�������� "�������� ����"',
		number: 2355,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	158: {
		id: 331,
		name: '���������� �������',
		number: 158,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	205: {
		id: 332,
		name: '������ ��������',
		number: 205,
		numeric: false,
		effect: true,
		percent: 1,
		unique: true
	}, 
	2354: {
		id: 333,
		name: '�������� "������� ������������"',
		number: 2354,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2352: {
		id: 334,
		name: '�������� "����� ������"',
		number: 2352,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2353: {
		id: 335,
		name: '�������� "������ ������"',
		number: 2353,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	346: {
		id: 336,
		name: '������ ������',
		number: 346,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	347: {
		id: 337,
		name: '������ �����',
		number: 347,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	348: {
		id: 338,
		name: '������ �������',
		number: 348,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2042: {
		id: 339,
		name: '�������� "����� ����������"',
		number: 2042,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	215: {
		id: 340,
		name: '���� �����',
		number: 215,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2064: {
		id: 341,
		name: '�������� "����������"',
		number: 2064,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	206: {
		id: 342,
		name: '����������',
		number: 206,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	305: {
		id: 343,
		name: '������� �����',
		number: 305,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	306: {
		id: 344,
		name: '������� �����',
		number: 306,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	307: {
		id: 345,
		name: '������� �����',
		number: 307,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	308: {
		id: 346,
		name: '������� �����',
		number: 308,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	349: {
		id: 347,
		name: '������ �������',
		number: 349,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	350: {
		id: 348,
		name: '������ ��������',
		number: 350,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	351: {
		id: 349,
		name: '������ �������',
		number: 351,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	352: {
		id: 350,
		name: '������ ��������',
		number: 352,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	353: {
		id: 351,
		name: '������ �������',
		number: 353,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	354: {
		id: 352,
		name: '������ ������',
		number: 354,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	177: {
		id: 353,
		name: '�������� ���������',
		number: 177,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	136: {
		id: 354,
		name: '������� ����',
		number: 136,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	137: {
		id: 355,
		name: '�������� �������',
		number: 137,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	143: {
		id: 356,
		name: '�������� ������',
		number: 143,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	144: {
		id: 357,
		name: '�������� ������',
		number: 144,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	146: {
		id: 358,
		name: '����������',
		number: 146,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	109: {
		id: 359,
		name: '��������� � ������������',
		number: 109,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	78: {
		id: 360,
		name: '����������� �������',
		number: 78,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	79: {
		id: 361,
		name: '������ �������',
		number: 79,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	187: {
		id: 362,
		name: '������ ������',
		number: 187,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	147: {
		id: 363,
		name: '�������� ������',
		number: 147,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	150: {
		id: 364,
		name: '�� �������',
		number: 150,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	161: {
		id: 365,
		name: '������ �����������',
		number: 161,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	2158: {
		id: 366,
		name: '�������� "������"',
		number: 2158,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	235: {
		id: 367,
		name: '������� ����',
		number: 235,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	236: {
		id: 368,
		name: '����� ����',
		number: 236,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2288: {
		id: 369,
		name: '�������� "����� ����"',
		number: 2288,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	157: {
		id: 370,
		name: '������ ����',
		number: 157,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2348: {
		id: 371,
		name: '�������� "���������� ������"',
		number: 2348,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2269: {
		id: 372,
		name: '�������� "�������� �����"',
		number: 2269,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2270: {
		id: 373,
		name: '�������� "����"',
		number: 2270,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	124: {
		id: 374,
		name: '������',
		number: 124,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2273: {
		id: 375,
		name: '�������� "�����������"',
		number: 2273,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2260: {
		id: 376,
		name: '�������� "��������� ����"',
		number: 2260,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2274: {
		id: 377,
		name: '�������� "��������"',
		number: 2274,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2275: {
		id: 378,
		name: '�������� "�������� �����"',
		number: 2275,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2276: {
		id: 379,
		name: '�������� "�������� �������"',
		number: 2276,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	167: {
		id: 380,
		name: '���������',
		number: 167,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	2279: {
		id: 381,
		name: '�������� "������������� ����"',
		number: 2279,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2280: {
		id: 382,
		name: '�������� "����� �����"',
		number: 2280,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2281: {
		id: 383,
		name: '�������� "����� �������"',
		number: 2281,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2282: {
		id: 384,
		name: '�������� "����� �������"',
		number: 2282,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2283: {
		id: 385,
		name: '�������� "���������"',
		number: 2283,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2278: {
		id: 386,
		name: '�������� "������� ����"',
		number: 2278,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2284: {
		id: 387,
		name: '�������� "���������� ����"',
		number: 2284,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	186: {
		id: 388,
		name: '���� �����',
		number: 186,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2287: {
		id: 389,
		name: '�������� "������"',
		number: 2287,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	154: {
		id: 390,
		name: '������� ������',
		number: 154,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	176: {
		id: 391,
		name: '�������� �������',
		number: 176,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	355: {
		id: 392,
		name: '"���������� �����"',
		number: 355,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2291: {
		id: 393,
		name: '"���������� ���������"',
		number: 2291,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2292: {
		id: 394,
		name: '"���������� �������"',
		number: 2292,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2293: {
		id: 395,
		name: '"���������� �������"',
		number: 2293,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2294: {
		id: 396,
		name: '�������� "�������� �����"',
		number: 2294,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2295: {
		id: 397,
		name: '"��������� ���������"',
		number: 2295,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2247: {
		id: 398,
		name: '�������� "������ ����"',
		number: 2247,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2296: {
		id: 399,
		name: '"��������� �������"',
		number: 2296,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2297: {
		id: 400,
		name: '"��������� �������"',
		number: 2297,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2298: {
		id: 401,
		name: '�������� "���������"',
		number: 2298,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	184: {
		id: 402,
		name: '������������',
		number: 184,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	123: {
		id: 403,
		name: 'Ҹ���� �����������',
		number: 123,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	202: {
		id: 404,
		name: '������� �� ������',
		number: 202,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	203: {
		id: 405,
		name: '������� �� �������',
		number: 203,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	303: {
		id: 406,
		name: '�������� "�������� �����"',
		number: 303,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	317: {
		id: 407,
		name: '�������� "������� �����"',
		number: 317,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2062: {
		id: 408,
		name: '�������� "���"',
		number: 2062,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	204: {
		id: 409,
		name: '������',
		number: 204,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	318: {
		id: 410,
		name: '�������� "������� �����"',
		number: 318,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2301: {
		id: 411,
		name: '�������� "������ ����"',
		number: 2301,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2302: {
		id: 412,
		name: '�������� "�������� �����"',
		number: 2302,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	121: {
		id: 413,
		name: 'Ҹ���� �������������',
		number: 121,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	105: {
		id: 414,
		name: '��������� � �������',
		number: 105,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	106: {
		id: 415,
		name: '��������� � ����������',
		number: 106,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	107: {
		id: 416,
		name: '��������� � ���������',
		number: 107,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2303: {
		id: 417,
		name: '�������� "������� ����"',
		number: 2303,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2304: {
		id: 418,
		name: '�������� "������� �������"',
		number: 2304,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2305: {
		id: 419,
		name: '�������� "������� �����"',
		number: 2305,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2306: {
		id: 420,
		name: '�������� "����� ������ ������"',
		number: 2306,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2307: {
		id: 421,
		name: '�������� "������� �������"',
		number: 2307,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2308: {
		id: 422,
		name: '�������� "����� �������"',
		number: 2308,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2309: {
		id: 423,
		name: '�������� "������� �������"',
		number: 2309,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2310: {
		id: 424,
		name: '�������� "������� ������"',
		number: 2310,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2311: {
		id: 425,
		name: '�������� "������� �����������"',
		number: 2311,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2312: {
		id: 426,
		name: '�������� "������� �������"',
		number: 2312,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	301: {
		id: 427,
		name: '�������� "׸���� �����"',
		number: 301,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2272: {
		id: 428,
		name: '�������� "�������� ����"',
		number: 2272,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	80: {
		id: 429,
		name: '�����������',
		number: 80,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	312: {
		id: 430,
		name: '�������� "������� �����"',
		number: 312,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2299: {
		id: 431,
		name: '�������� "�����"',
		number: 2299,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2317: {
		id: 432,
		name: '�������� "������"',
		number: 2317,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2318: {
		id: 433,
		name: '�������� "������� � �����"',
		number: 2318,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2319: {
		id: 434,
		name: '�������� "������ �����"',
		number: 2319,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2320: {
		id: 435,
		name: '�������� "������ ������"',
		number: 2320,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2321: {
		id: 436,
		name: '�������� "������ ������"',
		number: 2321,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	304: {
		id: 437,
		name: '�������� "����� �����"',
		number: 304,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	180: {
		id: 438,
		name: '��������� �������',
		number: 180,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	181: {
		id: 439,
		name: '��������� �����',
		number: 181,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	182: {
		id: 440,
		name: '���������� �����',
		number: 182,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	191: {
		id: 441,
		name: '�����������',
		number: 191,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	319: {
		id: 442,
		name: '�������� "������� �����"',
		number: 319,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2324: {
		id: 443,
		name: '�������� "�����-�������"',
		number: 2324,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2325: {
		id: 444,
		name: '�������� "����� �����"',
		number: 2325,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	302: {
		id: 445,
		name: '������ ����',
		number: 302,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2326: {
		id: 446,
		name: '�������� "���������� ���������"',
		number: 2326,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2327: {
		id: 447,
		name: '�������� "���������� ��������"',
		number: 2327,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2328: {
		id: 448,
		name: '�������� "���������� ���������"',
		number: 2328,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2329: {
		id: 449,
		name: '�������� "���������� ��������"',
		number: 2329,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2330: {
		id: 450,
		name: '�������� "������ ����"',
		number: 2330,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2331: {
		id: 451,
		name: '�������� "����� �����������"',
		number: 2331,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2332: {
		id: 452,
		name: '�������� "����� ���������"',
		number: 2332,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2333: {
		id: 453,
		name: '�������� "���������� ����"',
		number: 2333,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	198: {
		id: 454,
		name: '���������',
		number: 198,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	128: {
		id: 455,
		name: '��������� �������',
		number: 128,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	196: {
		id: 456,
		name: '���������� ����',
		number: 196,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	195: {
		id: 457,
		name: '������ ���',
		number: 195,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	185: {
		id: 458,
		name: '������ �������',
		number: 185,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	168: {
		id: 459,
		name: '����������',
		number: 168,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	40: {
		id: 460,
		name: '���������� �������',
		number: 40,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	51: {
		id: 461,
		name: '����������� �������',
		number: 51,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	155: {
		id: 462,
		name: '������ ������',
		number: 155,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	156: {
		id: 463,
		name: '������ ������',
		number: 156,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	61: {
		id: 464,
		name: '������������ ������',
		number: 61,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	197: {
		id: 465,
		name: '����� �����',
		number: 197,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	134: {
		id: 466,
		name: '����������� ����',
		number: 134,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	135: {
		id: 467,
		name: '���������� �������',
		number: 135,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	126: {
		id: 468,
		name: '������������',
		number: 126,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	979: {
		id: 469,
		name: '�������� ��������',
		number: 979,
		numeric: true,
		effect: true,
		percent: 0,
		unique: true
	}, 
	313: {
		id: 470,
		name: '�������� "�������� �����"',
		number: 313,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2242: {
		id: 471,
		name: '�������� "�������� ������� �����"',
		number: 2242,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	131: {
		id: 472,
		name: '������� ������',
		number: 131,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	132: {
		id: 473,
		name: '������� �������',
		number: 132,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	2059: {
		id: 474,
		name: '�������� "������������"',
		number: 2059,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	201: {
		id: 475,
		name: '����������',
		number: 201,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	2338: {
		id: 476,
		name: '�������� "Ҹ���� ����������"',
		number: 2338,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2339: {
		id: 477,
		name: '�������� "����� ����"',
		number: 2339,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2340: {
		id: 478,
		name: '�������� "����� ����"',
		number: 2340,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2341: {
		id: 479,
		name: '�������� "������"',
		number: 2341,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	316: {
		id: 480,
		name: '�������� "������� �����"',
		number: 316,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2343: {
		id: 481,
		name: '�������� "�������� ����"',
		number: 2343,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2346: {
		id: 482,
		name: '�������� "����� �����"',
		number: 2346,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	17: {
		id: 483,
		name: '����� �����',
		number: 17,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	217: {
		id: 484,
		name: '��������',
		number: 217,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	218: {
		id: 485,
		name: '���� �����',
		number: 218,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	320: {
		id: 486,
		name: '�������� ����� �����',
		number: 320,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	321: {
		id: 487,
		name: '�������� ����� �����',
		number: 321,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	322: {
		id: 488,
		name: '�������� ����� �����',
		number: 322,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	323: {
		id: 489,
		name: '�������� �����',
		number: 323,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	324: {
		id: 490,
		name: '�������� �����',
		number: 324,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	325: {
		id: 491,
		name: '�������� �����',
		number: 325,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	326: {
		id: 492,
		name: '�������� ���������',
		number: 326,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	327: {
		id: 493,
		name: '�������� ���������',
		number: 327,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	328: {
		id: 494,
		name: '�������� ���������',
		number: 328,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2363: {
		id: 495,
		name: '�������� "����� �����"',
		number: 2363,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2364: {
		id: 496,
		name: '�������� "����� ������"',
		number: 2364,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2365: {
		id: 497,
		name: '�������� "����� ����"',
		number: 2365,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2366: {
		id: 498,
		name: '�������� "����� �����"',
		number: 2366,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2368: {
		id: 499,
		name: '�������� "���� �����"',
		number: 2368,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	911: {
		id: 500,
		name: '��������� �������',
		number: 911,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2369: {
		id: 501,
		name: '�������� "������ ���� ����"',
		number: 2369,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	335: {
		id: 502,
		name: '�������� ����� �����',
		number: 335,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	169: {
		id: 503,
		name: '���������������',
		number: 169,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	3030: {
		id: 504,
		name: '�������� �����',
		number: 3030,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	214: {
		id: 505,
		name: '���������',
		number: 214,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	336: {
		id: 506,
		name: '����������� ����',
		number: 336,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	337: {
		id: 507,
		name: '����������� ����',
		number: 337,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	338: {
		id: 508,
		name: '����������� ����',
		number: 338,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	339: {
		id: 509,
		name: '����������� ����',
		number: 339,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	232: {
		id: 510,
		name: '����� ����',
		number: 232,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	216: {
		id: 511,
		name: '������',
		number: 216,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	340: {
		id: 512,
		name: '������',
		number: 340,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	341: {
		id: 513,
		name: '�����',
		number: 341,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	342: {
		id: 514,
		name: '���� �������',
		number: 342,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	249: {
		id: 515,
		name: '���',
		number: 249,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	2139: {
		id: 516,
		name: '�������� "��������"',
		number: 2139,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2373: {
		id: 517,
		name: '�������� "��������� �����"',
		number: 2373,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	240: {
		id: 518,
		name: '���� � �������',
		number: 240,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	228: {
		id: 519,
		name: '���� � �����',
		number: 228,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	2378: {
		id: 520,
		name: '�������� "���� ����"',
		number: 2378,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	343: {
		id: 521,
		name: '����������',
		number: 343,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	344: {
		id: 522,
		name: '�������� ���������',
		number: 344,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	345: {
		id: 523,
		name: '�������� �������',
		number: 345,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	227: {
		id: 524,
		name: '����������� ��������',
		number: 227,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	221: {
		id: 525,
		name: '������ �����',
		number: 221,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	222: {
		id: 526,
		name: '���������',
		number: 222,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	356: {
		id: 527,
		name: '�������',
		number: 356,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	269: {
		id: 528,
		name: '�������������',
		number: 269,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	360: {
		id: 529,
		name: '�������� "������ ����"',
		number: 360,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	361: {
		id: 530,
		name: '�������� "���������� ����"',
		number: 361,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	362: {
		id: 531,
		name: '�������� "������������� ����"',
		number: 362,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2374: {
		id: 532,
		name: '�������� "�������� ���"',
		number: 2374,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	358: {
		id: 533,
		name: '���� ����',
		number: 358,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	359: {
		id: 534,
		name: '���� ��������',
		number: 359,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	363: {
		id: 535,
		name: '���� ����',
		number: 363,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	364: {
		id: 536,
		name: '���� �����',
		number: 364,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	365: {
		id: 537,
		name: '���� ����',
		number: 365,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	366: {
		id: 538,
		name: '���� �����',
		number: 366,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2383: {
		id: 539,
		name: '�������� "�������� � �������"',
		number: 2383,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2375: {
		id: 540,
		name: '�������� "����� �������� ���"',
		number: 2375,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	368: {
		id: 541,
		name: '���� ������',
		number: 368,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	367: {
		id: 542,
		name: '���� ����',
		number: 367,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	369: {
		id: 543,
		name: '���� �����',
		number: 369,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	370: {
		id: 544,
		name: '���� ���������',
		number: 370,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	371: {
		id: 545,
		name: '���� �����',
		number: 371,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	372: {
		id: 546,
		name: '���� ��������',
		number: 372,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	373: {
		id: 547,
		name: '���� ��������',
		number: 373,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2384: {
		id: 548,
		name: '�������� "�������� �����"',
		number: 2384,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2385: {
		id: 549,
		name: '�������� "���� �����"',
		number: 2385,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	374: {
		id: 550,
		name: '�������� �����',
		number: 374,
		numeric: true,
		effect: false,
		percent: 1,
		unique: true
	}, 
	2386: {
		id: 551,
		name: '�������� "��������� ���"',
		number: 2386,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	287: {
		id: 552,
		name: '����� ��������',
		number: 287,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	288: {
		id: 553,
		name: '����� ������',
		number: 288,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	289: {
		id: 554,
		name: '����� �������',
		number: 289,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	284: {
		id: 555,
		name: '�������� �����',
		number: 284,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	291: {
		id: 556,
		name: '���������� �������',
		number: 291,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	275: {
		id: 557,
		name: '�����������',
		number: 275,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	282: {
		id: 558,
		name: '�������� �����',
		number: 282,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	283: {
		id: 559,
		name: '��������� �����',
		number: 283,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	400: {
		id: 560,
		name: '��������',
		number: 400,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	402: {
		id: 561,
		name: '��������',
		number: 402,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	403: {
		id: 562,
		name: '��������',
		number: 403,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	404: {
		id: 563,
		name: '��������',
		number: 404,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	405: {
		id: 564,
		name: '��������',
		number: 405,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	406: {
		id: 565,
		name: '��������',
		number: 406,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	408: {
		id: 566,
		name: '��������',
		number: 408,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	930: {
		id: 567,
		name: '������',
		number: 930,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	931: {
		id: 568,
		name: '�������',
		number: 931,
		numeric: true,
		effect: true,
		percent: 1,
		unique: false
	}, 
	932: {
		id: 569,
		name: '���������',
		number: 932,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	933: {
		id: 570,
		name: '�����',
		number: 933,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	935: {
		id: 571,
		name: '������������',
		number: 935,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	3006: {
		id: 572,
		name: '��� �����',
		number: 3006,
		numeric: true,
		effect: true,
		percent: 0,
		unique: true
	}, 
	970: {
		id: 573,
		name: '���������',
		number: 970,
		numeric: true,
		effect: true,
		percent: 0,
		unique: false
	}, 
	295: {
		id: 574,
		name: '������',
		number: 295,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	311: {
		id: 575,
		name: '�����',
		number: 311,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2265: {
		id: 576,
		name: '�������� "�������� ����������"',
		number: 2265,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	2264: {
		id: 577,
		name: '�������� "����� �����������������"',
		number: 2264,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	277: {
		id: 578,
		name: '���� �������',
		number: 277,
		numeric: true,
		effect: false,
		percent: 0,
		unique: false
	}, 
	2379: {
		id: 579,
		name: '�������� "�������� ����"',
		number: 2379,
		numeric: true,
		effect: false,
		percent: 0,
		unique: true
	}, 
	273: {
		id: 580,
		name: '������� ������',
		number: 273,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	274: {
		id: 581,
		name: '������� �������',
		number: 274,
		numeric: false,
		effect: true,
		percent: 0,
		unique: true
	}, 
	310: {
		id: 10310,
		name: '����� ����������',
		number: 310,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	309: {
		id: 10309,
		name: '�����',
		number: 309,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	220: {
		id: 10220,
		name: '���������� �� ���� ����� ������� ��������� ��� ������',
		number: 220,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	130: {
		id: 10130,
		name: '��������� ���������� ��� ��������',
		number: 130,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	255: {
		id: 10255,
		name: '������� (��������� ����������)',
		number: 255,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	258: {
		id: 10258,
		name: '������������ ��� ��������� ����������� "�� ���������" �� ��������, ����������� �� �������',
		number: 258,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	256: {
		id: 10256,
		name: '������� (���������� ������ �������� �� ����������� ����� ����)',
		number: 256,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	257: {
		id: 10257,
		name: '������������ ��� ������������� ��������, ������������ �� �������',
		number: 257,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	189: {
		id: 10189,
		name: '���������� �� ���� (�������� �����������)',
		number: 189,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	170: {
		id: 10170,
		name: '������� �����',
		number: 170,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	175: {
		id: 10175,
		name: '������������ ��� �������� ������',
		number: 175,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	172: {
		id: 10172,
		name: '������������ ��� �������� ������',
		number: 172,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	332: {
		id: 10332,
		name: '������������ ��� �������',
		number: 332,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	193: {
		id: 10193,
		name: '������������ ��� ���������� ������ ����������� �����������',
		number: 193,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	194: {
		id: 10194,
		name: '������������ ��� ���������� ������ ����������� �����������',
		number: 194,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	333: {
		id: 10333,
		name: '������������ ��� �������',
		number: 333,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	334: {
		id: 10334,
		name: '������������ ��� �������',
		number: 334,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	329: {
		id: 10329,
		name: '������������ ��� �������',
		number: 329,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	330: {
		id: 10330,
		name: '������������ ��� �������',
		number: 330,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	331: {
		id: 10331,
		name: '������������ ��� �������',
		number: 331,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	153: {
		id: 10153,
		name: '������� ������ (�������� �����������)',
		number: 153,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	224: {
		id: 10224,
		name: '��������� ��� ��������� �������',
		number: 224,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	254: {
		id: 10254,
		name: '������ �������� �� ������ �� ������ ��� ����������',
		number: 254,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	192: {
		id: 10192,
		name: '������������ ��� ���������� ������ ����������� �����������',
		number: 192,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	129: {
		id: 10129,
		name: '��������� ���������� ��� �����',
		number: 129,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	99: {
		id: 10099,
		name: '�� ������ �� ��������� ��� ������',
		number: 99,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	160: {
		id: 10160,
		name: '���������� �������� ������������ �������� ��� ������������� ��������������� ��������',
		number: 160,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	115: {
		id: 10115,
		name: '���������� � ������������ ����������, ��������� ��� ������� (������ ����������� � Quantity)',
		number: 115,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	133: {
		id: 10133,
		name: '�������� ���� (�� ���������� � ������� ��� ������������� ����������)',
		number: 133,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	174: {
		id: 10174,
		name: '������������ ��� �������� ������',
		number: 174,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	171: {
		id: 10171,
		name: '������������ ��� �������� ������',
		number: 171,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	173: {
		id: 10173,
		name: '������������ ��� �������� ������',
		number: 173,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	188: {
		id: 10188,
		name: '������������ ��� ���������� ������ ����������� ������ ������',
		number: 188,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	268: {
		id: 10268,
		name: '������������ ��� ����������� "�������������" (�������� �� ���������� ������������������ ������ ��� ��������)',
		number: 268,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	357: {
		id: 10357,
		name: '������������ ��� ��������� �����',
		number: 357,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	162: {
		id: 10162,
		name: '����� ����� (�������� �����������)',
		number: 162,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	207: {
		id: 10207,
		name: '���� ������� �������� ���������� ������',
		number: 207,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	208: {
		id: 10208,
		name: '���� ������� �������� ���������� ����������',
		number: 208,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	294: {
		id: 10294,
		name: '������������ ������ �������������� �������',
		number: 294,
		numeric: false,
		effect: false,
		percent: 0,
		unique: true
	}, 
	209: {
		id: 10209,
		name: '���� �������� �� Quantity (%%) ������ ����� �� ���',
		number: 209,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	248: {
		id: 10248,
		name: '������������ ��� �������� ������ ������� �������������',
		number: 248,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1019: {
		id: 1019,
		name: '������',
		number: 1019,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1020: {
		id: 1020,
		name: '�����',
		number: 1020,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1021: {
		id: 1021,
		name: '���',
		number: 1021,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	244: {
		id: 10244,
		name: '������������ ��� �������� �������������',
		number: 244,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	243: {
		id: 10243,
		name: '������������ ��� �������� �������� ������',
		number: 243,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	70: {
		id: 10070,
		name: '����������� ��� ����� ��������',
		number: 70,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1036: {
		id: 1036,
		name: '�����',
		number: 1036,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1039: {
		id: 1039,
		name: '��������',
		number: 1039,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	247: {
		id: 10247,
		name: '������������ ��� �������� ���������',
		number: 247,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1037: {
		id: 1037,
		name: '׸��',
		number: 1037,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1038: {
		id: 1038,
		name: '������ ������',
		number: 1038,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	93: {
		id: 10093,
		name: '������ ����������/������������. �������� ������������ ������� ���',
		number: 93,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	233: {
		id: 10233,
		name: '������������ ��� �������� ���������� ����',
		number: 233,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	91: {
		id: 10091,
		name: 'Ҹ���� ����',
		number: 91,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	242: {
		id: 10242,
		name: '������������ ��� �������� ������',
		number: 242,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1056: {
		id: 1056,
		name: '�����',
		number: 1056,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	89: {
		id: 10089,
		name: '���������� �����������',
		number: 89,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	73: {
		id: 10073,
		name: '����������',
		number: 73,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1055: {
		id: 1055,
		name: '�������',
		number: 1055,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	71: {
		id: 10071,
		name: '��������������� �������� �����������',
		number: 71,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	72: {
		id: 10072,
		name: '��������������� �������� ������������� �������',
		number: 72,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	74: {
		id: 10074,
		name: '������������',
		number: 74,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1064: {
		id: 1064,
		name: '������',
		number: 1064,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1065: {
		id: 1065,
		name: '������',
		number: 1065,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	245: {
		id: 10245,
		name: '������������ ��� �������� �����',
		number: 245,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	75: {
		id: 10075,
		name: '�������������� ���',
		number: 75,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1060: {
		id: 1060,
		name: '������',
		number: 1060,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1260: {
		id: 1260,
		name: '�����',
		number: 1260,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	92: {
		id: 10092,
		name: '����� ������ ����',
		number: 92,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1071: {
		id: 1071,
		name: '�������� �����',
		number: 1071,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	149: {
		id: 10149,
		name: '������ ���������� ��������������� ���������� � ���� ��������',
		number: 149,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1070: {
		id: 1070,
		name: '������',
		number: 1070,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	145: {
		id: 10145,
		name: '������������ �������',
		number: 145,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1108: {
		id: 1108,
		name: '�����',
		number: 1108,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	298: {
		id: 10298,
		name: '������������ ��� ������������� ����������� 148 (��������� �������� �����������)',
		number: 298,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	148: {
		id: 10148,
		name: '�������� �������� �� ���� � �����������',
		number: 148,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	151: {
		id: 10151,
		name: '������ �� ������������� ����������� ��������',
		number: 151,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1118: {
		id: 1118,
		name: '���������� ����',
		number: 1118,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	241: {
		id: 10241,
		name: '������������ ��� �������� ���',
		number: 241,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	164: {
		id: 10164,
		name: '������� ���� ��������, ���� ��� ���� ��������',
		number: 164,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1150: {
		id: 1150,
		name: '�������',
		number: 1150,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	246: {
		id: 10246,
		name: '������������ ��� �������� ��������� �����',
		number: 246,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1153: {
		id: 1153,
		name: '���������� �������',
		number: 1153,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1154: {
		id: 1154,
		name: '���������� �����',
		number: 1154,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1155: {
		id: 1155,
		name: '���������� ����',
		number: 1155,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1159: {
		id: 1159,
		name: '����� ������',
		number: 1159,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1098: {
		id: 1098,
		name: '������� ������',
		number: 1098,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1045: {
		id: 1045,
		name: '����',
		number: 1045,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1163: {
		id: 1163,
		name: '������',
		number: 1163,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	183: {
		id: 10183,
		name: '"�����" ������ ����������� �������������� �������',
		number: 183,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	212: {
		id: 10212,
		name: '��������� ������������� �� ����������� ������� �� ����������',
		number: 212,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	211: {
		id: 10211,
		name: '��������� ������� ���� ������ ���',
		number: 211,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1182: {
		id: 1182,
		name: '�����������',
		number: 1182,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1186: {
		id: 1186,
		name: '�������� ����',
		number: 1186,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1170: {
		id: 1170,
		name: '����',
		number: 1170,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1092: {
		id: 1092,
		name: '�����',
		number: 1092,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1171: {
		id: 1171,
		name: '������',
		number: 1171,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1172: {
		id: 1172,
		name: '������',
		number: 1172,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	159: {
		id: 10159,
		name: '��������� �������',
		number: 159,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1097: {
		id: 1097,
		name: '�����������',
		number: 1097,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	252: {
		id: 10252,
		name: '������ ������� ������� ����',
		number: 252,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1193: {
		id: 1193,
		name: '������ �����',
		number: 1193,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1194: {
		id: 1194,
		name: '������',
		number: 1194,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	200: {
		id: 10200,
		name: '��������� ���������� �������',
		number: 200,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1207: {
		id: 1207,
		name: '���� �����',
		number: 1207,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	225: {
		id: 10225,
		name: '�������� �����',
		number: 225,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1208: {
		id: 1208,
		name: '�����',
		number: 1208,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1083: {
		id: 1083,
		name: '����',
		number: 1083,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1210: {
		id: 1210,
		name: '������� �������',
		number: 1210,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1212: {
		id: 1212,
		name: '����',
		number: 1212,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1214: {
		id: 1214,
		name: '������',
		number: 1214,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1213: {
		id: 1213,
		name: '������',
		number: 1213,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1216: {
		id: 1216,
		name: '��������� ����',
		number: 1216,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1218: {
		id: 1218,
		name: '�����',
		number: 1218,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1224: {
		id: 1224,
		name: '��������� ��������',
		number: 1224,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1222: {
		id: 1222,
		name: '���������� �����',
		number: 1222,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	297: {
		id: 10297,
		name: '������������ ��� ���������� �������� ����� ���� ��������, ������������ � ������� ���� � ������ ���� �����',
		number: 297,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	234: {
		id: 10234,
		name: '����������� �� �������� - ������ ��� ������� � ��-������������� ������',
		number: 234,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1099: {
		id: 1099,
		name: '�������',
		number: 1099,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1191: {
		id: 1191,
		name: '������',
		number: 1191,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1094: {
		id: 1094,
		name: '������',
		number: 1094,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1095: {
		id: 1095,
		name: '������',
		number: 1095,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	230: {
		id: 10230,
		name: '������ ����������� �� ������',
		number: 230,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1279: {
		id: 1279,
		name: '���������� ������',
		number: 1279,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1280: {
		id: 1280,
		name: '��������� ������',
		number: 1280,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	152: {
		id: 10152,
		name: '������ �� ������������� ����������� ������������',
		number: 152,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1187: {
		id: 1187,
		name: '�������-���',
		number: 1187,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1205: {
		id: 1205,
		name: '�����������',
		number: 1205,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1229: {
		id: 1229,
		name: '����� �����',
		number: 1229,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1230: {
		id: 1230,
		name: '����� ������',
		number: 1230,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1231: {
		id: 1231,
		name: '����� ����',
		number: 1231,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1234: {
		id: 1234,
		name: '����� �����',
		number: 1234,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1227: {
		id: 1227,
		name: '������',
		number: 1227,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1169: {
		id: 1169,
		name: '�����',
		number: 1169,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1228: {
		id: 1228,
		name: '������ �������',
		number: 1228,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	223: {
		id: 10223,
		name: '����������� ������������ ������������� �������� �� ���������, ������������� �������� - �� �����������',
		number: 223,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	296: {
		id: 10296,
		name: '������� ���������� ������������� �������',
		number: 296,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	251: {
		id: 10251,
		name: '������ ������� ������� ��������',
		number: 251,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1082: {
		id: 1082,
		name: '���������� ���',
		number: 1082,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	285: {
		id: 10285,
		name: '��������� ���������������� ���� ��� ��������� �������',
		number: 285,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1287: {
		id: 1287,
		name: '������',
		number: 1287,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1288: {
		id: 1288,
		name: '�����',
		number: 1288,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}, 
	1289: {
		id: 1289,
		name: '�����',
		number: 1289,
		numeric: false,
		effect: false,
		percent: 0,
		unique: false
	}
}