medalMap = {
	1: {
		id: 1,
		name: '���� �������',
		goldSpent: 0,
		gemSpent: 0,
		rarity: 0,
		effects: [
			{
				upgType: 85,
				power: 25
			}, 
			{
				upgType: 98,
				power: 1
			}, 
			{
				upgType: 0,
				power: 0
			}
		]
	}, 
	2: {
		id: 2,
		name: '������ �� ������',
		goldSpent: 3,
		gemSpent: 0,
		rarity: 1,
		effects: [
			{
				upgType: 12,
				power: 4
			}, 
			{
				upgType: 197,
				power: 1
			}
		]
	}, 
	3: {
		id: 3,
		name: '������ �� ��������',
		goldSpent: 4,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 2,
				power: 3
			}
		]
	}, 
	4: {
		id: 4,
		name: '������ �� ������',
		goldSpent: 4,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 11,
				power: 4
			}, 
			{
				upgType: 21,
				power: 1
			}
		]
	}, 
	5: {
		id: 5,
		name: '������ �� ���������',
		goldSpent: 4,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 1,
				power: 5
			}
		]
	}, 
	6: {
		id: 6,
		name: '������ �� ��������',
		goldSpent: 6,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 77,
				power: 2
			}
		]
	}, 
	7: {
		id: 7,
		name: '����� ��������',
		goldSpent: 10,
		gemSpent: 0,
		rarity: 5,
		effects: [
			{
				upgType: 8,
				power: 2
			}, 
			{
				upgType: 10,
				power: 1
			}
		]
	}, 
	8: {
		id: 8,
		name: '������ ��������',
		goldSpent: 0,
		gemSpent: 1,
		rarity: 2,
		effects: [
			{
				upgType: 24,
				power: 1
			}, 
			{
				upgType: 35,
				power: 1
			}
		]
	}, 
	9: {
		id: 9,
		name: '����� �� �������� ����������',
		goldSpent: 3,
		gemSpent: 2,
		rarity: 5,
		effects: [
			{
				upgType: 24,
				power: 3
			}
		]
	}, 
	10: {
		id: 10,
		name: '������ �� ��������',
		goldSpent: 4,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 5,
				power: 3
			}
		]
	}, 
	11: {
		id: 11,
		name: '������ �� ��������',
		goldSpent: 3,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 4,
				power: 2
			}
		]
	}, 
	12: {
		id: 12,
		name: '������ �� ���������������',
		goldSpent: 3,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 6,
				power: 2
			}
		]
	}, 
	13: {
		id: 13,
		name: '����� ���������',
		goldSpent: 10,
		gemSpent: 0,
		rarity: 5,
		effects: [
			{
				upgType: 4,
				power: 2
			}, 
			{
				upgType: 5,
				power: 2
			}, 
			{
				upgType: 6,
				power: 2
			}
		]
	}, 
	14: {
		id: 14,
		name: '����� �����',
		goldSpent: 10,
		gemSpent: 0,
		rarity: 4,
		effects: [
			{
				upgType: 12,
				power: 6
			}, 
			{
				upgType: 13,
				power: 1
			}, 
			{
				upgType: 120,
				power: 2
			}
		]
	}, 
	15: {
		id: 15,
		name: '����� �����������',
		goldSpent: 10,
		gemSpent: 0,
		rarity: 4,
		effects: [
			{
				upgType: 197,
				power: 2
			}, 
			{
				upgType: 25,
				power: 2
			}, 
			{
				upgType: 121,
				power: 3
			}
		]
	}, 
	16: {
		id: 16,
		name: '����� ����������',
		goldSpent: 8,
		gemSpent: 1,
		rarity: 3,
		effects: [
			{
				upgType: 32,
				power: 1
			}, 
			{
				upgType: 33,
				power: 1
			}, 
			{
				upgType: 34,
				power: 1
			}
		]
	}, 
	17: {
		id: 17,
		name: '������ �� ����������',
		goldSpent: 3,
		gemSpent: 0,
		rarity: 2,
		effects: [
			{
				upgType: 124,
				power: 2
			}
		]
	}, 
	18: {
		id: 18,
		name: '����� ����������',
		goldSpent: 8,
		gemSpent: 0,
		rarity: 6,
		effects: [
			{
				upgType: 3,
				power: 4
			}, 
			{
				upgType: 52,
				power: 2
			}
		]
	}, 
	19: {
		id: 19,
		name: '������� ������',
		goldSpent: 2,
		gemSpent: 1,
		rarity: 2,
		effects: [
			{
				upgType: 60,
				power: 4
			}
		]
	}, 
	20: {
		id: 20,
		name: '������ �� ����������',
		goldSpent: 0,
		gemSpent: 1,
		rarity: 2,
		effects: [
			{
				upgType: 95,
				power: 1
			}, 
			{
				upgType: 161,
				power: 1
			}
		]
	}, 
	21: {
		id: 21,
		name: '������ ��������',
		goldSpent: 5,
		gemSpent: 2,
		rarity: 6,
		effects: [
			{
				upgType: 8,
				power: 2
			}, 
			{
				upgType: 135,
				power: 2
			}
		]
	}, 
	22: {
		id: 22,
		name: '���� �������',
		goldSpent: 0,
		gemSpent: 1,
		rarity: 2,
		effects: [
			{
				upgType: 10,
				power: 2
			}
		]
	}, 
	23: {
		id: 23,
		name: '����� ����',
		goldSpent: 0,
		gemSpent: 4,
		rarity: 4,
		effects: [
			{
				upgType: 905,
				power: 1
			}, 
			{
				upgType: 906,
				power: 1
			}, 
			{
				upgType: 907,
				power: 4
			}
		]
	}
}