upgTypeToIdMap = {};

currentUnit = null;
currentUnitUsedUpgrades = [];
currentHits = {};
currentMedalIds = [];

function onReady() {
	unitStatsResult = $("#unit-stats-result").html();
	initUpgTypeToIdMap();
	initUnitDropdowns();
	initMedalDropdowns();
	var id = param("id");
	if (id) {
		$(".field-unit-id").val(id);
		$(".field-unit-id").trigger("change");
		setTimeout(function(){
			$(".search-btn[mode=init]").trigger("click");
		}, 0);
	}
}

/**
* Initializes medal selection dropdowns.
*/
function initMedalDropdowns() {
	var options = [];
	_.each(medalMap, function(medal, medalId) {
		options.push({value: medalId, label: medal.name});
	});
	options = _.orderBy(options, ['label'], ['asc']);
	$(".field-medal-ids").each(function() {
		var dd = $(this);
		dd.append("<option value=''>-</option>");
		_.each(options, function(opt) {
			dd.append("<option value='" + opt.value + "'>" + opt.label + "</option>");
		});
	});
}

/**
* Initializes unit selection dropdowns.
*/
function initUnitDropdowns() {
	var options = [];
	_.each(unitMap, function(unit, unitId) {
		// all units except heros
		if (!_.includes(unit.subtypes, 5)) {
			options.push({value: unitId, label: unit.name});
		}
	});
	options = _.orderBy(options, ['label'], ['asc']);
	$(".field-unit-id").each(function() {
		var dd = $(this);
		_.each(options, function(opt) {
			dd.append("<option value='" + opt.value + "'>" + opt.label + "</option>");
		});
		dd.change(function() {
			var upgDD = dd.parents("form:first").find(".field-upgrade-id");
			if (upgDD) {
				upgDD.empty();
				var unitId = $(this).val();
				var unit = unitMap[unitId];
				var upgradeIds = unitUpgradeIds(unit);
				var upgOptions = [];
				_.each(upgradeIds, function(upgId) {
					upgOptions.push({value: upgId, label: upgradeMap[upgId].name});
				});
				upgOptions = _.orderBy(upgOptions, ['label'], ['asc']);
				_.each(upgOptions, function(opt) {
					upgDD.append("<option value='" + opt.value + "'>" + opt.label + "</option>");
				});
			}
		});
		dd.trigger("change");
	});
}

/**
* Initializes the map 'upgTypeToIdMap' (upgradeType -> upgradeId).
*/
function initUpgTypeToIdMap() {
	_.each(abilityMap, function(ability, upgTypeStr) {
		var upgType = parseInt(upgTypeStr);
		var upg = _.find(upgradeMap, function(upg) { return upg.upgType == upgType && upg.components.length == 1; });
		if (!upg) {
			upg = _.find(upgradeMap, function(upg) { return _.includes(upg.components, upgType); });
		}
		upgTypeToIdMap[upgType] = !upg ? 0 : upg.id;
	});
}

/**
* Main function. Starts the handler for selected action.
*/
function analyzer(data) {
	log("Unit analyzer: start");
	log(data);
	var action = data.action;
	if (action == "upg-prob") {
		unitUpgradeProbCalc(data);
	} else if (action == "avg-upg-count") {
		unitAvgUpgradeCountCalc(data);
	} else if (action == "unit-stats") {
		unitStatsCalc(data);
	}
}

/**
* Returns distinct unit upgrade IDs.
*/
function unitUpgradeIds(unit) {
	var flatUpgrades = _.flatten(unit.upgrades);
	var upgradeIds = _.uniq( _.map(flatUpgrades, function(u) { return u[0]; }) );
	return upgradeIds;
}

/**
* Automatically calculates unit stats at selected level and renders new state.
*/
function unitStatsCalc(data) {
	var cnt = $("#unit-stats-result");
	cnt.html(unitStatsResult);
	var unit = unitMap[data.unitId];
	var medalIds = _.pull(data.medalIds, '');
	currentMedalIds = medalIds;
	initUnitStats(unit, cnt);
	
	if (!data.mode) {
		var upgrades = unit.upgrades;
		var maxLevel = parseInt(data.level);
		var iterCount = parseInt(data.iter);
		var hits = {};
		
		for (var i = 0; i < iterCount; i++) {
			var usedUpgrades = [];
			for (var j = 0; j < maxLevel; j++) {
				var level = j+1;
				var proposedUpgradeIds = upgradeProposals(unit, level, usedUpgrades);
				var usedUpgradeId = pickUpgrade(proposedUpgradeIds, unit, usedUpgrades);
				var upgrade = upgradeMap[usedUpgradeId];
				_.each(upgrade.components, function(upgType) {
					addUpgradeToHits(upgrade, upgType, hits, usedUpgrades);
				});
				usedUpgrades.push(usedUpgradeId);
			}
		}

		// calculate average level up bonuses
		_.each(hits, function(count, upgType) {
			hits[upgType] = Math.round(hits[upgType]*100/iterCount)/100;
		});
		
		// add medal bonuses
		addMedalsToHits(medalIds, hits);
		
		// add initial attributes/abilities and render new state
		renderHits(hits, unit, maxLevel, cnt);
		
		cnt.find(".manual-lvl-up-btn").hide();
	}
}

/**
* Adds initial attributes/abilities and renders new state.
*/
function renderHits(hits, unit, level, cnt) {
	// add initial base attribute values to level up bonuses (base attributes: defence, stamina etc.)
	_.each(unitAbilityToAttrMap, function(attr, upgType) {
		var baseValue = unit[unitAbilityToAttrMap[upgType]];
		hits[upgType] = !hits[upgType] ? baseValue : hits[upgType] + baseValue
	});
	
	log("hits:");
	log(hits);
	
	// add initial abilities to level up bonuses
	_.each(unit.abilities, function(upgId) {
		var upgrade = upgradeMap[upgId];
		var usedUpgrades = hits[upgrade.upgType] > 0 ? [upgId] : [];
		_.each(upgrade.components, function(upgType) {
			addUpgradeToHits(upgrade, upgType, hits, usedUpgrades, true);
		});
	});
		
	// render new base attribute values
	_.each(unitAbilityToAttrMap, function(attr, upgType) {
		var value = Math.round(hits[upgType]*10)/10;
		cnt.find(".param-" + upgType).text(value);
	});
	
	// render other abilities
	var abCnt = cnt.find(".ab");
	var lvlUpgCnt = cnt.find(".lvl-upg-result");
	abCnt.empty();
	lvlUpgCnt.empty();
	_.each(hits, function(count, upgType) {
		if (upgType > 12) {
			var ability = abilityMap[upgType];
			var value = ability.numeric && !ability.unique ? Math.round(hits[upgType]*10)/10 : Math.round(hits[upgType]*100) + ' %';
			var upgId = upgTypeToIdMap[upgType];
			abCnt.append('<a href="../upgrades.html#'+upgId+'"><img src="../images/upgrades/'+upgId+'.gif" title="'+ability.name+' ('+value+')"></a>');
			lvlUpgCnt.append('<a href="../upgrades.html#'+upgId+'">'+ability.name+' ('+value+')</a><br/>');
		}
	});
	
	cnt.find(".param-level").text(level);
	
	// render payment (consider level, rank and medals)
	var payment = unitPayment(unit, hits, level);
	cnt.find(".param-goldPayment").text(payment.gold);
	cnt.find(".param-gemPayment").text(payment.gem);
}

/**
* Returns unit payment ({gold: X, gem: Y}) at level.
* Considers medals and forager/increasedPayment abilities.
*/
function unitPayment(unit, hits, level) {
	var gold = unit.goldPayment;
	var gem = unit.gemPayment;
	_.each(currentMedalIds, function(mId) {
		var medalId = parseInt(mId);
		var medal = medalMap[medalId];
		gold += medal.goldSpent;
		gem += medal.gemSpent;
	});
	
	var forager = hits[54];
	if (forager) {
		gold -= forager;
	}
	
	gold = Math.max(gold, 0);
	
	gold = Math.floor(gold*(1 + level/(10*(unit.rank+1))));
	gem  = Math.floor( gem*(1 + level/(10*(unit.rank+1))));
	
	var increasedPayment = hits[85];
	if (increasedPayment) {
		gold = Math.floor( gold*2.5 );
	}
	
	return {gold: gold, gem: gem};
}

/**
* Adds upgrade quantity to hits.
*/
function addUpgradeToHits(upgrade, upgType, hits, usedUpgrades, ignoreOnlyOnce) {
	var ability = abilityMap[upgType];
	if (!_.includes(upgTypesToExclude, upgType)) {
		if (!hits[upgType]) {
			hits[upgType] = 0;
		}
		if (!(upgrade.onlyOnce && !ignoreOnlyOnce && _.includes(usedUpgrades, upgrade.id))) {
			var idx = _.indexOf(upgrade.components, upgType);
			var quantity = upgrade.quantities[idx];
			var q = ability.numeric && !ability.unique ? quantity : 1;
			if (ability.unique && ((ignoreOnlyOnce && hits[upgType] > 0) || (!ignoreOnlyOnce && isAbilityUsed(usedUpgrades, upgType)))) {
				q = ignoreOnlyOnce && hits[upgType] < 1 ? 1 - hits[upgType] : 0;
			}
			hits[upgType] += q;
			//log("adding upgrade " + upgrade.id + "/" + upgrade.name + "/" + upgType + " ability: " + ability.id + "/" + ability.name + " quantity: " + q);
		}
	} else {
		//log("ignoring upgrade " + upgrade.id + "/" + upgrade.name + "/" + upgType + " ability: " + ability.id + "/" + ability.name);
	}
}

function isAbilityUsed(usedUpgrades, upgType) {
	for (var i = 0; i < usedUpgrades.length; i++) {
		var upgId = usedUpgrades[i];
		var upgrade = upgradeMap[upgId];
		if (_.includes(upgrade.components, upgType)) {
			return true;
		}
	}
	return false;
}

/**
* Adds medal effects to hits.
*/
function addMedalsToHits(medalIds, hits) {
	_.each(medalIds, function(mId) {
		var medalId = parseInt(mId);
		var medal = medalMap[medalId];
		_.each(medal.effects, function(effect) {
			var upgType = effect.upgType;
			var ability = abilityMap[upgType];
			if (ability) {
				if (!hits[upgType]) {
					hits[upgType] = 0;
				}
				hits[upgType] += ability.numeric && !ability.unique ? Math.max(effect.power, 1) : 1;
			}
		});
	});
}

/**
* Converts unit class to one of 3 basic classes (militia = 1, bowman = 2, mage = 4).
*/
function effectiveUnitClass(unit) {
	var unitClass = unit.unitClass;
	if (!_.includes([1, 2, 4], unitClass)) {
		return unit.attack >= unit.rangedAttack ? 1 : 2;
	}
	return unitClass;
}

/**
* Picks one of two proposed upgrades (the one with a higher weight for unit class).
* Returns upgrade ID.
*/
function pickUpgrade(proposedUpgradeIds, unit, usedUpgrades) {
	var unitClass = effectiveUnitClass(unit);
	var weight1 = upgWeightForClass(proposedUpgradeIds[0], unitClass);
	var weight2 = upgWeightForClass(proposedUpgradeIds[1], unitClass);
	return weight1 >= weight2 ? proposedUpgradeIds[0] : proposedUpgradeIds[1];
}

/**
* Returns weight of the upgrade for unit class.
*/
function upgWeightForClass(upgId, unitClass) {
	var upgWeightMap = unitClassToUpgWeightMap[unitClass];
	var upgrade = upgradeMap[upgId];
	var weight = 0;
	_.each(upgrade.components, function(upgType, i) {
		var quantity = Math.max(upgrade.quantities[i], 1);
		if (upgType <= 12) {
			// base attributes
			weight += upgWeightMap[upgType]*quantity;
		} else {
			// abilities
			var ability = abilityMap[upgType];
			weight += ability.numeric ? 5*quantity : 10;
		}
	});
	return weight;
}

/**
* Renders unit box with initial stats (level 0).
*/
function initUnitStats(unit, cnt) {
	currentUnit = unit;
	_.each(unitAbilityToAttrMap, function(attr, upgType) {
		cnt.find(".param-" + upgType).text(unit[attr]);
	});
	_.each(['name', 'goldPayment', 'gemPayment', 'rank', 'goldPrice', 'gemPrice', 'exp', 'expMod'], function(param) {
		cnt.find(".param-" + param).text(unit[param]);
	});
	cnt.find(".param-id-attr").each(function() {
		var el = $(this);
		_.each(['name', 'href', 'style'], function(attr) {
			if (el.attr(attr)) el.attr(attr, el.attr(attr).replace("000", unit.id));
		});
		$(this).text($(this).text().replace("000", unit.id));
	});
	cnt.find(".param-karma").text(karmaValues[unit.karma+3]);
	_.each(unit.abilities, function(upgId) {
		cnt.find(".ab").append('<a href="../upgrades.html#'+upgId+'"><img src="../images/upgrades/'+upgId+'.gif" title="'+upgradeMap[upgId].name+'"></a>');
	});

	var subtypes = '';
	var subtypeArr = unit.unitKind ? _.merge(unit.unitKind, unit.subtypes) : unit.subtypes;
	_.each(subtypeArr, function(st, i) {
		if (i > 0) subtypes += ', ';
		subtypes += unitSubtypes[st];
	});
	var amr = cnt.find(".amr");
	amr.empty();
	_.each(unit.resources, function(resourceId) {
		amr.append('<img title="' + resourceMap[resourceId] + '" src="../images/resources/' + resourceId + '.gif">');
	});
	cnt.find(".param-subtypes").text(subtypes);
	cnt.find(".param-class").text(unitClasses[unit.unitClass]);
	// level upgrades
	var lvlUpgrades = groupedLvlUpgrades(unit);
	_.each(lvlUpgrades, function(upgrades, i) {
		var lvlCnt = cnt.find(".lvl-"+i);
		lvlCnt.empty();
		var tr = lvlCnt.parents("tr:first");
		upgrades.length > 0 ? tr.show() : tr.hide();
		_.each(upgrades, function(upg, j) {
			if (j > 0) {
				lvlCnt.append(', ');
			}
			lvlCnt.append('<a href="../upgrades.html#' + upg.id + '" class="imp' + upg.important + '">' + upgradeMap[upg.id].name + '</a>');
			if (upg.count > 1) {
				lvlCnt.append(' x' + upg.count);
			}
		});
	});
	cnt.find(".manual-lvl-up-btn").show();
	cnt.find(".manual-lvl-up-btn button").click(function() {
		cnt.find(".manual-lvl-up-btn").hide();
		cnt.find(".manual-lvl-up-lvls").show();
		currentUnitUsedUpgrades = [];
		currentHits = {};
		doLvlUp();
	});
}

/**
* Handles manual level up (shows upgrade proposals and handles upgrade selection).
*/
function doLvlUp() {
	var cnt = $("#unit-stats-result");
	var level = currentUnitUsedUpgrades.length+1;
	if (level <= 30) {
		var proposedUpgradeIds = upgradeProposals(currentUnit, level, currentUnitUsedUpgrades);
		cnt.find(".pick-lvl-counter").text(level);
		_.each(proposedUpgradeIds, function(upgId, i) {
			var slotCnt = cnt.find(".pick-lvl-" + i);
			slotCnt.empty();
			slotCnt.append('<img upg-id="' + upgId + '" src="../images/upgrades/' + upgId + '.gif" title="' + upgradeMap[upgId].name + '"/>');
			slotCnt.find("img").click(function() {
				applyLvl(parseInt($(this).attr("upg-id")), cnt);
			});
		});
	} else {
		cnt.find(".manual-lvl-up-lvls").hide();
	}
}

/**
* Applies manually selected upgrade.
*/
function applyLvl(upgId, cnt) {
	var upgrade = upgradeMap[upgId];
	_.each(upgrade.components, function(upgType) {
		addUpgradeToHits(upgrade, upgType, currentHits, currentUnitUsedUpgrades);
	});
	currentUnitUsedUpgrades.push(upgId);
	var level = currentUnitUsedUpgrades.length;
	var hits = _.cloneDeep(currentHits);
	// add medal bonuses
	addMedalsToHits(currentMedalIds, hits);
	renderHits(hits, currentUnit, level, cnt);
	doLvlUp();
}

/**
* Returns upgrade info entries grouped by level.
*/
function groupedLvlUpgrades(unit) {
	var upgMap = lvlUpgradeMap(unit);
	var lvlUpgrades = new Array(21);
	_.each(lvlUpgrades, function(e, i) {lvlUpgrades[i] = [];});
	_.each(upgMap, function(v, upgId) {
		var upgrade = upgradeMap[upgId];
		var ability = abilityMap[upgrade.upgType];
		var important = upgrade.upgType > 12;
		lvlUpgrades[v.level].push({id: upgId, count: v.count, important: important});
	});
	return lvlUpgrades;
}

/**
* Returns map: upgradeId -> {level: X, count: Y}.
* level: starting level (first upgrade appearance)
* count: total count of upgrade appearances
*/
function lvlUpgradeMap(unit) {
	var upgMap = {};
	_.each(unit.upgrades, function(lvlUpgrades, i) {
		_.each(lvlUpgrades, function(upg) {
			var upgId = upg[0];
			if (!upgMap[upgId]) {
				upgMap[upgId] = {level: i+1, count: 1};
			} else {
				upgMap[upgId].count++;
			}
		});
	});
	_.each(unit.abilities, function(upgId) {
		if (!upgMap[upgId]) {
			upgMap[upgId] = {level: 0, count: 1};
		} else {
			upgMap[upgId].level = 0;
			upgMap[upgId].count++;
		}
	});
	return upgMap;
}


/**
* Calculates and renders average upgrade count at different levels
* (upgrade has the highest priority).
*/
function unitAvgUpgradeCountCalc(data) {
	var cnt = $("#avg-upg-count-result");
	cnt.empty();
	var unit = unitMap[data.unitId];
	var upgrades = unit.upgrades;
	var maxLevel = parseInt(data.level);
	var upgradeId = parseInt(data.upgradeId);
	var iterCount = parseInt(data.iter);
	var hits = _.fill(Array(maxLevel), 0);
	
	for (var i = 0; i < iterCount; i++) {
		var usedUpgrades = [];
		var count = 0;
		for (var j = 0; j < maxLevel; j++) {
			var level = j+1;
			var proposedUpgradeIds = upgradeProposals(unit, level, usedUpgrades);
			if (_.includes(proposedUpgradeIds, upgradeId)) {
				count++;
				usedUpgrades.push(upgradeId);
			} else {
				usedUpgrades.push(proposedUpgradeIds[0]);
			}
			hits[j] += count;
		}
	}
	
	for (var j = 0; j < maxLevel; j++) {
		hits[j] = Math.round(hits[j]*10/iterCount)/10;
		cnt.append((j+1) + ": " + hits[j] + "<br/>");
	}
}

/**
* Calculates and renders average upgrade probability at different levels.
*/
function unitUpgradeProbCalc(data) {
	var cnt = $("#upg-prob-result");
	cnt.empty();
	var unit = unitMap[data.unitId];
	var upgrades = unit.upgrades;
	var maxLevel = parseInt(data.level);
	var upgradeId = parseInt(data.upgradeId);
	var iterCount = parseInt(data.iter);
	var hits = _.fill(Array(maxLevel), 0);
	
	for (var i = 0; i < iterCount; i++) {
		var usedUpgrades = [];
		var found = false;
		for (var j = 0; j < maxLevel; j++) {
			var level = j+1;
			if (!found) {
				var proposedUpgradeIds = upgradeProposals(unit, level, usedUpgrades);
				usedUpgrades.push(proposedUpgradeIds[0]);
				if (_.includes(proposedUpgradeIds, upgradeId)) {
					found = true;
				}
			}
			if (found) {
				hits[j]++;
			}
		}
	}
	
	for (var j = 0; j < maxLevel; j++) {
		hits[j] = Math.round(hits[j]*100/iterCount);
		cnt.append((j+1) + ": " + hits[j] + " %<br/>");
	}
}

/**
* Returns 2 upgrade proposals for certain level.
*/
function upgradeProposals(unit, level, usedUpgrades) {
	var slot1UpgId = upgradeProposal(unit, level, usedUpgrades, 0);
	var slot2UpgId = upgradeProposal(unit, level, usedUpgrades, slot1UpgId);
	return [slot1UpgId, slot2UpgId];
}

/**
* Returns 1 upgrade proposal for certain level (exceptUpgId = 0 for first slot).
*/
function upgradeProposal(unit, level, usedUpgrades, exceptUpgId) {
	var poolSize = 200;
	// 1
	var missedUpgrades = _.fill(Array(poolSize), 0);
	// 2
	var roller = new Array(poolSize);
	_.each(roller, function(upg, x) {
		roller[x] = {id: 0, weight: 0};
	});
	//_.fill(Array(poolSize), function() { return {id: 0, weight: 0} });
	var upgrades = unit.upgrades;
	// 3
	for (var i = 0; i < level; i++) {
		// 3.1
		if (i > 0) {
			var prevUsedUpgradeId = usedUpgrades[i-1];
			var x = _.findIndex(roller, { 'id': prevUsedUpgradeId });
			if (x != -1) {
				if (missedUpgrades[x] == 1) {
					roller[x].weight = 0;
				} else if (missedUpgrades[x] >= 2) {
					roller[x].weight -= roller[x].weight / missedUpgrades[x];
				}
				missedUpgrades[x] -= 1;
			}
		}
		
		// 3.2
		_.each(roller, function(upg, x) {
			if (upg.id > 0 && upg.weight > 0) {
				upg.weight += missedUpgrades[x];
			}
		});
		
		// 3.3
		if (i < 20) {
			var lvlUpgrades = upgrades[i];
			for (var j = 0; j < lvlUpgrades.length; j++) {
				var upgrade = lvlUpgrades[j];
				var upgId = upgrade[0];
				var weight = upgrade[1];
				var x = _.findIndex(roller, function(upg) { return upg.id == 0 || upg.id == upgId; });
				roller[x].id = upgId;
				roller[x].weight = weight;
				missedUpgrades[x] += 1;
			}
		}
	}

	// 4
	if (exceptUpgId > 0) {
		var exceptUpgType = upgradeMap[exceptUpgId].upgType;
		_.each(roller, function(upg, x) {
			if (upg.id > 0 && upgradeMap[upg.id].upgType == exceptUpgType) {
				upg.weight = 0;
			}
		});
	}
	
	// 5
	var upgrade = randomUpgrade(roller);
	
	return upgrade.id;
	
	/*
	1) ��������� � ���������� ������ MissedUpgrades; ����������� = 1000 (��� � Roller). ���� ������ �������� �� "��������" ����� � ���������, ���������� � ��������� Roller.
	2) ���������� ��� �������� � Roller.

	3) ����������� ����, ������������ ��� ������ "Lvl XX upgrades" �� unit.var, ����������� � �����, ��� ������� �� 0 �� ToLevel-1 (������ ������ ������ "Lvl 01 upgrades" � ���� = 0, �� ���� ������ ������� �� -1). ������� ����� - LevelCnt: 
	
	3.1) ���� LevelCnt > 0, ��:
	3.1.1) ���� � Roller ������ ������� X, � �������� Roller[X].Type == ID ��������, ������� ������ �����, ��� ��������� ������ LevelCnt-1;
	3.1.2) ���� ����� ������� ������, ��:
	3.1.2.1) ���� MissedUpgrades[X] ==1, �� Roller[X].Weight = 0;
	3.1.2.2) ���� MissedUpgrades[X] >=2, �� Roller[X].Weight -= Roller[X].Weight / MissedUpgrades[X];
	3.1.2.3) MissedUpgrades[X] -= 1
	
	3.2) ��� ������� �������� X �� Roller, � �������� Roller[X].Type > 0 � Roller[X].Weight > 0 ����������� ��������: Roller[X].Weight += MissedUpgrades[X];

	3.3) ���� LevelCnt < 20, ��:
	3.3.1) ����������� ����, ������������ ��� �������� �� unit.var, ������� ��������� �� ������, ����������� � ������ ����� = LevelCnt. ������� �������������� ������� - CurUpg; ��� ��������� � ������ UpgType � UpgWeight.
	3.3.1.1) �����������, ��� ��� ����������� ��� ��������� �������� CurUpg.UpgType ������ ��� ���� � ����� (���������� ��� �� ���������� ��������� ��� ����� ������). ��� ������ �������� ����� ���� Need � unit_upg.var.
	3.3.1.2) ���� ���� ��� ��������� ������, ��:
	3.3.1.2.1) ���� � Roller ������ ������� X � Roller[X].Type == 0 ��� Roller[X].Type == CurUpg.UpgType;
	3.3.1.2.2) ����������� ���������� �������� Roller[X] �������������� CurUpg: Roller[X].Type = CurUpg.UpgType; Roller[X].Weight = CurUpg.Weight;
	3.3.1.2.3) MissedUpgrades[X] += 1;

	4) �������� Roller[X].Weight ��� ������� X, � �������� ��� ������ ������ �������� Roller[X].Type ����� ���� ������ ������ �������� � ������� ExceptUpg. ��� "������ ������" �������� � ������ �� ����� "Upg Type" ��� ������� � unit_upg.var.

	5) �� Roller ���������� ��������� ������� � ������������ � "������" (����������� ������ �������� ����� ��������������� ��� ����). ID ����� �������� ������������ ��������. 
	*/
}

/**
* Returns random upgrade.
* The algorithm is based on the discrete cumulative density function (CDF)
* which is the sum of the weights.
* https://stackoverflow.com/questions/41418689/get-random-element-from-array-with-weighted-elements/41418770
**/
function randomUpgrade(roller) {
	var sumOfWeights = roller.reduce(function(memo, upgrade) {
		return memo + upgrade.weight;
	}, 0);

	function getRandom(sumOfWeights) {
	  var random = Math.random() * sumOfWeights;

	  return function(upgrade) {
		random -= upgrade.weight;
		return random <= 0;
	  };
	}

	var upgrade = roller.find(getRandom(sumOfWeights));
	return upgrade;
}

function param(name) {
    return (location.search.split(name + '=')[1] || '').split('&')[0];
}

function log(a) {
	console.log(a);
}
