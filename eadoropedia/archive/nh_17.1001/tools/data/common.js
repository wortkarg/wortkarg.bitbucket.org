upgTypesToExclude = [171, 172, 173, 174, 175, 189, 193, 194, 305, 306, 307, 308, 329];
//[189, 311, 310, 309, 220, 130, 255, 258, 256, 257, 170, 175, 172, 332, 193, 194, 333, 334, 329, 330, 331, 153, 224, 254, 192, 129, 99, 160, 115, 133, 174, 171, 173, 188, 268];

karmaValues = {
	0: '������� ���',
	1: '����',
	2: '�������������',
	3: '�����������',
	4: '����������',
	5: '������',
	6: '�������� �����'
}

unitSubtypes = {
	1: '��������',
	2: '������',
	3: '�����',
	4: '��������',
	5: '�����',
	6: '�������',
	7: '����',
	8: '����',
	9: '������',
	10: '���',
	11: '����������',
	12: '�������',
	13: '��������',
	14: '������',
	15: '�����',
	16: '������',
	17: '��������',
	18: '����� ������',
	19: '��������',
	20: '�������',
	21: '���� �����',
	22: '��������� ���',
	23: '�����',
	24: '�������',
	25: '������',
	26: '��������',
	27: '����������',
	28: '��������� ������',
	29: '��������'
}

unitClasses = {
	1: '������',
	2: '�������',
	3: '���������',
	4: '�����������',
	5: '������',
	6: '������',
	7: '��������'
}

unitAbilityToAttrMap = {
	1: 'life',
	2: 'attack',
	3: 'counterAttack',
	4: 'defence',
	5: 'rangedDefence',
	6: 'resist',
	7: 'speed',
	8: 'rangedAttack',
	9: 'shootingRange',
	10: 'ammo',
	11: 'stamina',
	12: 'morale'
}

unitClassToUpgWeightMap = {
	// ������
	1: {
		1: 1, // life
		2: 1.2, // attack
		3: 1.2, // counterAttack
		4: 1.5, // defence
		5: 1.3, // rangedDefence
		6: 1.4, // resist
		7: 6, // speed
		8: 1.1, // rangedAttack
		9: 2, // shootingRange
		10: 1.1, // ammo
		11: 0.7, // stamina
		12: 0.7, // morale
	},
	// �������
	2: {
		1: 1, // life
		2: 0.3, // attack
		3: 0.3, // counterAttack
		4: 0.5, // defence
		5: 0.6, // rangedDefence
		6: 0.6, // resist
		7: 2, // speed
		8: 3, // rangedAttack
		9: 11, // shootingRange
		10: 1.1, // ammo
		11: 0.6, // stamina
		12: 0.6, // morale
	},
	// �����������
	4: {
		1: 1, // life
		2: 0.3, // attack
		3: 0.3, // counterAttack
		4: 0.5, // defence
		5: 0.6, // rangedDefence
		6: 0.6, // resist
		7: 2, // speed
		8: 3, // rangedAttack
		9: 11, // shootingRange
		10: 1.1, // ammo
		11: 0.6, // stamina
		12: 0.6, // morale
	},
}

resourceMap = {
	1: '������', 
	2: '������� ������', 
	3: '����', 
	4: '����������', 
	5: '�������', 
	6: '������', 
	7: '������', 
	8: '������', 
	9: '׸���� �����'
}
