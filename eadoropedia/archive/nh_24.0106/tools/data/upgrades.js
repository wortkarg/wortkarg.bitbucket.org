upgradeMap = {
	1: {
		id: 1,
		name: '����� +1',
		onlyOnce: false,
		upgType: 1,
		quantity: 1,
		components: [1],
		quantities: [1]
	},
	2: {
		id: 2,
		name: '����� +2',
		onlyOnce: false,
		upgType: 1,
		quantity: 2,
		components: [1],
		quantities: [2]
	},
	3: {
		id: 3,
		name: '����� +3',
		onlyOnce: false,
		upgType: 1,
		quantity: 3,
		components: [1],
		quantities: [3]
	},
	4: {
		id: 4,
		name: '����� +1',
		onlyOnce: false,
		upgType: 2,
		quantity: 1,
		components: [2],
		quantities: [1]
	},
	5: {
		id: 5,
		name: '����� +2',
		onlyOnce: false,
		upgType: 2,
		quantity: 2,
		components: [2],
		quantities: [2]
	},
	6: {
		id: 6,
		name: '����� +3',
		onlyOnce: false,
		upgType: 2,
		quantity: 3,
		components: [2],
		quantities: [3]
	},
	7: {
		id: 7,
		name: '���������� +1',
		onlyOnce: false,
		upgType: 3,
		quantity: 1,
		components: [3],
		quantities: [1]
	},
	8: {
		id: 8,
		name: '���������� +2',
		onlyOnce: false,
		upgType: 3,
		quantity: 2,
		components: [3],
		quantities: [2]
	},
	9: {
		id: 9,
		name: '���������� +3',
		onlyOnce: false,
		upgType: 3,
		quantity: 3,
		components: [3],
		quantities: [3]
	},
	10: {
		id: 10,
		name: '������ +1',
		onlyOnce: false,
		upgType: 4,
		quantity: 1,
		components: [4],
		quantities: [1]
	},
	11: {
		id: 11,
		name: '������ +2',
		onlyOnce: false,
		upgType: 4,
		quantity: 2,
		components: [4],
		quantities: [2]
	},
	12: {
		id: 12,
		name: '������ +3',
		onlyOnce: false,
		upgType: 4,
		quantity: 3,
		components: [4],
		quantities: [3]
	},
	13: {
		id: 13,
		name: '������ �� �������� +1',
		onlyOnce: false,
		upgType: 5,
		quantity: 1,
		components: [5],
		quantities: [1]
	},
	14: {
		id: 14,
		name: '������ �� �������� +2',
		onlyOnce: false,
		upgType: 5,
		quantity: 2,
		components: [5],
		quantities: [2]
	},
	15: {
		id: 15,
		name: '������ �� �������� +3',
		onlyOnce: false,
		upgType: 5,
		quantity: 3,
		components: [5],
		quantities: [3]
	},
	16: {
		id: 16,
		name: '������������� +1',
		onlyOnce: false,
		upgType: 6,
		quantity: 1,
		components: [6],
		quantities: [1]
	},
	17: {
		id: 17,
		name: '������������� +2',
		onlyOnce: false,
		upgType: 6,
		quantity: 2,
		components: [6],
		quantities: [2]
	},
	18: {
		id: 18,
		name: '�������� �����',
		onlyOnce: false,
		upgType: 284,
		quantity: 1,
		neededAbilities: [85],
		components: [284],
		quantities: [1]
	},
	19: {
		id: 19,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 7,
		quantity: 1,
		components: [7],
		quantities: [1]
	},
	20: {
		id: 20,
		name: '������������� ����� +1',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		components: [8],
		quantities: [1]
	},
	21: {
		id: 21,
		name: '������������� ����� +2',
		onlyOnce: false,
		upgType: 8,
		quantity: 2,
		components: [8],
		quantities: [2]
	},
	22: {
		id: 22,
		name: '������������� ����� +3',
		onlyOnce: false,
		upgType: 8,
		quantity: 3,
		components: [8],
		quantities: [3]
	},
	23: {
		id: 23,
		name: '��������� �������� +1',
		onlyOnce: false,
		upgType: 9,
		quantity: 1,
		components: [9],
		quantities: [1]
	},
	24: {
		id: 24,
		name: '��������� �������� +2',
		onlyOnce: false,
		upgType: 9,
		quantity: 2,
		components: [9],
		quantities: [2]
	},
	25: {
		id: 25,
		name: '����� +4',
		onlyOnce: false,
		upgType: 1,
		quantity: 4,
		components: [1],
		quantities: [4]
	},
	26: {
		id: 26,
		name: '������������ +1',
		onlyOnce: false,
		upgType: 24,
		quantity: 1,
		neededAbilities: [24],
		components: [24],
		quantities: [1]
	},
	27: {
		id: 27,
		name: '����� �������� +1',
		onlyOnce: false,
		upgType: 10,
		quantity: 1,
		components: [10],
		quantities: [1]
	},
	28: {
		id: 28,
		name: '����� �������� +2',
		onlyOnce: false,
		upgType: 10,
		quantity: 2,
		components: [10],
		quantities: [2]
	},
	29: {
		id: 29,
		name: '����� �������� +3',
		onlyOnce: false,
		upgType: 10,
		quantity: 3,
		components: [10],
		quantities: [3]
	},
	30: {
		id: 30,
		name: '������������ +1',
		onlyOnce: false,
		upgType: 11,
		quantity: 1,
		components: [11],
		quantities: [1]
	},
	31: {
		id: 31,
		name: '������������ +2',
		onlyOnce: false,
		upgType: 11,
		quantity: 2,
		components: [11],
		quantities: [2]
	},
	32: {
		id: 32,
		name: '������������ +3',
		onlyOnce: false,
		upgType: 11,
		quantity: 3,
		components: [11],
		quantities: [3]
	},
	33: {
		id: 33,
		name: '������ ��� +1',
		onlyOnce: false,
		upgType: 12,
		quantity: 1,
		components: [12],
		quantities: [1]
	},
	34: {
		id: 34,
		name: '������ ��� +2',
		onlyOnce: false,
		upgType: 12,
		quantity: 2,
		components: [12],
		quantities: [2]
	},
	35: {
		id: 35,
		name: '������ ��� +3',
		onlyOnce: false,
		upgType: 12,
		quantity: 3,
		components: [12],
		quantities: [3]
	},
	36: {
		id: 36,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 1,
		quantity: 1,
		components: [1, 11],
		quantities: [1, 1]
	},
	37: {
		id: 37,
		name: '�������� +2',
		onlyOnce: false,
		upgType: 1,
		quantity: 2,
		components: [1, 11],
		quantities: [2, 2]
	},
	38: {
		id: 38,
		name: '���� +1',
		onlyOnce: false,
		upgType: 2,
		quantity: 1,
		components: [2, 3],
		quantities: [1, 1]
	},
	39: {
		id: 39,
		name: '���� +2',
		onlyOnce: false,
		upgType: 2,
		quantity: 2,
		components: [2, 3],
		quantities: [2, 2]
	},
	40: {
		id: 40,
		name: '����� +1',
		onlyOnce: false,
		upgType: 4,
		quantity: 1,
		components: [4, 5],
		quantities: [1, 1]
	},
	41: {
		id: 41,
		name: '����� +2',
		onlyOnce: false,
		upgType: 4,
		quantity: 2,
		components: [4, 5],
		quantities: [2, 2]
	},
	42: {
		id: 42,
		name: '���� ���� +1',
		onlyOnce: false,
		upgType: 6,
		quantity: 1,
		components: [6, 12],
		quantities: [1, 1]
	},
	43: {
		id: 43,
		name: '���� ���� +2',
		onlyOnce: false,
		upgType: 6,
		quantity: 2,
		components: [6, 12],
		quantities: [2, 2]
	},
	44: {
		id: 44,
		name: '��������',
		onlyOnce: false,
		upgType: 14,
		quantity: 1,
		components: [14],
		quantities: [1]
	},
	45: {
		id: 45,
		name: '�������������',
		onlyOnce: false,
		upgType: 15,
		quantity: 1,
		components: [15],
		quantities: [1]
	},
	46: {
		id: 46,
		name: '������ ����',
		onlyOnce: false,
		upgType: 16,
		quantity: 1,
		components: [16],
		quantities: [1]
	},
	47: {
		id: 47,
		name: '����������� �������',
		onlyOnce: false,
		upgType: 31,
		quantity: 1,
		components: [31],
		quantities: [1]
	},
	48: {
		id: 48,
		name: '������������',
		onlyOnce: false,
		upgType: 18,
		quantity: 1,
		components: [18],
		quantities: [1]
	},
	49: {
		id: 49,
		name: '��������������',
		onlyOnce: false,
		upgType: 19,
		quantity: 1,
		components: [19],
		quantities: [1]
	},
	50: {
		id: 50,
		name: '�������������� �������',
		onlyOnce: true,
		upgType: 20,
		quantity: 5,
		components: [20],
		quantities: [5]
	},
	51: {
		id: 51,
		name: '�������������� ��� +1',
		onlyOnce: false,
		upgType: 21,
		quantity: 1,
		components: [21],
		quantities: [1]
	},
	52: {
		id: 52,
		name: '��������� +1',
		onlyOnce: false,
		upgType: 22,
		quantity: 1,
		components: [22],
		quantities: [1]
	},
	53: {
		id: 53,
		name: '���� ��������',
		onlyOnce: true,
		upgType: 23,
		quantity: 3,
		components: [23],
		quantities: [3]
	},
	54: {
		id: 54,
		name: '������������ +3',
		onlyOnce: false,
		upgType: 24,
		quantity: 3,
		components: [24],
		quantities: [3]
	},
	55: {
		id: 55,
		name: '������ ������� +2',
		onlyOnce: false,
		upgType: 25,
		quantity: 2,
		components: [25],
		quantities: [2]
	},
	56: {
		id: 56,
		name: '��������',
		onlyOnce: false,
		upgType: 26,
		quantity: 1,
		components: [26],
		quantities: [1]
	},
	57: {
		id: 57,
		name: '���������� ����',
		onlyOnce: true,
		upgType: 27,
		quantity: 1,
		components: [27],
		quantities: [1]
	},
	58: {
		id: 58,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 28,
		quantity: 1,
		components: [28],
		quantities: [1]
	},
	59: {
		id: 59,
		name: '����-������',
		onlyOnce: true,
		upgType: 29,
		quantity: 1,
		components: [29],
		quantities: [1]
	},
	60: {
		id: 60,
		name: '��������� � �����������',
		onlyOnce: false,
		upgType: 110,
		quantity: 1,
		components: [110],
		quantities: [1]
	},
	61: {
		id: 61,
		name: '������� ������',
		onlyOnce: true,
		upgType: 13,
		quantity: 1,
		components: [13, 19, 18, 108, 102],
		quantities: [1, 1, 1, 1, 1]
	},
	62: {
		id: 62,
		name: '������ ���� +4',
		onlyOnce: true,
		upgType: 32,
		quantity: 4,
		components: [32],
		quantities: [4]
	},
	63: {
		id: 63,
		name: '������ ������ +4',
		onlyOnce: true,
		upgType: 33,
		quantity: 4,
		components: [33],
		quantities: [4]
	},
	64: {
		id: 64,
		name: '������ ����� +4',
		onlyOnce: true,
		upgType: 34,
		quantity: 4,
		components: [34],
		quantities: [4]
	},
	65: {
		id: 65,
		name: '������ ������ +1',
		onlyOnce: false,
		upgType: 35,
		quantity: 1,
		components: [35],
		quantities: [1]
	},
	66: {
		id: 66,
		name: '������������ (4)',
		onlyOnce: true,
		upgType: 24,
		quantity: 4,
		components: [24],
		quantities: [4]
	},
	67: {
		id: 67,
		name: '����� � �������',
		onlyOnce: true,
		upgType: 37,
		quantity: 1,
		components: [37],
		quantities: [1]
	},
	68: {
		id: 68,
		name: '�� ���������',
		onlyOnce: true,
		upgType: 38,
		quantity: 1,
		components: [38],
		quantities: [1]
	},
	69: {
		id: 69,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 30,
		quantity: 1,
		components: [30],
		quantities: [1]
	},
	70: {
		id: 70,
		name: '���������� ���� +1',
		onlyOnce: false,
		upgType: 39,
		quantity: 1,
		components: [39],
		quantities: [1]
	},
	71: {
		id: 71,
		name: '�������� ����� +1',
		onlyOnce: false,
		upgType: 41,
		quantity: 1,
		components: [41],
		quantities: [1]
	},
	72: {
		id: 72,
		name: '��������� � ���',
		onlyOnce: true,
		upgType: 108,
		quantity: 1,
		components: [108],
		quantities: [1]
	},
	73: {
		id: 73,
		name: '�����',
		onlyOnce: true,
		upgType: 96,
		quantity: 4,
		components: [96],
		quantities: [4]
	},
	74: {
		id: 74,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 44,
		quantity: 2,
		components: [44],
		quantities: [2]
	},
	75: {
		id: 75,
		name: '����������� �����',
		onlyOnce: true,
		upgType: 45,
		quantity: 3,
		components: [45],
		quantities: [3]
	},
	76: {
		id: 76,
		name: '���������� (4)',
		onlyOnce: true,
		upgType: 46,
		quantity: 4,
		components: [46],
		quantities: [4]
	},
	77: {
		id: 77,
		name: '�������',
		onlyOnce: true,
		upgType: 47,
		quantity: 3,
		components: [47],
		quantities: [3]
	},
	78: {
		id: 78,
		name: '�������� ����� (2)',
		onlyOnce: true,
		upgType: 41,
		quantity: 2,
		components: [41],
		quantities: [2]
	},
	79: {
		id: 79,
		name: '����������� +10',
		onlyOnce: false,
		upgType: 226,
		quantity: 10,
		components: [226],
		quantities: [10]
	},
	80: {
		id: 80,
		name: '���������� +1',
		onlyOnce: false,
		upgType: 50,
		quantity: 1,
		components: [50],
		quantities: [1]
	},
	81: {
		id: 81,
		name: '��������� ���� (2)',
		onlyOnce: true,
		upgType: 127,
		quantity: 2,
		components: [127],
		quantities: [2]
	},
	82: {
		id: 82,
		name: '����������� ������ (3)',
		onlyOnce: true,
		upgType: 52,
		quantity: 3,
		components: [52],
		quantities: [3]
	},
	83: {
		id: 83,
		name: '����������� (2)',
		onlyOnce: true,
		upgType: 53,
		quantity: 2,
		components: [53],
		quantities: [2]
	},
	84: {
		id: 84,
		name: '�������',
		onlyOnce: false,
		upgType: 54,
		quantity: 5,
		components: [54],
		quantities: [5]
	},
	85: {
		id: 85,
		name: '����������� +2',
		onlyOnce: false,
		upgType: 53,
		quantity: 2,
		neededAbilities: [53],
		components: [53],
		quantities: [2]
	},
	86: {
		id: 86,
		name: '������ 10',
		onlyOnce: false,
		upgType: 56,
		quantity: 10,
		components: [56],
		quantities: [10]
	},
	87: {
		id: 87,
		name: '������ 15',
		onlyOnce: false,
		upgType: 56,
		quantity: 15,
		components: [56],
		quantities: [15]
	},
	88: {
		id: 88,
		name: '��������� 10',
		onlyOnce: false,
		upgType: 58,
		quantity: 10,
		components: [58],
		quantities: [10]
	},
	89: {
		id: 89,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 59,
		quantity: 4,
		components: [59],
		quantities: [4]
	},
	90: {
		id: 90,
		name: '������������ ������ 4',
		onlyOnce: true,
		upgType: 60,
		quantity: 4,
		components: [60],
		quantities: [4]
	},
	91: {
		id: 91,
		name: '������������ ������ +1',
		onlyOnce: false,
		upgType: 60,
		quantity: 1,
		neededAbilities: [60],
		components: [60],
		quantities: [1]
	},
	92: {
		id: 92,
		name: '����� ����������',
		onlyOnce: true,
		upgType: 62,
		quantity: 3,
		components: [62],
		quantities: [3]
	},
	93: {
		id: 93,
		name: '������ ������� +1',
		onlyOnce: false,
		upgType: 25,
		quantity: 1,
		neededAbilities: [25],
		components: [25],
		quantities: [1]
	},
	94: {
		id: 94,
		name: '������������ (7)',
		onlyOnce: true,
		upgType: 24,
		quantity: 7,
		components: [24],
		quantities: [7]
	},
	95: {
		id: 95,
		name: '������ ������� (5)',
		onlyOnce: true,
		upgType: 25,
		quantity: 5,
		components: [25],
		quantities: [5]
	},
	96: {
		id: 96,
		name: '������ ������ +3',
		onlyOnce: false,
		upgType: 35,
		quantity: 3,
		components: [35],
		quantities: [3]
	},
	97: {
		id: 97,
		name: '����������� (3)',
		onlyOnce: true,
		upgType: 48,
		quantity: 3,
		components: [48],
		quantities: [3]
	},
	98: {
		id: 98,
		name: '����������� +1',
		onlyOnce: false,
		upgType: 48,
		quantity: 1,
		components: [48],
		quantities: [1]
	},
	99: {
		id: 99,
		name: '����� (4)',
		onlyOnce: true,
		upgType: 55,
		quantity: 4,
		components: [55],
		quantities: [4]
	},
	100: {
		id: 100,
		name: '����� +2',
		onlyOnce: false,
		upgType: 55,
		quantity: 2,
		components: [55],
		quantities: [2]
	},
	101: {
		id: 101,
		name: '����� (6)',
		onlyOnce: true,
		upgType: 55,
		quantity: 6,
		components: [55],
		quantities: [6]
	},
	102: {
		id: 102,
		name: '��������� ���� (5)',
		onlyOnce: true,
		upgType: 57,
		quantity: 5,
		components: [57],
		quantities: [5]
	},
	103: {
		id: 103,
		name: '���������� ��� +1',
		onlyOnce: false,
		upgType: 120,
		quantity: 1,
		components: [120],
		quantities: [1]
	},
	104: {
		id: 104,
		name: '������������ +2',
		onlyOnce: false,
		upgType: 63,
		quantity: 2,
		components: [63],
		quantities: [2]
	},
	105: {
		id: 105,
		name: '������������ (15)',
		onlyOnce: true,
		upgType: 63,
		quantity: 15,
		components: [63],
		quantities: [15]
	},
	106: {
		id: 106,
		name: '�������� �������',
		onlyOnce: false,
		upgType: 178,
		quantity: 1,
		components: [178],
		quantities: [1]
	},
	107: {
		id: 107,
		name: '����� ���� ������',
		onlyOnce: true,
		upgType: 65,
		quantity: 1,
		components: [65],
		quantities: [1]
	},
	108: {
		id: 108,
		name: '�������� ����� (4)',
		onlyOnce: true,
		upgType: 66,
		quantity: 4,
		components: [66],
		quantities: [4]
	},
	109: {
		id: 109,
		name: '�����',
		onlyOnce: false,
		upgType: 67,
		quantity: 3,
		components: [67],
		quantities: [3]
	},
	110: {
		id: 110,
		name: '������ ������ +6',
		onlyOnce: false,
		upgType: 35,
		quantity: 6,
		components: [35],
		quantities: [6]
	},
	111: {
		id: 111,
		name: '������������ (10)',
		onlyOnce: true,
		upgType: 63,
		quantity: 10,
		components: [63],
		quantities: [10]
	},
	112: {
		id: 112,
		name: '���������� (50)',
		onlyOnce: true,
		upgType: 68,
		quantity: 50,
		components: [68],
		quantities: [50]
	},
	113: {
		id: 113,
		name: '���������� (3)',
		onlyOnce: true,
		upgType: 50,
		quantity: 3,
		components: [50],
		quantities: [3]
	},
	114: {
		id: 114,
		name: '������ ������� (3)',
		onlyOnce: true,
		upgType: 69,
		quantity: 3,
		components: [69],
		quantities: [3]
	},
	115: {
		id: 115,
		name: '�����',
		onlyOnce: true,
		upgType: 2010,
		quantity: 1,
		components: [2010],
		quantities: [1]
	},
	116: {
		id: 116,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2019,
		quantity: 2,
		neededAbilities: [310],
		components: [2019],
		quantities: [2]
	},
	117: {
		id: 117,
		name: '�����',
		onlyOnce: true,
		upgType: 2023,
		quantity: 4,
		components: [2023],
		quantities: [4]
	},
	118: {
		id: 118,
		name: '�������������',
		onlyOnce: true,
		upgType: 2011,
		quantity: 1,
		components: [2011],
		quantities: [1]
	},
	119: {
		id: 119,
		name: '���������',
		onlyOnce: true,
		upgType: 2038,
		quantity: 2,
		components: [2038],
		quantities: [2]
	},
	120: {
		id: 120,
		name: '��������� ���',
		onlyOnce: true,
		upgType: 2009,
		quantity: 1,
		components: [2009],
		quantities: [1]
	},
	121: {
		id: 121,
		name: '�����',
		onlyOnce: true,
		upgType: 2013,
		quantity: 1,
		components: [2013],
		quantities: [1]
	},
	122: {
		id: 122,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 2014,
		quantity: 2,
		components: [2014],
		quantities: [2]
	},
	123: {
		id: 123,
		name: '�������������',
		onlyOnce: true,
		upgType: 2003,
		quantity: 1,
		components: [2003],
		quantities: [1]
	},
	124: {
		id: 124,
		name: '����� ����������',
		onlyOnce: true,
		upgType: 310,
		quantity: 1,
		components: [310, 2021],
		quantities: [1, 1]
	},
	125: {
		id: 125,
		name: '������ ���',
		onlyOnce: true,
		upgType: 2016,
		quantity: 1,
		components: [2016],
		quantities: [1]
	},
	126: {
		id: 126,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2057,
		quantity: 3,
		components: [2057],
		quantities: [3]
	},
	127: {
		id: 127,
		name: '������ ���� +1',
		onlyOnce: false,
		upgType: 76,
		quantity: 1,
		components: [76],
		quantities: [1]
	},
	128: {
		id: 128,
		name: '������ ������� +1',
		onlyOnce: false,
		upgType: 77,
		quantity: 1,
		components: [77],
		quantities: [1]
	},
	129: {
		id: 129,
		name: '������ ���� 2',
		onlyOnce: true,
		upgType: 76,
		quantity: 2,
		components: [76],
		quantities: [2]
	},
	130: {
		id: 130,
		name: '������ ������� 2',
		onlyOnce: true,
		upgType: 77,
		quantity: 2,
		components: [77],
		quantities: [2]
	},
	131: {
		id: 131,
		name: '��������� ���� +1',
		onlyOnce: false,
		upgType: 57,
		quantity: 1,
		neededAbilities: [57],
		components: [57],
		quantities: [1]
	},
	132: {
		id: 132,
		name: '�������� ������� +1',
		onlyOnce: false,
		upgType: 42,
		quantity: 1,
		components: [42],
		quantities: [1]
	},
	133: {
		id: 133,
		name: '�������� ����� (4)',
		onlyOnce: true,
		upgType: 41,
		quantity: 4,
		components: [41],
		quantities: [4]
	},
	134: {
		id: 134,
		name: '���� ��������',
		onlyOnce: true,
		upgType: 23,
		quantity: 1,
		components: [23],
		quantities: [1]
	},
	135: {
		id: 135,
		name: '�����',
		onlyOnce: true,
		upgType: 309,
		quantity: 1,
		components: [309, 2013],
		quantities: [1, 1]
	},
	136: {
		id: 136,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2015,
		quantity: 2,
		components: [2015],
		quantities: [2]
	},
	137: {
		id: 137,
		name: '���������� ������',
		onlyOnce: true,
		upgType: 64,
		quantity: 19,
		components: [64],
		quantities: [19]
	},
	138: {
		id: 138,
		name: '���������',
		onlyOnce: true,
		upgType: 2008,
		quantity: 2,
		components: [2008],
		quantities: [2]
	},
	139: {
		id: 139,
		name: '���� ����� +1',
		onlyOnce: false,
		upgType: 49,
		quantity: 1,
		neededAbilities: [217],
		components: [49],
		quantities: [1]
	},
	140: {
		id: 140,
		name: '�� +1',
		onlyOnce: false,
		upgType: 41,
		quantity: 1,
		components: [41],
		quantities: [1]
	},
	141: {
		id: 141,
		name: '���������',
		onlyOnce: true,
		upgType: 2037,
		quantity: 3,
		components: [2037],
		quantities: [3]
	},
	142: {
		id: 142,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2052,
		quantity: 4,
		components: [2052],
		quantities: [4]
	},
	143: {
		id: 143,
		name: '�����������',
		onlyOnce: true,
		upgType: 2049,
		quantity: 4,
		components: [2049],
		quantities: [4]
	},
	144: {
		id: 144,
		name: '����� ���������',
		onlyOnce: true,
		upgType: 2056,
		quantity: 4,
		components: [2056],
		quantities: [4]
	},
	145: {
		id: 145,
		name: '���������� (6)',
		onlyOnce: true,
		upgType: 46,
		quantity: 6,
		components: [46],
		quantities: [6]
	},
	146: {
		id: 146,
		name: '���������� +1',
		onlyOnce: false,
		upgType: 46,
		quantity: 1,
		components: [46, 8],
		quantities: [1, 1]
	},
	147: {
		id: 147,
		name: '�������� ����� (2)',
		onlyOnce: true,
		upgType: 43,
		quantity: 2,
		components: [43],
		quantities: [2]
	},
	148: {
		id: 148,
		name: '�������� ����� +1',
		onlyOnce: false,
		upgType: 43,
		quantity: 1,
		components: [43],
		quantities: [1]
	},
	149: {
		id: 149,
		name: '�������� ������� (2)',
		onlyOnce: true,
		upgType: 95,
		quantity: 2,
		components: [95],
		quantities: [2]
	},
	150: {
		id: 150,
		name: '�������� ������� +1',
		onlyOnce: false,
		upgType: 95,
		quantity: 1,
		components: [95],
		quantities: [1]
	},
	151: {
		id: 151,
		name: '�������� ������� (2)',
		onlyOnce: true,
		upgType: 42,
		quantity: 2,
		components: [42],
		quantities: [2]
	},
	152: {
		id: 152,
		name: '������������',
		onlyOnce: true,
		upgType: 97,
		quantity: 1,
		components: [97],
		quantities: [1]
	},
	153: {
		id: 153,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2028,
		quantity: 3,
		components: [2028],
		quantities: [3]
	},
	154: {
		id: 154,
		name: '�������� ��������',
		onlyOnce: true,
		upgType: 2055,
		quantity: 7,
		components: [2055],
		quantities: [7]
	},
	155: {
		id: 155,
		name: '�������',
		onlyOnce: true,
		upgType: 2012,
		quantity: 1,
		components: [2012],
		quantities: [1]
	},
	156: {
		id: 156,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2078,
		quantity: 2,
		components: [2078, 2079, 2080],
		quantities: [2, 3, 1]
	},
	157: {
		id: 157,
		name: '������� ����� +1',
		onlyOnce: false,
		upgType: 210,
		quantity: 1,
		components: [210, 25],
		quantities: [1, 1]
	},
	158: {
		id: 158,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 122,
		quantity: 1,
		components: [122],
		quantities: [1]
	},
	159: {
		id: 159,
		name: '����������� ������',
		onlyOnce: true,
		upgType: 2081,
		quantity: 1,
		components: [2081],
		quantities: [1]
	},
	160: {
		id: 160,
		name: '�������� ������ 1',
		onlyOnce: true,
		upgType: 125,
		quantity: 1,
		components: [125],
		quantities: [1]
	},
	161: {
		id: 161,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 2082,
		quantity: 3,
		components: [2082, 147, 51],
		quantities: [3, 1, 1]
	},
	162: {
		id: 162,
		name: '���������� ������',
		onlyOnce: true,
		upgType: 64,
		quantity: 174,
		neededAbilities: [64],
		components: [64],
		quantities: [174]
	},
	163: {
		id: 163,
		name: '����������� (4)',
		onlyOnce: true,
		upgType: 53,
		quantity: 4,
		components: [53],
		quantities: [4]
	},
	164: {
		id: 164,
		name: '�������� 1',
		onlyOnce: false,
		upgType: 49,
		quantity: 1,
		components: [49, 217],
		quantities: [1, 1]
	},
	165: {
		id: 165,
		name: '�������� 2',
		onlyOnce: false,
		upgType: 49,
		quantity: 2,
		components: [49, 217],
		quantities: [2, 1]
	},
	166: {
		id: 166,
		name: '������������ �������',
		onlyOnce: true,
		upgType: 20,
		quantity: -2,
		neededAbilities: [20],
		components: [20],
		quantities: [-2]
	},
	167: {
		id: 167,
		name: '������ ���',
		onlyOnce: false,
		upgType: 2083,
		quantity: 2,
		components: [2083],
		quantities: [2]
	},
	168: {
		id: 168,
		name: '������ ����� 1',
		onlyOnce: true,
		upgType: 221,
		quantity: 1,
		components: [221],
		quantities: [1]
	},
	169: {
		id: 169,
		name: '������ ����� 2',
		onlyOnce: true,
		upgType: 221,
		quantity: 2,
		components: [221],
		quantities: [2]
	},
	170: {
		id: 170,
		name: '��������� +1',
		onlyOnce: false,
		upgType: 222,
		quantity: 1,
		components: [222],
		quantities: [1]
	},
	171: {
		id: 171,
		name: '��������� +2',
		onlyOnce: true,
		upgType: 222,
		quantity: 2,
		components: [222],
		quantities: [2]
	},
	172: {
		id: 172,
		name: '������ ��� �������',
		onlyOnce: false,
		upgType: 220,
		quantity: 1,
		components: [220, 908, 355, 295],
		quantities: [1, 10, 1, 1]
	},
	173: {
		id: 173,
		name: '�������� ������� (3)',
		onlyOnce: true,
		upgType: 42,
		quantity: 3,
		components: [42],
		quantities: [3]
	},
	174: {
		id: 174,
		name: '�������� ����� (3)',
		onlyOnce: true,
		upgType: 41,
		quantity: 3,
		components: [41],
		quantities: [3]
	},
	175: {
		id: 175,
		name: '�������������� ��������',
		onlyOnce: false,
		upgType: 227,
		quantity: 2,
		components: [227],
		quantities: [2]
	},
	176: {
		id: 176,
		name: '������������ ������� +1',
		onlyOnce: false,
		upgType: 227,
		quantity: 1,
		components: [227],
		quantities: [1]
	},
	177: {
		id: 177,
		name: '������ ������ +5',
		onlyOnce: false,
		upgType: 35,
		quantity: 5,
		components: [35],
		quantities: [5]
	},
	178: {
		id: 178,
		name: '�����������',
		onlyOnce: false,
		upgType: 226,
		quantity: 5,
		components: [226, 227],
		quantities: [5, 2]
	},
	179: {
		id: 179,
		name: '�������� ���������',
		onlyOnce: false,
		upgType: 130,
		quantity: 362,
		components: [130, 344],
		quantities: [362, 1]
	},
	180: {
		id: 180,
		name: '�������� �������',
		onlyOnce: false,
		upgType: 130,
		quantity: -332,
		neededAbilities: [344],
		components: [130, 344, 345],
		quantities: [-332, -1, 1]
	},
	181: {
		id: 181,
		name: '�������� �������',
		onlyOnce: false,
		upgType: 176,
		quantity: 1,
		components: [176],
		quantities: [1]
	},
	182: {
		id: 182,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 2084,
		quantity: 4,
		components: [2084],
		quantities: [4]
	},
	183: {
		id: 183,
		name: '����� (8)',
		onlyOnce: true,
		upgType: 55,
		quantity: 8,
		components: [55],
		quantities: [8]
	},
	184: {
		id: 184,
		name: '�������� ����� (2)',
		onlyOnce: true,
		upgType: 66,
		quantity: 2,
		components: [66],
		quantities: [2]
	},
	185: {
		id: 185,
		name: '����������������',
		onlyOnce: true,
		upgType: 2085,
		quantity: 1,
		components: [2085],
		quantities: [1]
	},
	186: {
		id: 186,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2086,
		quantity: 3,
		components: [2086],
		quantities: [3]
	},
	187: {
		id: 187,
		name: '������ ������',
		onlyOnce: true,
		upgType: 2087,
		quantity: 2,
		components: [2087],
		quantities: [2]
	},
	188: {
		id: 188,
		name: '������',
		onlyOnce: true,
		upgType: 2088,
		quantity: 3,
		components: [2088],
		quantities: [3]
	},
	189: {
		id: 189,
		name: '����������',
		onlyOnce: true,
		upgType: 2089,
		quantity: 4,
		components: [2089],
		quantities: [4]
	},
	190: {
		id: 190,
		name: '���������� �����',
		onlyOnce: true,
		upgType: 2090,
		quantity: 4,
		components: [2090],
		quantities: [4]
	},
	191: {
		id: 191,
		name: '���������',
		onlyOnce: true,
		upgType: 2091,
		quantity: 2,
		components: [2091],
		quantities: [2]
	},
	192: {
		id: 192,
		name: '��������� � ����',
		onlyOnce: false,
		upgType: 101,
		quantity: 1,
		components: [101],
		quantities: [1]
	},
	193: {
		id: 193,
		name: '������ ������',
		onlyOnce: true,
		upgType: 2092,
		quantity: 3,
		neededAbilities: [366],
		components: [2092],
		quantities: [3]
	},
	194: {
		id: 194,
		name: '��������� ���',
		onlyOnce: true,
		upgType: 2093,
		quantity: 2,
		neededAbilities: [366],
		components: [2093],
		quantities: [2]
	},
	195: {
		id: 195,
		name: '�������� ��� ��',
		onlyOnce: true,
		upgType: 2094,
		quantity: 1,
		neededAbilities: [366],
		components: [2094],
		quantities: [1]
	},
	196: {
		id: 196,
		name: '�������� ���� ��',
		onlyOnce: true,
		upgType: 2095,
		quantity: 5,
		neededAbilities: [366],
		components: [2095],
		quantities: [5]
	},
	197: {
		id: 197,
		name: '���� �����',
		onlyOnce: true,
		upgType: 2096,
		quantity: 1,
		components: [2096],
		quantities: [1]
	},
	198: {
		id: 198,
		name: '��������� �������',
		onlyOnce: true,
		upgType: 140,
		quantity: 2,
		components: [140],
		quantities: [2]
	},
	199: {
		id: 199,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 141,
		quantity: 1,
		components: [141],
		quantities: [1]
	},
	200: {
		id: 200,
		name: '���� ����������',
		onlyOnce: true,
		upgType: 2099,
		quantity: 3,
		components: [2099],
		quantities: [3]
	},
	201: {
		id: 201,
		name: '���� �������',
		onlyOnce: true,
		upgType: 2100,
		quantity: 1,
		components: [2100],
		quantities: [1]
	},
	202: {
		id: 202,
		name: '���� ��������������',
		onlyOnce: true,
		upgType: 2101,
		quantity: 2,
		components: [2101],
		quantities: [2]
	},
	203: {
		id: 203,
		name: '���� ������',
		onlyOnce: true,
		upgType: 2102,
		quantity: 3,
		components: [2102],
		quantities: [3]
	},
	204: {
		id: 204,
		name: '�������� ����',
		onlyOnce: true,
		upgType: 2103,
		quantity: 1,
		components: [2103],
		quantities: [1]
	},
	205: {
		id: 205,
		name: '������� �������',
		onlyOnce: true,
		upgType: 2104,
		quantity: 1,
		components: [2104],
		quantities: [1]
	},
	206: {
		id: 206,
		name: '��� �����',
		onlyOnce: true,
		upgType: 2105,
		quantity: 1,
		components: [2105],
		quantities: [1]
	},
	207: {
		id: 207,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 2106,
		quantity: 1,
		components: [2106],
		quantities: [1]
	},
	208: {
		id: 208,
		name: '������',
		onlyOnce: true,
		upgType: 2107,
		quantity: 1,
		components: [2107],
		quantities: [1]
	},
	209: {
		id: 209,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2367,
		quantity: 1,
		components: [2367],
		quantities: [1]
	},
	210: {
		id: 210,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2109,
		quantity: 1,
		components: [2109],
		quantities: [1]
	},
	211: {
		id: 211,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2110,
		quantity: 1,
		components: [2110],
		quantities: [1]
	},
	212: {
		id: 212,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2111,
		quantity: 1,
		components: [2111],
		quantities: [1]
	},
	213: {
		id: 213,
		name: '��������� ������� +1',
		onlyOnce: false,
		upgType: 911,
		quantity: 1,
		components: [911],
		quantities: [1]
	},
	214: {
		id: 214,
		name: '�������',
		onlyOnce: true,
		upgType: 2112,
		quantity: 2,
		components: [2112],
		quantities: [2]
	},
	215: {
		id: 215,
		name: '����������',
		onlyOnce: true,
		upgType: 2113,
		quantity: 2,
		components: [2113],
		quantities: [2]
	},
	216: {
		id: 216,
		name: '���������� �����',
		onlyOnce: true,
		upgType: 2114,
		quantity: 2,
		components: [2114],
		quantities: [2]
	},
	217: {
		id: 217,
		name: '����',
		onlyOnce: true,
		upgType: 2115,
		quantity: 4,
		components: [2115],
		quantities: [4]
	},
	218: {
		id: 218,
		name: '������',
		onlyOnce: true,
		upgType: 2116,
		quantity: 4,
		components: [2116],
		quantities: [4]
	},
	219: {
		id: 219,
		name: '����� ���� - ���������',
		onlyOnce: true,
		upgType: 2117,
		quantity: 3,
		components: [2117],
		quantities: [3]
	},
	220: {
		id: 220,
		name: '����� ���� - ���������',
		onlyOnce: true,
		upgType: 2118,
		quantity: 3,
		components: [2118],
		quantities: [3]
	},
	221: {
		id: 221,
		name: '����� ���� - ����',
		onlyOnce: true,
		upgType: 2119,
		quantity: 3,
		components: [2119],
		quantities: [3]
	},
	222: {
		id: 222,
		name: '������ ���� ����',
		onlyOnce: true,
		upgType: 2120,
		quantity: 4,
		components: [2120],
		quantities: [4]
	},
	223: {
		id: 223,
		name: '������� ������',
		onlyOnce: true,
		upgType: 2121,
		quantity: 1,
		components: [2121],
		quantities: [1]
	},
	224: {
		id: 224,
		name: '���������',
		onlyOnce: true,
		upgType: 2122,
		quantity: 2,
		components: [2122],
		quantities: [2]
	},
	225: {
		id: 225,
		name: '������� �����',
		onlyOnce: true,
		upgType: 2123,
		quantity: 3,
		components: [2123],
		quantities: [3]
	},
	226: {
		id: 226,
		name: '�����',
		onlyOnce: false,
		upgType: 341,
		quantity: 1,
		components: [341, 255, 258],
		quantities: [1, 136, 1]
	},
	227: {
		id: 227,
		name: '������',
		onlyOnce: false,
		upgType: 340,
		quantity: 1,
		components: [340, 255, 256, 257],
		quantities: [1, 135, 1, 3]
	},
	228: {
		id: 228,
		name: '������ �������',
		onlyOnce: false,
		upgType: 342,
		quantity: 1,
		components: [342, 255],
		quantities: [1, 137]
	},
	229: {
		id: 229,
		name: '����� ������������',
		onlyOnce: true,
		upgType: 62,
		quantity: 7,
		components: [62],
		quantities: [7]
	},
	230: {
		id: 230,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 62,
		quantity: 4,
		components: [62],
		quantities: [4]
	},
	231: {
		id: 231,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 62,
		quantity: 9,
		components: [62],
		quantities: [9]
	},
	232: {
		id: 232,
		name: '����� �������',
		onlyOnce: true,
		upgType: 62,
		quantity: 8,
		components: [62],
		quantities: [8]
	},
	233: {
		id: 233,
		name: '����� ���������',
		onlyOnce: true,
		upgType: 62,
		quantity: 6,
		components: [62],
		quantities: [6]
	},
	234: {
		id: 234,
		name: '����� �����������',
		onlyOnce: true,
		upgType: 62,
		quantity: 11,
		components: [62],
		quantities: [11]
	},
	235: {
		id: 235,
		name: '����� �������',
		onlyOnce: true,
		upgType: 62,
		quantity: 12,
		components: [62],
		quantities: [12]
	},
	236: {
		id: 236,
		name: '����� ���������',
		onlyOnce: true,
		upgType: 62,
		quantity: 2,
		components: [62],
		quantities: [2]
	},
	237: {
		id: 237,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 62,
		quantity: 5,
		components: [62],
		quantities: [5]
	},
	238: {
		id: 238,
		name: '����� �����',
		onlyOnce: true,
		upgType: 62,
		quantity: 10,
		components: [62],
		quantities: [10]
	},
	239: {
		id: 239,
		name: '����������� �������',
		onlyOnce: true,
		upgType: 2124,
		quantity: 1,
		components: [2124],
		quantities: [1]
	},
	240: {
		id: 240,
		name: '������ �����',
		onlyOnce: true,
		upgType: 2125,
		quantity: 4,
		components: [2125],
		quantities: [4]
	},
	241: {
		id: 241,
		name: '������� �������',
		onlyOnce: true,
		upgType: 2126,
		quantity: 2,
		components: [2126],
		quantities: [2]
	},
	242: {
		id: 242,
		name: '������������ ������',
		onlyOnce: true,
		upgType: 2127,
		quantity: 2,
		components: [2127],
		quantities: [2]
	},
	243: {
		id: 243,
		name: '������ �������',
		onlyOnce: true,
		upgType: 2128,
		quantity: 1,
		components: [2128],
		quantities: [1]
	},
	244: {
		id: 244,
		name: '��� �������',
		onlyOnce: true,
		upgType: 2129,
		quantity: 1,
		components: [2129],
		quantities: [1]
	},
	245: {
		id: 245,
		name: '���� ����',
		onlyOnce: true,
		upgType: 2130,
		quantity: 1,
		components: [2130, 367],
		quantities: [1, 1]
	},
	246: {
		id: 246,
		name: '����������',
		onlyOnce: true,
		upgType: 2131,
		quantity: 2,
		neededAbilities: [367],
		components: [2131],
		quantities: [2]
	},
	247: {
		id: 247,
		name: '�����',
		onlyOnce: true,
		upgType: 2132,
		quantity: 3,
		neededAbilities: [367],
		components: [2132],
		quantities: [3]
	},
	248: {
		id: 248,
		name: '��� �������',
		onlyOnce: true,
		upgType: 2137,
		quantity: 1,
		components: [2137],
		quantities: [1]
	},
	249: {
		id: 249,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 2133,
		quantity: 2,
		components: [2133],
		quantities: [2]
	},
	250: {
		id: 250,
		name: '���������� ������',
		onlyOnce: true,
		upgType: 2370,
		quantity: 1,
		components: [2370],
		quantities: [1]
	},
	251: {
		id: 251,
		name: '���������� �����',
		onlyOnce: true,
		upgType: 2371,
		quantity: 1,
		components: [2371],
		quantities: [1]
	},
	252: {
		id: 252,
		name: '���������� ����',
		onlyOnce: true,
		upgType: 2372,
		quantity: 1,
		components: [2372],
		quantities: [1]
	},
	253: {
		id: 253,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2134,
		quantity: 2,
		components: [2134],
		quantities: [2]
	},
	254: {
		id: 254,
		name: '�������������',
		onlyOnce: true,
		upgType: 2138,
		quantity: 1,
		components: [2138],
		quantities: [1]
	},
	255: {
		id: 255,
		name: '���������',
		onlyOnce: true,
		upgType: 2140,
		quantity: 1,
		components: [2140],
		quantities: [1]
	},
	256: {
		id: 256,
		name: '���� ������� +1',
		onlyOnce: false,
		upgType: 907,
		quantity: 1,
		components: [907],
		quantities: [1]
	},
	257: {
		id: 257,
		name: '������������� +1',
		onlyOnce: false,
		upgType: 32,
		quantity: 1,
		components: [32, 33, 34],
		quantities: [1, 1, 1]
	},
	258: {
		id: 258,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2039,
		quantity: 2,
		components: [2039],
		quantities: [2]
	},
	259: {
		id: 259,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2051,
		quantity: 3,
		components: [2051],
		quantities: [3]
	},
	260: {
		id: 260,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2050,
		quantity: 4,
		components: [2050],
		quantities: [4]
	},
	261: {
		id: 261,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2058,
		quantity: 5,
		components: [2058],
		quantities: [5]
	},
	262: {
		id: 262,
		name: '���������������',
		onlyOnce: true,
		upgType: 2141,
		quantity: 2,
		components: [2141],
		quantities: [2]
	},
	263: {
		id: 263,
		name: '�������� ����',
		onlyOnce: true,
		upgType: 2142,
		quantity: 2,
		components: [2142],
		quantities: [2]
	},
	264: {
		id: 264,
		name: '������ �������',
		onlyOnce: true,
		upgType: 2143,
		quantity: 1,
		components: [2143],
		quantities: [1]
	},
	265: {
		id: 265,
		name: '���������',
		onlyOnce: true,
		upgType: 2144,
		quantity: 2,
		components: [2144],
		quantities: [2]
	},
	266: {
		id: 266,
		name: '������������ ������',
		onlyOnce: true,
		upgType: 2145,
		quantity: 3,
		components: [2145],
		quantities: [3]
	},
	267: {
		id: 267,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2146,
		quantity: 2,
		components: [2146],
		quantities: [2]
	},
	268: {
		id: 268,
		name: '�������� �����������',
		onlyOnce: true,
		upgType: 2147,
		quantity: 4,
		components: [2147, 215],
		quantities: [4, 1]
	},
	269: {
		id: 269,
		name: '���������',
		onlyOnce: true,
		upgType: 2148,
		quantity: 4,
		components: [2148],
		quantities: [4]
	},
	270: {
		id: 270,
		name: '������������ ������',
		onlyOnce: true,
		upgType: 2149,
		quantity: 3,
		components: [2149],
		quantities: [3]
	},
	271: {
		id: 271,
		name: '������������ ����',
		onlyOnce: true,
		upgType: 2150,
		quantity: 5,
		components: [2150],
		quantities: [5]
	},
	272: {
		id: 272,
		name: '������ �����',
		onlyOnce: true,
		upgType: 2151,
		quantity: 5,
		components: [2151],
		quantities: [5]
	},
	273: {
		id: 273,
		name: '������������ �����',
		onlyOnce: true,
		upgType: 2152,
		quantity: 5,
		components: [2152],
		quantities: [5]
	},
	274: {
		id: 274,
		name: '���� � �������',
		onlyOnce: true,
		upgType: 240,
		quantity: 1,
		components: [240],
		quantities: [1]
	},
	275: {
		id: 275,
		name: '��������',
		onlyOnce: true,
		upgType: 2139,
		quantity: 2,
		components: [2139],
		quantities: [2]
	},
	276: {
		id: 276,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2373,
		quantity: 3,
		components: [2373],
		quantities: [3]
	},
	277: {
		id: 277,
		name: '������',
		onlyOnce: false,
		upgType: 216,
		quantity: 1,
		components: [216],
		quantities: [1]
	},
	278: {
		id: 278,
		name: '���������� ��� +2',
		onlyOnce: false,
		upgType: 120,
		quantity: 2,
		components: [120],
		quantities: [2]
	},
	279: {
		id: 279,
		name: '���������� ��� +3',
		onlyOnce: false,
		upgType: 120,
		quantity: 3,
		components: [120],
		quantities: [3]
	},
	280: {
		id: 280,
		name: '����� �����������',
		onlyOnce: true,
		upgType: 62,
		quantity: 144,
		neededAbilities: [309],
		components: [62],
		quantities: [144]
	},
	281: {
		id: 281,
		name: '����� �����������',
		onlyOnce: true,
		upgType: 62,
		quantity: 190,
		neededAbilities: [310],
		components: [62],
		quantities: [190]
	},
	282: {
		id: 282,
		name: '������� +1',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		neededAbilities: [47],
		components: [8, 47],
		quantities: [1, 1]
	},
	283: {
		id: 283,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 336,
		quantity: 1,
		components: [336, 48],
		quantities: [1, 1]
	},
	284: {
		id: 284,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 336,
		quantity: -1,
		neededAbilities: [336],
		components: [336, 337, 195],
		quantities: [-1, 2, 1]
	},
	285: {
		id: 285,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 337,
		quantity: -2,
		neededAbilities: [337],
		components: [337, 338, 48],
		quantities: [-2, 3, 1]
	},
	286: {
		id: 286,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 338,
		quantity: -3,
		neededAbilities: [338],
		components: [338, 339, 48, 195],
		quantities: [-3, 4, 1, 1]
	},
	287: {
		id: 287,
		name: '��������',
		onlyOnce: true,
		upgType: 18,
		quantity: 1,
		components: [18, 19, 38, 108],
		quantities: [1, 1, 1, 1]
	},
	288: {
		id: 288,
		name: '������ ��������',
		onlyOnce: true,
		upgType: 22,
		quantity: 1,
		components: [22, 2156, 905, 906],
		quantities: [1, 4, 1, 1]
	},
	289: {
		id: 289,
		name: '���������� ���',
		onlyOnce: false,
		upgType: 27,
		quantity: 1,
		components: [27, 28],
		quantities: [1, 1]
	},
	290: {
		id: 290,
		name: '���������� ���',
		onlyOnce: true,
		upgType: 2074,
		quantity: 5,
		components: [2074],
		quantities: [5]
	},
	291: {
		id: 291,
		name: '������������ (10)',
		onlyOnce: true,
		upgType: 24,
		quantity: 10,
		components: [24],
		quantities: [10]
	},
	292: {
		id: 292,
		name: '������ ���������',
		onlyOnce: true,
		upgType: 27,
		quantity: 1,
		components: [27, 28, 22, 120, 2051],
		quantities: [1, 1, 1, 1, 3]
	},
	293: {
		id: 293,
		name: '���������� ��������',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		components: [8, 77],
		quantities: [1, 1]
	},
	294: {
		id: 294,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2154,
		quantity: 4,
		components: [2154, 907],
		quantities: [4, 4]
	},
	295: {
		id: 295,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 30,
		quantity: 1,
		components: [30],
		quantities: [1]
	},
	296: {
		id: 296,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2155,
		quantity: 5,
		components: [2155],
		quantities: [5]
	},
	297: {
		id: 297,
		name: '������� ������',
		onlyOnce: true,
		upgType: 2156,
		quantity: 4,
		components: [2156],
		quantities: [4]
	},
	298: {
		id: 298,
		name: '������',
		onlyOnce: true,
		upgType: 2157,
		quantity: 5,
		components: [2157],
		quantities: [5]
	},
	299: {
		id: 299,
		name: '��������� � ������',
		onlyOnce: false,
		upgType: 102,
		quantity: 1,
		components: [102],
		quantities: [1]
	},
	300: {
		id: 300,
		name: '��������� � �������',
		onlyOnce: false,
		upgType: 103,
		quantity: 1,
		components: [103],
		quantities: [1]
	},
	301: {
		id: 301,
		name: '�������� ������ 2',
		onlyOnce: true,
		upgType: 125,
		quantity: 2,
		components: [125],
		quantities: [2]
	},
	302: {
		id: 302,
		name: '��������� ����',
		onlyOnce: true,
		upgType: 34,
		quantity: 2,
		components: [34, 102, 2121, 2122, 2123],
		quantities: [2, 1, 1, 3, 2]
	},
	303: {
		id: 303,
		name: '�������������',
		onlyOnce: true,
		upgType: 139,
		quantity: 2,
		components: [139],
		quantities: [2]
	},
	304: {
		id: 304,
		name: '��������� � ���������',
		onlyOnce: false,
		upgType: 104,
		quantity: 1,
		components: [104],
		quantities: [1]
	},
	305: {
		id: 305,
		name: '���������',
		onlyOnce: false,
		upgType: 104,
		quantity: 1,
		components: [104, 13, 19, 108],
		quantities: [1, 1, 1, 1]
	},
	306: {
		id: 306,
		name: '�������� ����',
		onlyOnce: true,
		upgType: 2159,
		quantity: 2,
		components: [2159],
		quantities: [2]
	},
	307: {
		id: 307,
		name: '���� ������',
		onlyOnce: true,
		upgType: 2160,
		quantity: 4,
		components: [2160],
		quantities: [4]
	},
	308: {
		id: 308,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2007,
		quantity: 1,
		components: [2007],
		quantities: [1]
	},
	309: {
		id: 309,
		name: '��������� �������',
		onlyOnce: true,
		upgType: 21,
		quantity: 2,
		components: [21, 103, 2159, 2160, 2007],
		quantities: [2, 1, 2, 4, 1]
	},
	310: {
		id: 310,
		name: '��������������',
		onlyOnce: true,
		upgType: 2162,
		quantity: 1,
		components: [2162],
		quantities: [1]
	},
	311: {
		id: 311,
		name: '������� �����',
		onlyOnce: true,
		upgType: 2163,
		quantity: 3,
		components: [2163],
		quantities: [3]
	},
	312: {
		id: 312,
		name: '�������������',
		onlyOnce: true,
		upgType: 2164,
		quantity: 5,
		components: [2164],
		quantities: [5]
	},
	313: {
		id: 313,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2162,
		quantity: 1,
		components: [2162, 2163, 2164, 39],
		quantities: [1, 3, 5, 2]
	},
	314: {
		id: 314,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2165,
		quantity: 2,
		components: [2165],
		quantities: [2]
	},
	315: {
		id: 315,
		name: '����� �������������',
		onlyOnce: true,
		upgType: 2166,
		quantity: 4,
		components: [2166],
		quantities: [4]
	},
	316: {
		id: 316,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2167,
		quantity: 1,
		components: [2167],
		quantities: [1]
	},
	317: {
		id: 317,
		name: '��������� ����',
		onlyOnce: true,
		upgType: 2165,
		quantity: 2,
		components: [2165, 2166, 2167, 146],
		quantities: [2, 4, 1, 1]
	},
	318: {
		id: 318,
		name: '������ ���� �������',
		onlyOnce: true,
		upgType: 2168,
		quantity: 4,
		components: [2168],
		quantities: [4]
	},
	319: {
		id: 319,
		name: '������ ���� �����',
		onlyOnce: true,
		upgType: 2169,
		quantity: 4,
		components: [2169, 10],
		quantities: [4, 1]
	},
	320: {
		id: 320,
		name: '���� �����',
		onlyOnce: true,
		upgType: 2368,
		quantity: 1,
		components: [2368],
		quantities: [1]
	},
	321: {
		id: 321,
		name: '������ ����������',
		onlyOnce: true,
		upgType: 27,
		quantity: 1,
		components: [27, 28, 96, 2153],
		quantities: [1, 1, 5, 3]
	},
	322: {
		id: 322,
		name: '�������� ����� ������',
		onlyOnce: true,
		upgType: 2171,
		quantity: 6,
		components: [2171],
		quantities: [6]
	},
	323: {
		id: 323,
		name: '����� ������� ������',
		onlyOnce: true,
		upgType: 2172,
		quantity: 2,
		components: [2172],
		quantities: [2]
	},
	324: {
		id: 324,
		name: '����� � �������',
		onlyOnce: true,
		upgType: 37,
		quantity: 2,
		components: [37],
		quantities: [2]
	},
	325: {
		id: 325,
		name: '���� ���������',
		onlyOnce: true,
		upgType: 2173,
		quantity: 3,
		components: [2173],
		quantities: [3]
	},
	326: {
		id: 326,
		name: '����� ������ ������',
		onlyOnce: true,
		upgType: 2035,
		quantity: 1,
		components: [2035],
		quantities: [1]
	},
	327: {
		id: 327,
		name: '���������� �����',
		onlyOnce: true,
		upgType: 2308,
		quantity: 2,
		components: [2308, 57, 274, 121, 283],
		quantities: [2, 15, 1, 2, 1]
	},
	328: {
		id: 328,
		name: '����� �������������',
		onlyOnce: true,
		upgType: 2120,
		quantity: 4,
		components: [2120, 2168, 2169, 2170],
		quantities: [4, 4, 4, 4]
	},
	329: {
		id: 329,
		name: '��������� � �������',
		onlyOnce: true,
		upgType: 2174,
		quantity: 2,
		components: [2174],
		quantities: [2]
	},
	330: {
		id: 330,
		name: '��������� � �����',
		onlyOnce: true,
		upgType: 2175,
		quantity: 3,
		components: [2175],
		quantities: [3]
	},
	331: {
		id: 331,
		name: '��������� � �����',
		onlyOnce: true,
		upgType: 2176,
		quantity: 4,
		components: [2176],
		quantities: [4]
	},
	332: {
		id: 332,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 2024,
		quantity: 1,
		components: [2024],
		quantities: [1]
	},
	333: {
		id: 333,
		name: '����� ������ ���',
		onlyOnce: true,
		upgType: 2048,
		quantity: 4,
		components: [2048],
		quantities: [4]
	},
	334: {
		id: 334,
		name: '������ �������',
		onlyOnce: true,
		upgType: 2177,
		quantity: 3,
		components: [2177],
		quantities: [3]
	},
	335: {
		id: 335,
		name: '��������',
		onlyOnce: true,
		upgType: 2178,
		quantity: 3,
		components: [2178],
		quantities: [3]
	},
	336: {
		id: 336,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2179,
		quantity: 2,
		components: [2179],
		quantities: [2]
	},
	337: {
		id: 337,
		name: '������',
		onlyOnce: true,
		upgType: 2180,
		quantity: 3,
		components: [2180],
		quantities: [3]
	},
	338: {
		id: 338,
		name: '������������ ������',
		onlyOnce: true,
		upgType: 2181,
		quantity: 4,
		components: [2181],
		quantities: [4]
	},
	339: {
		id: 339,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2177,
		quantity: 3,
		components: [2177, 2178, 2179, 2182, 2181],
		quantities: [3, 3, 2, 4, 4]
	},
	340: {
		id: 340,
		name: '��������� ����',
		onlyOnce: true,
		upgType: 2182,
		quantity: 4,
		components: [2182],
		quantities: [4]
	},
	341: {
		id: 341,
		name: '������� �����',
		onlyOnce: true,
		upgType: 2183,
		quantity: 2,
		components: [2183],
		quantities: [2]
	},
	342: {
		id: 342,
		name: '��������� ����',
		onlyOnce: true,
		upgType: 35,
		quantity: 3,
		components: [35, 59, 120, 2301],
		quantities: [3, 4, 2, 3]
	},
	343: {
		id: 343,
		name: '������ ������',
		onlyOnce: true,
		upgType: 22,
		quantity: 1,
		components: [22, 24, 28, 2003],
		quantities: [1, 6, 1, 1]
	},
	344: {
		id: 344,
		name: '��������� 30',
		onlyOnce: false,
		upgType: 214,
		quantity: 30,
		components: [214],
		quantities: [30]
	},
	345: {
		id: 345,
		name: '��������� +20',
		onlyOnce: false,
		upgType: 214,
		quantity: 20,
		neededAbilities: [214],
		components: [214],
		quantities: [20]
	},
	346: {
		id: 346,
		name: '������������ 5',
		onlyOnce: true,
		upgType: 63,
		quantity: 5,
		components: [63],
		quantities: [5]
	},
	347: {
		id: 347,
		name: '������������ +5',
		onlyOnce: false,
		upgType: 63,
		quantity: 5,
		components: [63],
		quantities: [5]
	},
	348: {
		id: 348,
		name: '������ �����',
		onlyOnce: true,
		upgType: 2184,
		quantity: 4,
		neededAbilities: [367],
		components: [2184],
		quantities: [4]
	},
	349: {
		id: 349,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 2185,
		quantity: 4,
		components: [2185],
		quantities: [4]
	},
	350: {
		id: 350,
		name: '�����������',
		onlyOnce: true,
		upgType: 142,
		quantity: 1,
		components: [142],
		quantities: [1]
	},
	351: {
		id: 351,
		name: '��������',
		onlyOnce: true,
		upgType: 2187,
		quantity: 1,
		components: [2187],
		quantities: [1]
	},
	352: {
		id: 352,
		name: '�� ���������',
		onlyOnce: true,
		upgType: 88,
		quantity: 1,
		components: [88],
		quantities: [1]
	},
	353: {
		id: 353,
		name: '���������',
		onlyOnce: true,
		upgType: 2188,
		quantity: 2,
		components: [2188],
		quantities: [2]
	},
	354: {
		id: 354,
		name: '������� ����',
		onlyOnce: true,
		upgType: 2189,
		quantity: 3,
		components: [2189],
		quantities: [3]
	},
	355: {
		id: 355,
		name: '����������� �������',
		onlyOnce: true,
		upgType: 2190,
		quantity: 10,
		components: [2190],
		quantities: [10]
	},
	356: {
		id: 356,
		name: '��������� ���� (3)',
		onlyOnce: true,
		upgType: 57,
		quantity: 3,
		components: [57],
		quantities: [3]
	},
	357: {
		id: 357,
		name: '����� ������',
		onlyOnce: true,
		upgType: 62,
		quantity: 170,
		components: [62],
		quantities: [170]
	},
	358: {
		id: 358,
		name: '��������������� (2)',
		onlyOnce: true,
		upgType: 169,
		quantity: 2,
		components: [169],
		quantities: [2]
	},
	359: {
		id: 359,
		name: '��������������� +1',
		onlyOnce: false,
		upgType: 169,
		quantity: 1,
		neededAbilities: [169],
		components: [169],
		quantities: [1]
	},
	360: {
		id: 360,
		name: '������� �����',
		onlyOnce: false,
		upgType: 25,
		quantity: 1,
		components: [25, 210],
		quantities: [1, 2]
	},
	361: {
		id: 361,
		name: '������',
		onlyOnce: true,
		upgType: 14,
		quantity: 1,
		components: [14, 27, 57, 2191],
		quantities: [1, 1, 8, 1]
	},
	362: {
		id: 362,
		name: '���������',
		onlyOnce: true,
		upgType: 2047,
		quantity: 1,
		components: [2047],
		quantities: [1]
	},
	363: {
		id: 363,
		name: '������',
		onlyOnce: true,
		upgType: 18,
		quantity: 1,
		components: [18, 19, 108, 13, 104],
		quantities: [1, 1, 1, 1, 1]
	},
	364: {
		id: 364,
		name: '���� ������� +4',
		onlyOnce: false,
		upgType: 907,
		quantity: 4,
		components: [907, 911],
		quantities: [4, 1]
	},
	365: {
		id: 365,
		name: '�������',
		onlyOnce: true,
		upgType: 2192,
		quantity: 4,
		components: [2192],
		quantities: [4]
	},
	366: {
		id: 366,
		name: '�������� ���',
		onlyOnce: true,
		upgType: 2193,
		quantity: 6,
		components: [2193],
		quantities: [6]
	},
	367: {
		id: 367,
		name: '������� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 194,
		components: [189, 314],
		quantities: [194, 1]
	},
	368: {
		id: 368,
		name: '������� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 336,
		components: [189, 315],
		quantities: [336, 1]
	},
	369: {
		id: 369,
		name: '�������� �����������',
		onlyOnce: true,
		upgType: 2196,
		quantity: 6,
		components: [2196],
		quantities: [6]
	},
	370: {
		id: 370,
		name: '�������� ����',
		onlyOnce: true,
		upgType: 2197,
		quantity: 4,
		components: [2197],
		quantities: [4]
	},
	371: {
		id: 371,
		name: '������ ��������',
		onlyOnce: true,
		upgType: 2337,
		quantity: 2,
		components: [2337],
		quantities: [2]
	},
	372: {
		id: 372,
		name: '������ ����',
		onlyOnce: true,
		upgType: 2098,
		quantity: 2,
		components: [2098],
		quantities: [2]
	},
	373: {
		id: 373,
		name: '���� ������� +8',
		onlyOnce: false,
		upgType: 907,
		quantity: 8,
		components: [907, 911],
		quantities: [8, 2]
	},
	374: {
		id: 374,
		name: '������� ����',
		onlyOnce: true,
		upgType: 2198,
		quantity: 3,
		components: [2198],
		quantities: [3]
	},
	375: {
		id: 375,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 132,
		quantity: 199,
		components: [132, 300],
		quantities: [199, 1]
	},
	376: {
		id: 376,
		name: '���� ����',
		onlyOnce: true,
		upgType: 2200,
		quantity: 2,
		components: [2200],
		quantities: [2]
	},
	377: {
		id: 377,
		name: '������',
		onlyOnce: true,
		upgType: 2158,
		quantity: 2,
		components: [2158],
		quantities: [2]
	},
	378: {
		id: 378,
		name: '���������� ���',
		onlyOnce: true,
		upgType: 2097,
		quantity: 2,
		components: [2097],
		quantities: [2]
	},
	379: {
		id: 379,
		name: 'Ҹ���� �����������',
		onlyOnce: true,
		upgType: 2201,
		quantity: 4,
		components: [2201],
		quantities: [4]
	},
	380: {
		id: 380,
		name: 'Ҹ���� �����������',
		onlyOnce: true,
		upgType: 2207,
		quantity: 4,
		components: [2207],
		quantities: [4]
	},
	381: {
		id: 381,
		name: '������ ����',
		onlyOnce: true,
		upgType: 2208,
		quantity: 3,
		components: [2208],
		quantities: [3]
	},
	382: {
		id: 382,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2203,
		quantity: 4,
		components: [2203],
		quantities: [4]
	},
	383: {
		id: 383,
		name: '��������� ������',
		onlyOnce: true,
		upgType: 2210,
		quantity: 5,
		components: [2210],
		quantities: [5]
	},
	384: {
		id: 384,
		name: '�������������� ������',
		onlyOnce: true,
		upgType: 2211,
		quantity: 3,
		components: [2211],
		quantities: [3]
	},
	385: {
		id: 385,
		name: '������ �������',
		onlyOnce: true,
		upgType: 2213,
		quantity: 4,
		components: [2213],
		quantities: [4]
	},
	386: {
		id: 386,
		name: '�� ��������� ����',
		onlyOnce: false,
		upgType: 13,
		quantity: 1,
		components: [13],
		quantities: [1]
	},
	387: {
		id: 387,
		name: '�������������� ��� +3',
		onlyOnce: false,
		upgType: 21,
		quantity: 3,
		components: [21],
		quantities: [3]
	},
	388: {
		id: 388,
		name: '���������� �����',
		onlyOnce: true,
		upgType: 2224,
		quantity: 3,
		components: [2224],
		quantities: [3]
	},
	389: {
		id: 389,
		name: '���� �����',
		onlyOnce: true,
		upgType: 2226,
		quantity: 3,
		components: [2226],
		quantities: [3]
	},
	390: {
		id: 390,
		name: '���� �����',
		onlyOnce: true,
		upgType: 2227,
		quantity: 2,
		components: [2227],
		quantities: [2]
	},
	391: {
		id: 391,
		name: '����������� ����',
		onlyOnce: true,
		upgType: 2230,
		quantity: 4,
		components: [2230, 2081, 2014, 2153, 2224],
		quantities: [4, 1, 2, 3, 3]
	},
	392: {
		id: 392,
		name: '���� ������',
		onlyOnce: true,
		upgType: 2214,
		quantity: 8,
		components: [2214],
		quantities: [8]
	},
	393: {
		id: 393,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2204,
		quantity: 3,
		components: [2204],
		quantities: [3]
	},
	394: {
		id: 394,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 2205,
		quantity: 5,
		components: [2205],
		quantities: [5]
	},
	395: {
		id: 395,
		name: '������ ���',
		onlyOnce: false,
		upgType: 2083,
		quantity: 1,
		components: [2083, 10],
		quantities: [1, 1]
	},
	396: {
		id: 396,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 2061,
		quantity: 10,
		components: [2061],
		quantities: [10]
	},
	397: {
		id: 397,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 2229,
		quantity: 8,
		components: [2229],
		quantities: [8]
	},
	398: {
		id: 398,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 2206,
		quantity: 8,
		components: [2206],
		quantities: [8]
	},
	399: {
		id: 399,
		name: '����������� ������',
		onlyOnce: true,
		upgType: 2230,
		quantity: 4,
		components: [2230],
		quantities: [4]
	},
	400: {
		id: 400,
		name: '�������� ��������',
		onlyOnce: true,
		upgType: 2231,
		quantity: 8,
		components: [2231],
		quantities: [8]
	},
	401: {
		id: 401,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2019,
		quantity: 2,
		components: [2019],
		quantities: [2]
	},
	402: {
		id: 402,
		name: '����� ������ ������',
		onlyOnce: true,
		upgType: 2035,
		quantity: 4,
		components: [2035],
		quantities: [4]
	},
	403: {
		id: 403,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2034,
		quantity: 5,
		components: [2034],
		quantities: [5]
	},
	404: {
		id: 404,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2045,
		quantity: 8,
		components: [2045],
		quantities: [8]
	},
	405: {
		id: 405,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2232,
		quantity: 1,
		components: [2232],
		quantities: [1]
	},
	406: {
		id: 406,
		name: '����� �������������',
		onlyOnce: true,
		upgType: 2233,
		quantity: 3,
		components: [2233],
		quantities: [3]
	},
	407: {
		id: 407,
		name: '�������� ���',
		onlyOnce: true,
		upgType: 2234,
		quantity: 5,
		components: [2234],
		quantities: [5]
	},
	408: {
		id: 408,
		name: '�����',
		onlyOnce: true,
		upgType: 2069,
		quantity: 6,
		components: [2069],
		quantities: [6]
	},
	409: {
		id: 409,
		name: 'Ҹ���� ����',
		onlyOnce: true,
		upgType: 2043,
		quantity: 3,
		components: [2043],
		quantities: [3]
	},
	410: {
		id: 410,
		name: '����������',
		onlyOnce: true,
		upgType: 166,
		quantity: 3,
		components: [166],
		quantities: [3]
	},
	411: {
		id: 411,
		name: '�����',
		onlyOnce: true,
		upgType: 13,
		quantity: 1,
		components: [13, 18, 19, 108, 106],
		quantities: [1, 1, 1, 1, 1]
	},
	412: {
		id: 412,
		name: '��������',
		onlyOnce: true,
		upgType: 31,
		quantity: 1,
		components: [31, 18, 69],
		quantities: [1, 1, 3]
	},
	413: {
		id: 413,
		name: '�������� ������ 3',
		onlyOnce: true,
		upgType: 125,
		quantity: 3,
		components: [125],
		quantities: [3]
	},
	414: {
		id: 414,
		name: '������� �����',
		onlyOnce: true,
		upgType: 25,
		quantity: 2,
		components: [25, 210],
		quantities: [2, 4]
	},
	415: {
		id: 415,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 2237,
		quantity: 5,
		components: [2237],
		quantities: [5]
	},
	416: {
		id: 416,
		name: '������� �����',
		onlyOnce: true,
		upgType: 2238,
		quantity: 1,
		components: [2238],
		quantities: [1]
	},
	417: {
		id: 417,
		name: '����������� �������',
		onlyOnce: true,
		upgType: 2190,
		quantity: 10,
		components: [2190],
		quantities: [10]
	},
	418: {
		id: 418,
		name: '����� �������������',
		onlyOnce: true,
		upgType: 62,
		quantity: 192,
		components: [62],
		quantities: [192]
	},
	419: {
		id: 419,
		name: '����������',
		onlyOnce: true,
		upgType: 2244,
		quantity: 3,
		components: [2244],
		quantities: [3]
	},
	420: {
		id: 420,
		name: '��������������',
		onlyOnce: false,
		upgType: 67,
		quantity: 2,
		components: [67],
		quantities: [2]
	},
	421: {
		id: 421,
		name: '������',
		onlyOnce: false,
		upgType: 15,
		quantity: 1,
		components: [15, 27, 28, 123],
		quantities: [1, 1, 1, 1]
	},
	422: {
		id: 422,
		name: '������ �������',
		onlyOnce: true,
		upgType: 2245,
		quantity: 2,
		components: [2245],
		quantities: [2]
	},
	423: {
		id: 423,
		name: '�������������� ��� +2',
		onlyOnce: false,
		upgType: 21,
		quantity: 2,
		components: [21],
		quantities: [2]
	},
	424: {
		id: 424,
		name: '�������',
		onlyOnce: false,
		upgType: 30,
		quantity: 1,
		components: [30, 39, 45, 127],
		quantities: [1, 1, 3, 4]
	},
	425: {
		id: 425,
		name: '�������� ������� (4)',
		onlyOnce: true,
		upgType: 95,
		quantity: 4,
		components: [95],
		quantities: [4]
	},
	426: {
		id: 426,
		name: '�������� ������� (8)',
		onlyOnce: true,
		upgType: 95,
		quantity: 8,
		components: [95],
		quantities: [8]
	},
	427: {
		id: 427,
		name: '�������� ������� (12)',
		onlyOnce: true,
		upgType: 95,
		quantity: 12,
		components: [95],
		quantities: [12]
	},
	428: {
		id: 428,
		name: '�����������',
		onlyOnce: true,
		upgType: 2252,
		quantity: 3,
		components: [2252],
		quantities: [3]
	},
	429: {
		id: 429,
		name: 'Ҹ���� �����',
		onlyOnce: true,
		upgType: 2253,
		quantity: 3,
		components: [2253],
		quantities: [3]
	},
	430: {
		id: 430,
		name: '������� �����',
		onlyOnce: true,
		upgType: 170,
		quantity: 1,
		components: [170, 175, 172, 308],
		quantities: [1, 20, 1003, 1]
	},
	431: {
		id: 431,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 132,
		quantity: 2.028,
		components: [132],
		quantities: [2028]
	},
	432: {
		id: 432,
		name: '��������� +2',
		onlyOnce: false,
		upgType: 22,
		quantity: 2,
		components: [22],
		quantities: [2]
	},
	433: {
		id: 433,
		name: '������� ����',
		onlyOnce: false,
		upgType: 2254,
		quantity: 4,
		components: [2254],
		quantities: [4]
	},
	434: {
		id: 434,
		name: '������� ������',
		onlyOnce: false,
		upgType: 2255,
		quantity: 5,
		components: [2255],
		quantities: [5]
	},
	435: {
		id: 435,
		name: '���������� �����',
		onlyOnce: false,
		upgType: 2256,
		quantity: 4,
		components: [2256],
		quantities: [4]
	},
	436: {
		id: 436,
		name: '��������� �����',
		onlyOnce: false,
		upgType: 2257,
		quantity: 5,
		neededAbilities: [368],
		components: [2257],
		quantities: [5]
	},
	437: {
		id: 437,
		name: '���� ����������',
		onlyOnce: false,
		upgType: 2258,
		quantity: 4,
		components: [2258],
		quantities: [4]
	},
	438: {
		id: 438,
		name: '���������� ������ 1',
		onlyOnce: false,
		upgType: 2254,
		quantity: 4,
		components: [2254, 2255, 2256, 2257],
		quantities: [4, 5, 4, 6]
	},
	439: {
		id: 439,
		name: '���������� ������ 2',
		onlyOnce: false,
		upgType: 101,
		quantity: 1,
		components: [101, 102, 103],
		quantities: [1, 1, 1]
	},
	440: {
		id: 440,
		name: '����������� � ����',
		onlyOnce: true,
		upgType: 332,
		quantity: 1,
		components: [332, 191, 193, 194],
		quantities: [1, 21, 1, 1]
	},
	441: {
		id: 441,
		name: '������ ���',
		onlyOnce: true,
		upgType: 2083,
		quantity: 1,
		components: [2083, 10, 123],
		quantities: [1, 1, 1]
	},
	442: {
		id: 442,
		name: '����������� � �����',
		onlyOnce: true,
		upgType: 333,
		quantity: 1,
		components: [333, 191, 193, 194],
		quantities: [1, 37, 1, 1]
	},
	443: {
		id: 443,
		name: '����� ������������',
		onlyOnce: true,
		upgType: 191,
		quantity: 184,
		neededAbilities: [332],
		components: [191, 11],
		quantities: [184, -4]
	},
	444: {
		id: 444,
		name: '����������� � ������',
		onlyOnce: true,
		upgType: 334,
		quantity: 1,
		components: [334, 191, 193, 194],
		quantities: [1, 56, 1, 1]
	},
	445: {
		id: 445,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 191,
		quantity: 201,
		neededAbilities: [334],
		components: [191, 1],
		quantities: [201, -5]
	},
	446: {
		id: 446,
		name: '����� �������',
		onlyOnce: true,
		upgType: 191,
		quantity: 154,
		neededAbilities: [333],
		components: [191],
		quantities: [154]
	},
	447: {
		id: 447,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 329,
		quantity: -1,
		neededAbilities: [329],
		components: [329, 330, 191, 1, 7],
		quantities: [-1, 1, 159, -8, 1]
	},
	448: {
		id: 448,
		name: '����������� �� ������� ������',
		onlyOnce: true,
		upgType: 329,
		quantity: 1,
		components: [329, 191, 193, 194],
		quantities: [1, 98, 1, 1]
	},
	449: {
		id: 449,
		name: '����� �������',
		onlyOnce: true,
		upgType: 329,
		quantity: -1,
		neededAbilities: [329],
		components: [329, 331, 191, 1, 11],
		quantities: [-1, 1, -33, -5, -5]
	},
	450: {
		id: 450,
		name: '������ ������',
		onlyOnce: true,
		upgType: 346,
		quantity: 1,
		components: [346, 48, 7],
		quantities: [1, 1, -1]
	},
	451: {
		id: 451,
		name: '������ �����',
		onlyOnce: true,
		upgType: 347,
		quantity: 1,
		components: [347, 155, 50, 4],
		quantities: [1, 1, 1, -1]
	},
	452: {
		id: 452,
		name: '������ �������',
		onlyOnce: true,
		upgType: 348,
		quantity: 1,
		components: [348, 127, 7, 4, 5],
		quantities: [1, 2, 1, -1, -1]
	},
	453: {
		id: 453,
		name: '������ �������',
		onlyOnce: true,
		upgType: 349,
		quantity: 1,
		components: [349, 25, 101, 6],
		quantities: [1, 3, 1, -1]
	},
	454: {
		id: 454,
		name: '������ ��������',
		onlyOnce: true,
		upgType: 350,
		quantity: 1,
		components: [350, 96, 907, 101, 6],
		quantities: [1, 4, 2, 1, -1]
	},
	455: {
		id: 455,
		name: '������ �������',
		onlyOnce: true,
		upgType: 351,
		quantity: 1,
		components: [351, 143, 101, 6],
		quantities: [1, 1, 1, -1]
	},
	456: {
		id: 456,
		name: '������ ��������',
		onlyOnce: true,
		upgType: 352,
		quantity: 1,
		components: [352, 120, 121, 2, 3],
		quantities: [1, 1, 1, -1, -1]
	},
	457: {
		id: 457,
		name: '������ �������',
		onlyOnce: true,
		upgType: 353,
		quantity: 1,
		components: [353, 59, 2, 3],
		quantities: [1, 4, -1, -1]
	},
	458: {
		id: 458,
		name: '����� ����������',
		onlyOnce: true,
		upgType: 2042,
		quantity: 5,
		components: [2042],
		quantities: [5]
	},
	459: {
		id: 459,
		name: '������ ������',
		onlyOnce: true,
		upgType: 354,
		quantity: 1,
		components: [354, 26, 56, 1],
		quantities: [1, 1, -5, -8]
	},
	460: {
		id: 460,
		name: '����������',
		onlyOnce: true,
		upgType: 2064,
		quantity: 5,
		components: [2064],
		quantities: [5]
	},
	461: {
		id: 461,
		name: '����� +4',
		onlyOnce: false,
		upgType: 55,
		quantity: 4,
		components: [55],
		quantities: [4]
	},
	462: {
		id: 462,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2269,
		quantity: 2,
		components: [2269],
		quantities: [2]
	},
	463: {
		id: 463,
		name: '���������� (4)',
		onlyOnce: true,
		upgType: 50,
		quantity: 4,
		components: [50],
		quantities: [4]
	},
	464: {
		id: 464,
		name: '���������� +2',
		onlyOnce: false,
		upgType: 50,
		quantity: 2,
		neededAbilities: [50],
		components: [50],
		quantities: [2]
	},
	465: {
		id: 465,
		name: '����',
		onlyOnce: true,
		upgType: 2270,
		quantity: 2,
		components: [2270],
		quantities: [2]
	},
	466: {
		id: 466,
		name: '������ (5)',
		onlyOnce: true,
		upgType: 124,
		quantity: 5,
		components: [124],
		quantities: [5]
	},
	467: {
		id: 467,
		name: '�����������',
		onlyOnce: true,
		upgType: 2273,
		quantity: 2,
		components: [2273],
		quantities: [2]
	},
	468: {
		id: 468,
		name: '������',
		onlyOnce: true,
		upgType: 124,
		quantity: 5,
		components: [124, 9, 10],
		quantities: [5, 1, 1]
	},
	469: {
		id: 469,
		name: '������ +1',
		onlyOnce: false,
		upgType: 124,
		quantity: 1,
		neededAbilities: [124],
		components: [124],
		quantities: [1]
	},
	470: {
		id: 470,
		name: '����� ����������',
		onlyOnce: true,
		upgType: 62,
		quantity: 209,
		components: [62],
		quantities: [209]
	},
	471: {
		id: 471,
		name: '��������',
		onlyOnce: true,
		upgType: 2274,
		quantity: 2,
		components: [2274],
		quantities: [2]
	},
	472: {
		id: 472,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2390,
		quantity: 2,
		neededAbilities: [370],
		components: [2390, 907, 911],
		quantities: [2, 2, 1]
	},
	473: {
		id: 473,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 2276,
		quantity: 3,
		neededAbilities: [366],
		components: [2276],
		quantities: [3]
	},
	474: {
		id: 474,
		name: '�������� ������ -1',
		onlyOnce: false,
		upgType: 125,
		quantity: -1,
		neededAbilities: [125],
		components: [125],
		quantities: [-1]
	},
	475: {
		id: 475,
		name: '����������� (2)',
		onlyOnce: true,
		upgType: 48,
		quantity: 2,
		components: [48],
		quantities: [2]
	},
	476: {
		id: 476,
		name: '����������� (4)',
		onlyOnce: true,
		upgType: 48,
		quantity: 4,
		components: [48],
		quantities: [4]
	},
	477: {
		id: 477,
		name: '������������� ����',
		onlyOnce: true,
		upgType: 2279,
		quantity: 1,
		components: [2279],
		quantities: [1]
	},
	478: {
		id: 478,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2280,
		quantity: 2,
		components: [2280],
		quantities: [2]
	},
	479: {
		id: 479,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2281,
		quantity: 2,
		components: [2281],
		quantities: [2]
	},
	480: {
		id: 480,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2282,
		quantity: 2,
		components: [2282],
		quantities: [2]
	},
	481: {
		id: 481,
		name: '���������',
		onlyOnce: true,
		upgType: 2283,
		quantity: 2,
		components: [2283],
		quantities: [2]
	},
	482: {
		id: 482,
		name: '������� ����',
		onlyOnce: true,
		upgType: 2278,
		quantity: 1,
		components: [2278],
		quantities: [1]
	},
	483: {
		id: 483,
		name: '���������� ����',
		onlyOnce: true,
		upgType: 2284,
		quantity: 1,
		components: [2284],
		quantities: [1]
	},
	484: {
		id: 484,
		name: '��������� ����',
		onlyOnce: true,
		upgType: 38,
		quantity: 1,
		components: [38, 88, 2010, 2284],
		quantities: [1, 1, 1, 1]
	},
	485: {
		id: 485,
		name: '�������� ��������� ����',
		onlyOnce: false,
		upgType: 1,
		quantity: 1,
		components: [1, 10],
		quantities: [1, 1]
	},
	486: {
		id: 486,
		name: '������ ���� +2',
		onlyOnce: false,
		upgType: 32,
		quantity: 2,
		components: [32],
		quantities: [2]
	},
	487: {
		id: 487,
		name: '������ ������ +2',
		onlyOnce: false,
		upgType: 33,
		quantity: 2,
		components: [33],
		quantities: [2]
	},
	488: {
		id: 488,
		name: '������ ����� +2',
		onlyOnce: false,
		upgType: 34,
		quantity: 2,
		components: [34],
		quantities: [2]
	},
	489: {
		id: 489,
		name: '��������',
		onlyOnce: true,
		upgType: 226,
		quantity: 5,
		components: [226, 33, 34],
		quantities: [5, 1, 1]
	},
	490: {
		id: 490,
		name: '������',
		onlyOnce: true,
		upgType: 2287,
		quantity: 2,
		components: [2287],
		quantities: [2]
	},
	491: {
		id: 491,
		name: '������� ������',
		onlyOnce: true,
		upgType: 153,
		quantity: 1,
		components: [153, 154],
		quantities: [1, 2]
	},
	492: {
		id: 492,
		name: '����� ����������',
		onlyOnce: true,
		upgType: 16,
		quantity: 1,
		components: [16, 26, 108, 2293, 2378],
		quantities: [1, 1, 1, 1, 4]
	},
	493: {
		id: 493,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 224,
		quantity: 1,
		components: [224],
		quantities: [1]
	},
	494: {
		id: 494,
		name: '���������� ���������',
		onlyOnce: true,
		upgType: 2291,
		quantity: 1,
		components: [2291],
		quantities: [1]
	},
	495: {
		id: 495,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 2292,
		quantity: 2,
		components: [2292],
		quantities: [2]
	},
	496: {
		id: 496,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 2293,
		quantity: 2,
		components: [2293],
		quantities: [2]
	},
	497: {
		id: 497,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2294,
		quantity: 2,
		components: [2294],
		quantities: [2]
	},
	498: {
		id: 498,
		name: '��������� ���������',
		onlyOnce: true,
		upgType: 2295,
		quantity: 2,
		components: [2295],
		quantities: [2]
	},
	499: {
		id: 499,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2294,
		quantity: 2,
		components: [2294, 10],
		quantities: [2, 2]
	},
	500: {
		id: 500,
		name: '����� +5',
		onlyOnce: false,
		upgType: 226,
		quantity: 5,
		components: [226],
		quantities: [5]
	},
	501: {
		id: 501,
		name: '������� �����',
		onlyOnce: false,
		upgType: 2,
		quantity: 2,
		components: [2, 226],
		quantities: [2, 5]
	},
	502: {
		id: 502,
		name: '׸���� �����',
		onlyOnce: false,
		upgType: 25,
		quantity: 1,
		components: [25, 68],
		quantities: [1, 10]
	},
	503: {
		id: 503,
		name: '����� +2',
		onlyOnce: false,
		upgType: 25,
		quantity: 2,
		components: [25],
		quantities: [2]
	},
	504: {
		id: 504,
		name: '������� ����� +10',
		onlyOnce: false,
		upgType: 226,
		quantity: 10,
		components: [226],
		quantities: [10]
	},
	505: {
		id: 505,
		name: '׸���� ����� +10',
		onlyOnce: false,
		upgType: 68,
		quantity: 10,
		components: [68],
		quantities: [10]
	},
	506: {
		id: 506,
		name: '������ ������� (2)',
		onlyOnce: true,
		upgType: 69,
		quantity: 2,
		components: [69],
		quantities: [2]
	},
	507: {
		id: 507,
		name: '������ ������� +1',
		onlyOnce: false,
		upgType: 69,
		quantity: 1,
		components: [69],
		quantities: [1]
	},
	508: {
		id: 508,
		name: '���� � ����� +1',
		onlyOnce: false,
		upgType: 228,
		quantity: 1,
		components: [228],
		quantities: [1]
	},
	509: {
		id: 509,
		name: '���� � ����� +2',
		onlyOnce: false,
		upgType: 228,
		quantity: 2,
		components: [228],
		quantities: [2]
	},
	510: {
		id: 510,
		name: '������ ����',
		onlyOnce: true,
		upgType: 2247,
		quantity: 4,
		components: [2247],
		quantities: [4]
	},
	511: {
		id: 511,
		name: '��������� �������',
		onlyOnce: true,
		upgType: 2296,
		quantity: 4,
		components: [2296],
		quantities: [4]
	},
	512: {
		id: 512,
		name: '��������� �������',
		onlyOnce: true,
		upgType: 2297,
		quantity: 4,
		components: [2297],
		quantities: [4]
	},
	513: {
		id: 513,
		name: '���� � ����� +4',
		onlyOnce: false,
		upgType: 228,
		quantity: 4,
		components: [228],
		quantities: [4]
	},
	514: {
		id: 514,
		name: '���������',
		onlyOnce: true,
		upgType: 2298,
		quantity: 1,
		components: [2298],
		quantities: [1]
	},
	515: {
		id: 515,
		name: '��������� (10)',
		onlyOnce: true,
		upgType: 198,
		quantity: 10,
		components: [198],
		quantities: [10]
	},
	516: {
		id: 516,
		name: '�����������',
		onlyOnce: true,
		upgType: 55,
		quantity: 10,
		components: [55, 30, 39, 105],
		quantities: [10, 1, 1, 1]
	},
	517: {
		id: 517,
		name: '������������ ���',
		onlyOnce: true,
		upgType: 2261,
		quantity: 2,
		components: [2261, 10],
		quantities: [2, 1]
	},
	518: {
		id: 518,
		name: '������������ ���',
		onlyOnce: true,
		upgType: 2356,
		quantity: 1,
		components: [2356, 10, 123],
		quantities: [1, 1, 1]
	},
	519: {
		id: 519,
		name: '��������� ������',
		onlyOnce: true,
		upgType: 2174,
		quantity: 2,
		components: [2174, 2024, 105, 107],
		quantities: [2, 1, 1, 1]
	},
	520: {
		id: 520,
		name: '������',
		onlyOnce: true,
		upgType: 104,
		quantity: 1,
		components: [104, 101, 105, 107],
		quantities: [1, 1, 1, 1]
	},
	521: {
		id: 521,
		name: '���������� ���',
		onlyOnce: false,
		upgType: 213,
		quantity: 1,
		components: [213],
		quantities: [1]
	},
	522: {
		id: 522,
		name: '����',
		onlyOnce: true,
		upgType: 204,
		quantity: 10,
		components: [204, 205],
		quantities: [10, 1]
	},
	523: {
		id: 523,
		name: '�������������',
		onlyOnce: true,
		upgType: 139,
		quantity: 3,
		components: [139],
		quantities: [3]
	},
	524: {
		id: 524,
		name: '�����',
		onlyOnce: true,
		upgType: 88,
		quantity: 1,
		components: [88, 67, 35],
		quantities: [1, 3, 5]
	},
	525: {
		id: 525,
		name: '�����',
		onlyOnce: false,
		upgType: 104,
		quantity: 1,
		components: [104, 105, 106, 107, 254],
		quantities: [1, 1, 1, 1, 1]
	},
	526: {
		id: 526,
		name: '���� ������� +4',
		onlyOnce: false,
		upgType: 907,
		quantity: 4,
		components: [907],
		quantities: [4]
	},
	527: {
		id: 527,
		name: '����� ���� +25',
		onlyOnce: false,
		upgType: 232,
		quantity: 25,
		components: [232],
		quantities: [25]
	},
	528: {
		id: 528,
		name: '������������ (20)',
		onlyOnce: true,
		upgType: 63,
		quantity: 20,
		components: [63],
		quantities: [20]
	},
	529: {
		id: 529,
		name: '������������',
		onlyOnce: true,
		upgType: 24,
		quantity: 12,
		components: [24, 215],
		quantities: [12, 1]
	},
	530: {
		id: 530,
		name: '���������',
		onlyOnce: true,
		upgType: 27,
		quantity: 1,
		components: [27, 28, 22, 64, 95],
		quantities: [1, 1, 2, 216, 6]
	},
	531: {
		id: 531,
		name: '������ �������',
		onlyOnce: true,
		upgType: 16,
		quantity: 1,
		components: [16, 21, 35, 37, 97],
		quantities: [1, 1, 2, 4, 1]
	},
	532: {
		id: 532,
		name: '���',
		onlyOnce: true,
		upgType: 2062,
		quantity: 5,
		components: [2062],
		quantities: [5]
	},
	533: {
		id: 533,
		name: '������������ +1',
		onlyOnce: false,
		upgType: 184,
		quantity: 1,
		components: [184],
		quantities: [1]
	},
	534: {
		id: 534,
		name: '���������� ������� 2',
		onlyOnce: false,
		upgType: 158,
		quantity: 2,
		components: [158],
		quantities: [2]
	},
	535: {
		id: 535,
		name: '����������� +5',
		onlyOnce: false,
		upgType: 226,
		quantity: 5,
		components: [226],
		quantities: [5]
	},
	536: {
		id: 536,
		name: '�������� ���������',
		onlyOnce: false,
		upgType: 177,
		quantity: 1,
		components: [177],
		quantities: [1]
	},
	537: {
		id: 537,
		name: '������ ����',
		onlyOnce: false,
		upgType: 41,
		quantity: 2,
		components: [41, 42, 108, 217, 343],
		quantities: [2, 2, 1, 1, 1]
	},
	538: {
		id: 538,
		name: '����������',
		onlyOnce: false,
		upgType: 98,
		quantity: 1,
		components: [98],
		quantities: [1]
	},
	539: {
		id: 539,
		name: '������ ����',
		onlyOnce: false,
		upgType: 41,
		quantity: 2,
		components: [41, 104, 98, 2291, 2294],
		quantities: [2, 1, 1, 1, 2]
	},
	540: {
		id: 540,
		name: '�������',
		onlyOnce: false,
		upgType: 28,
		quantity: 1,
		components: [28, 30, 45, 98],
		quantities: [1, 1, 3, 1]
	},
	541: {
		id: 541,
		name: '������ ����',
		onlyOnce: false,
		upgType: 26,
		quantity: 1,
		components: [26, 178, 76, 98],
		quantities: [1, 1, 2, 1]
	},
	542: {
		id: 542,
		name: '������ ����',
		onlyOnce: true,
		upgType: 2301,
		quantity: 3,
		components: [2301],
		quantities: [3]
	},
	543: {
		id: 543,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2302,
		quantity: 4,
		components: [2302],
		quantities: [4]
	},
	544: {
		id: 544,
		name: '��������� � �������',
		onlyOnce: false,
		upgType: 105,
		quantity: 1,
		components: [105],
		quantities: [1]
	},
	545: {
		id: 545,
		name: '��������� � ����������',
		onlyOnce: false,
		upgType: 106,
		quantity: 1,
		components: [106],
		quantities: [1]
	},
	546: {
		id: 546,
		name: '��������� � ���������',
		onlyOnce: false,
		upgType: 107,
		quantity: 1,
		components: [107],
		quantities: [1]
	},
	547: {
		id: 547,
		name: '���� ���������� +1',
		onlyOnce: false,
		upgType: 905,
		quantity: 1,
		components: [905],
		quantities: [1]
	},
	548: {
		id: 548,
		name: '������������ ���������� +1',
		onlyOnce: false,
		upgType: 906,
		quantity: 1,
		components: [906],
		quantities: [1]
	},
	549: {
		id: 549,
		name: '����������� +1',
		onlyOnce: false,
		upgType: 908,
		quantity: 1,
		components: [908],
		quantities: [1]
	},
	550: {
		id: 550,
		name: '������ -5',
		onlyOnce: false,
		upgType: 56,
		quantity: -5,
		neededAbilities: [56],
		components: [56, 12],
		quantities: [-5, -1]
	},
	551: {
		id: 551,
		name: '��������� -5',
		onlyOnce: false,
		upgType: 58,
		quantity: -5,
		neededAbilities: [58],
		components: [58, 12],
		quantities: [-5, -2]
	},
	552: {
		id: 552,
		name: '�������������',
		onlyOnce: false,
		upgType: 80,
		quantity: 1,
		components: [80, 105, 106, 107],
		quantities: [1, 1, 1, 1]
	},
	553: {
		id: 553,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2352,
		quantity: 2,
		components: [2352],
		quantities: [2]
	},
	554: {
		id: 554,
		name: '������ ������',
		onlyOnce: true,
		upgType: 2353,
		quantity: 4,
		components: [2353],
		quantities: [4]
	},
	555: {
		id: 555,
		name: '����� ������������',
		onlyOnce: true,
		upgType: 2354,
		quantity: 3,
		components: [2354],
		quantities: [3]
	},
	556: {
		id: 556,
		name: '������� �������',
		onlyOnce: false,
		upgType: 108,
		quantity: 1,
		components: [108, 63, 104, 105, 107],
		quantities: [1, 10, 1, 1, 1]
	},
	557: {
		id: 557,
		name: '����� ����� 1',
		onlyOnce: true,
		upgType: 13,
		quantity: 1,
		components: [13, 18, 19, 22, 108],
		quantities: [1, 1, 1, 1, 1]
	},
	558: {
		id: 558,
		name: '����� ����� 2',
		onlyOnce: true,
		upgType: 38,
		quantity: 1,
		components: [38, 88, 101, 104],
		quantities: [1, 1, 1, 1]
	},
	559: {
		id: 559,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2303,
		quantity: 1,
		components: [2303],
		quantities: [1]
	},
	560: {
		id: 560,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2304,
		quantity: 2,
		components: [2304],
		quantities: [2]
	},
	561: {
		id: 561,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2305,
		quantity: 3,
		components: [2305],
		quantities: [3]
	},
	562: {
		id: 562,
		name: '����� ������ ������',
		onlyOnce: true,
		upgType: 2306,
		quantity: 3,
		components: [2306],
		quantities: [3]
	},
	563: {
		id: 563,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2307,
		quantity: 5,
		components: [2307],
		quantities: [5]
	},
	564: {
		id: 564,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2308,
		quantity: 5,
		components: [2308],
		quantities: [5]
	},
	565: {
		id: 565,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2309,
		quantity: 4,
		components: [2309],
		quantities: [4]
	},
	566: {
		id: 566,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2310,
		quantity: 6,
		components: [2310],
		quantities: [6]
	},
	567: {
		id: 567,
		name: '����� �����������',
		onlyOnce: true,
		upgType: 2311,
		quantity: 8,
		components: [2311],
		quantities: [8]
	},
	568: {
		id: 568,
		name: '����� �������',
		onlyOnce: true,
		upgType: 2312,
		quantity: 10,
		components: [2312],
		quantities: [10]
	},
	569: {
		id: 569,
		name: '׸���� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 313,
		components: [189, 291, 301],
		quantities: [313, 1, 1]
	},
	570: {
		id: 570,
		name: 'Ҹ���� ������������� 1',
		onlyOnce: false,
		upgType: 121,
		quantity: 1,
		components: [121],
		quantities: [1]
	},
	571: {
		id: 571,
		name: 'Ҹ���� ������������� 2',
		onlyOnce: false,
		upgType: 121,
		quantity: 2,
		components: [121],
		quantities: [2]
	},
	572: {
		id: 572,
		name: 'Ҹ���� ������������� 3',
		onlyOnce: false,
		upgType: 121,
		quantity: 3,
		components: [121],
		quantities: [3]
	},
	573: {
		id: 573,
		name: '���� ������� ������ +1',
		onlyOnce: false,
		upgType: 909,
		quantity: 1,
		components: [909],
		quantities: [1]
	},
	574: {
		id: 574,
		name: '���� ������� ������ +2',
		onlyOnce: false,
		upgType: 909,
		quantity: 2,
		components: [909],
		quantities: [2]
	},
	575: {
		id: 575,
		name: '���� ������� ������ +4',
		onlyOnce: false,
		upgType: 909,
		quantity: 4,
		components: [909],
		quantities: [4]
	},
	576: {
		id: 576,
		name: '�������� ����',
		onlyOnce: true,
		upgType: 2272,
		quantity: 1,
		components: [2272],
		quantities: [1]
	},
	577: {
		id: 577,
		name: '���� �����',
		onlyOnce: true,
		upgType: 16,
		quantity: 1,
		components: [16, 96, 50],
		quantities: [1, 6, 3]
	},
	578: {
		id: 578,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2269,
		quantity: 4,
		components: [2269],
		quantities: [4]
	},
	579: {
		id: 579,
		name: '������� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 314,
		components: [189, 312],
		quantities: [314, 1]
	},
	580: {
		id: 580,
		name: '�������� ����',
		onlyOnce: true,
		upgType: 2272,
		quantity: -1,
		neededAbilities: [2.272],
		components: [2272, 2355],
		quantities: [-1, 1]
	},
	581: {
		id: 581,
		name: '������ ������� 5',
		onlyOnce: true,
		upgType: 185,
		quantity: 5,
		components: [185],
		quantities: [5]
	},
	582: {
		id: 582,
		name: '������',
		onlyOnce: true,
		upgType: 2317,
		quantity: 2,
		components: [2317],
		quantities: [2]
	},
	583: {
		id: 583,
		name: '������� � �����',
		onlyOnce: true,
		upgType: 2318,
		quantity: 3,
		components: [2318],
		quantities: [3]
	},
	584: {
		id: 584,
		name: '������ �����',
		onlyOnce: true,
		upgType: 2319,
		quantity: 2,
		components: [2319, 10],
		quantities: [2, 1]
	},
	585: {
		id: 585,
		name: '������ ������',
		onlyOnce: true,
		upgType: 2320,
		quantity: 1,
		components: [2320],
		quantities: [1]
	},
	586: {
		id: 586,
		name: '������ ������',
		onlyOnce: true,
		upgType: 2321,
		quantity: 2,
		components: [2321, 10],
		quantities: [2, 1]
	},
	587: {
		id: 587,
		name: '����� �����',
		onlyOnce: true,
		upgType: 130,
		quantity: 322,
		components: [130, 304],
		quantities: [322, 1]
	},
	588: {
		id: 588,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 191,
		quantity: 56,
		components: [191, 192],
		quantities: [56, 1]
	},
	589: {
		id: 589,
		name: '��������� �������',
		onlyOnce: true,
		upgType: 180,
		quantity: 1,
		components: [180, 129],
		quantities: [1, 323]
	},
	590: {
		id: 590,
		name: '����������� (3)',
		onlyOnce: true,
		upgType: 53,
		quantity: 3,
		components: [53],
		quantities: [3]
	},
	591: {
		id: 591,
		name: '������ ���� +1',
		onlyOnce: false,
		upgType: 32,
		quantity: 1,
		components: [32],
		quantities: [1]
	},
	592: {
		id: 592,
		name: '����� ������',
		onlyOnce: false,
		upgType: 131,
		quantity: 2.027,
		components: [131],
		quantities: [2027]
	},
	593: {
		id: 593,
		name: '������ ������ +1',
		onlyOnce: false,
		upgType: 33,
		quantity: 1,
		components: [33],
		quantities: [1]
	},
	594: {
		id: 594,
		name: '�����',
		onlyOnce: true,
		upgType: 2299,
		quantity: 4,
		components: [2299],
		quantities: [4]
	},
	595: {
		id: 595,
		name: '������ ����� +1',
		onlyOnce: false,
		upgType: 34,
		quantity: 1,
		components: [34],
		quantities: [1]
	},
	596: {
		id: 596,
		name: '���������',
		onlyOnce: false,
		upgType: 99,
		quantity: 1,
		components: [99, 167],
		quantities: [1, 2]
	},
	597: {
		id: 597,
		name: '�����-�������',
		onlyOnce: true,
		upgType: 2324,
		quantity: 3,
		components: [2324, 1, 10],
		quantities: [3, 1, 1]
	},
	598: {
		id: 598,
		name: '����������� +2',
		onlyOnce: false,
		upgType: 908,
		quantity: 2,
		components: [908],
		quantities: [2]
	},
	599: {
		id: 599,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2325,
		quantity: 1,
		components: [2325, 123],
		quantities: [1, 1]
	},
	600: {
		id: 600,
		name: '�������� ����� �����',
		onlyOnce: true,
		upgType: 322,
		quantity: -3,
		neededAbilities: [322],
		components: [322, 335, 51],
		quantities: [-3, 4, 1]
	},
	601: {
		id: 601,
		name: '�������������� �������',
		onlyOnce: true,
		upgType: 20,
		quantity: 3,
		components: [20, 160],
		quantities: [3, 1]
	},
	602: {
		id: 602,
		name: '��������� ���� +3',
		onlyOnce: false,
		upgType: 57,
		quantity: 3,
		neededAbilities: [57],
		components: [57],
		quantities: [3]
	},
	603: {
		id: 603,
		name: '�������� ������ +1',
		onlyOnce: false,
		upgType: 144,
		quantity: 1,
		neededAbilities: [363],
		components: [144],
		quantities: [1]
	},
	604: {
		id: 604,
		name: '������',
		onlyOnce: true,
		upgType: 14,
		quantity: 1,
		components: [14, 26, 120],
		quantities: [1, 1, 1]
	},
	605: {
		id: 605,
		name: '������������ ������ 6',
		onlyOnce: true,
		upgType: 60,
		quantity: 6,
		components: [60],
		quantities: [6]
	},
	606: {
		id: 606,
		name: '������� ������� +1',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		components: [8, 60],
		quantities: [1, 1]
	},
	607: {
		id: 607,
		name: '������ ����',
		onlyOnce: true,
		upgType: 132,
		quantity: 334,
		components: [132, 302],
		quantities: [334, 1]
	},
	608: {
		id: 608,
		name: '���������� ������',
		onlyOnce: true,
		upgType: 25,
		quantity: 3,
		components: [25, 37, 48, 97],
		quantities: [3, 1, 4, 1]
	},
	609: {
		id: 609,
		name: '������� �����',
		onlyOnce: true,
		upgType: 10,
		quantity: 2,
		components: [10, 2183],
		quantities: [2, 2]
	},
	610: {
		id: 610,
		name: '������ ������',
		onlyOnce: true,
		upgType: 48,
		quantity: 4,
		components: [48, 2281, 2282, 2157, 104],
		quantities: [4, 2, 2, 4, 1]
	},
	611: {
		id: 611,
		name: '���������� ���������',
		onlyOnce: true,
		upgType: 2326,
		quantity: 2,
		components: [2326],
		quantities: [2]
	},
	612: {
		id: 612,
		name: '���������� ��������',
		onlyOnce: true,
		upgType: 2327,
		quantity: 2,
		components: [2327],
		quantities: [2]
	},
	613: {
		id: 613,
		name: '���������� ���������',
		onlyOnce: true,
		upgType: 2328,
		quantity: 3,
		components: [2328],
		quantities: [3]
	},
	614: {
		id: 614,
		name: '���������� ��������',
		onlyOnce: true,
		upgType: 2329,
		quantity: 4,
		components: [2329],
		quantities: [4]
	},
	615: {
		id: 615,
		name: '������ ����',
		onlyOnce: true,
		upgType: 2330,
		quantity: 5,
		components: [2330],
		quantities: [5]
	},
	616: {
		id: 616,
		name: '����� �����������',
		onlyOnce: true,
		upgType: 2331,
		quantity: 4,
		components: [2331],
		quantities: [4]
	},
	617: {
		id: 617,
		name: '����� ���������',
		onlyOnce: true,
		upgType: 2332,
		quantity: 4,
		components: [2332],
		quantities: [4]
	},
	618: {
		id: 618,
		name: '���������� ����',
		onlyOnce: true,
		upgType: 2333,
		quantity: 1,
		components: [2333],
		quantities: [1]
	},
	619: {
		id: 619,
		name: '����� �������',
		onlyOnce: true,
		upgType: 88,
		quantity: 1,
		components: [88, 38, 13, 18, 19],
		quantities: [1, 1, 1, 1, 1]
	},
	620: {
		id: 620,
		name: '����������� �����',
		onlyOnce: true,
		upgType: 22,
		quantity: 10,
		components: [22, 2328, 2329, 2333],
		quantities: [10, 3, 4, 1]
	},
	621: {
		id: 621,
		name: '���� ������� ������ +2',
		onlyOnce: false,
		upgType: 909,
		quantity: 2,
		neededAbilities: [2.014],
		components: [909],
		quantities: [2]
	},
	622: {
		id: 622,
		name: '��������� (5)',
		onlyOnce: true,
		upgType: 198,
		quantity: 5,
		components: [198],
		quantities: [5]
	},
	623: {
		id: 623,
		name: '��������� +5',
		onlyOnce: false,
		upgType: 198,
		quantity: 5,
		neededAbilities: [198],
		components: [198],
		quantities: [5]
	},
	624: {
		id: 624,
		name: '����� +2',
		onlyOnce: false,
		upgType: 96,
		quantity: 2,
		neededAbilities: [96],
		components: [96],
		quantities: [2]
	},
	625: {
		id: 625,
		name: '����������� +1',
		onlyOnce: false,
		upgType: 53,
		quantity: 1,
		neededAbilities: [53],
		components: [53],
		quantities: [1]
	},
	626: {
		id: 626,
		name: '��������� ������� (2)',
		onlyOnce: true,
		upgType: 128,
		quantity: 2,
		components: [128],
		quantities: [2]
	},
	627: {
		id: 627,
		name: '��������� ������� +1',
		onlyOnce: false,
		upgType: 128,
		quantity: 1,
		neededAbilities: [128],
		components: [128],
		quantities: [1]
	},
	628: {
		id: 628,
		name: '���������� ����',
		onlyOnce: false,
		upgType: 196,
		quantity: 1,
		components: [196],
		quantities: [1]
	},
	629: {
		id: 629,
		name: '������ ��� +1',
		onlyOnce: false,
		upgType: 195,
		quantity: 1,
		components: [195],
		quantities: [1]
	},
	630: {
		id: 630,
		name: '������ ������� 3',
		onlyOnce: true,
		upgType: 185,
		quantity: 3,
		components: [185],
		quantities: [3]
	},
	631: {
		id: 631,
		name: '���������� +25',
		onlyOnce: false,
		upgType: 68,
		quantity: 25,
		neededAbilities: [68],
		components: [68],
		quantities: [25]
	},
	632: {
		id: 632,
		name: '���������� �����',
		onlyOnce: true,
		upgType: 64,
		quantity: 1,
		neededAbilities: [64],
		components: [64],
		quantities: [1]
	},
	633: {
		id: 633,
		name: '������ ������� +1',
		onlyOnce: false,
		upgType: 185,
		quantity: 1,
		neededAbilities: [185],
		components: [185],
		quantities: [1]
	},
	634: {
		id: 634,
		name: '����������� (6)',
		onlyOnce: true,
		upgType: 53,
		quantity: 6,
		components: [53],
		quantities: [6]
	},
	635: {
		id: 635,
		name: '����� ����������',
		onlyOnce: true,
		upgType: 2204,
		quantity: 2,
		components: [2204, 2205, 2056, 2081, 2062],
		quantities: [2, 4, 3, 1, 5]
	},
	636: {
		id: 636,
		name: '����� +1',
		onlyOnce: false,
		upgType: 143,
		quantity: 1,
		components: [143],
		quantities: [1]
	},
	637: {
		id: 637,
		name: '���������� 25',
		onlyOnce: true,
		upgType: 168,
		quantity: 25,
		components: [168],
		quantities: [25]
	},
	638: {
		id: 638,
		name: '���������� 50',
		onlyOnce: true,
		upgType: 168,
		quantity: 50,
		components: [168],
		quantities: [50]
	},
	639: {
		id: 639,
		name: '���������� +25',
		onlyOnce: false,
		upgType: 168,
		quantity: 25,
		neededAbilities: [168],
		components: [168],
		quantities: [25]
	},
	640: {
		id: 640,
		name: '����������� �������',
		onlyOnce: true,
		upgType: 31,
		quantity: 1,
		neededAbilities: [23],
		components: [31, 23],
		quantities: [1, -3]
	},
	641: {
		id: 641,
		name: '���������� ������� +1',
		onlyOnce: false,
		upgType: 40,
		quantity: 1,
		components: [40],
		quantities: [1]
	},
	642: {
		id: 642,
		name: '���������� ������� +2',
		onlyOnce: false,
		upgType: 40,
		quantity: 2,
		components: [40],
		quantities: [2]
	},
	643: {
		id: 643,
		name: '����������� ������� +1',
		onlyOnce: false,
		upgType: 51,
		quantity: 1,
		components: [51],
		quantities: [1]
	},
	644: {
		id: 644,
		name: '������ ������ 1',
		onlyOnce: true,
		upgType: 155,
		quantity: 1,
		components: [155],
		quantities: [1]
	},
	645: {
		id: 645,
		name: '������ ������ 2',
		onlyOnce: true,
		upgType: 155,
		quantity: 2,
		components: [155],
		quantities: [2]
	},
	646: {
		id: 646,
		name: '������ ������ +1',
		onlyOnce: false,
		upgType: 155,
		quantity: 1,
		neededAbilities: [155],
		components: [155],
		quantities: [1]
	},
	647: {
		id: 647,
		name: '������ ������ 1',
		onlyOnce: true,
		upgType: 156,
		quantity: 1,
		components: [156],
		quantities: [1]
	},
	648: {
		id: 648,
		name: '������ ������ 2',
		onlyOnce: true,
		upgType: 156,
		quantity: 2,
		components: [156],
		quantities: [2]
	},
	649: {
		id: 649,
		name: '������ ������ +1',
		onlyOnce: false,
		upgType: 156,
		quantity: 1,
		neededAbilities: [156],
		components: [156],
		quantities: [1]
	},
	650: {
		id: 650,
		name: '������������ ������ 4',
		onlyOnce: true,
		upgType: 61,
		quantity: 4,
		components: [61],
		quantities: [4]
	},
	651: {
		id: 651,
		name: '������������ ������ +2',
		onlyOnce: false,
		upgType: 61,
		quantity: 2,
		neededAbilities: [61],
		components: [61],
		quantities: [2]
	},
	652: {
		id: 652,
		name: '����� ����� +1',
		onlyOnce: false,
		upgType: 197,
		quantity: 1,
		components: [197],
		quantities: [1]
	},
	653: {
		id: 653,
		name: '����� ����� +2',
		onlyOnce: false,
		upgType: 197,
		quantity: 2,
		components: [197],
		quantities: [2]
	},
	654: {
		id: 654,
		name: '����������� ���� (3)',
		onlyOnce: true,
		upgType: 134,
		quantity: 3,
		components: [134],
		quantities: [3]
	},
	655: {
		id: 655,
		name: '����������� ���� +1',
		onlyOnce: false,
		upgType: 134,
		quantity: 1,
		neededAbilities: [134],
		components: [134],
		quantities: [1]
	},
	656: {
		id: 656,
		name: '���������� ������� (3)',
		onlyOnce: true,
		upgType: 135,
		quantity: 3,
		components: [135],
		quantities: [3]
	},
	657: {
		id: 657,
		name: '���������� ������� +1',
		onlyOnce: false,
		upgType: 135,
		quantity: 1,
		neededAbilities: [135],
		components: [135],
		quantities: [1]
	},
	658: {
		id: 658,
		name: '������������',
		onlyOnce: false,
		upgType: 126,
		quantity: 1,
		components: [126],
		quantities: [1]
	},
	659: {
		id: 659,
		name: '�������� �������� (1)',
		onlyOnce: true,
		upgType: 979,
		quantity: 1,
		components: [979],
		quantities: [1]
	},
	660: {
		id: 660,
		name: '�������� �������� (2)',
		onlyOnce: true,
		upgType: 979,
		quantity: 2,
		components: [979],
		quantities: [2]
	},
	661: {
		id: 661,
		name: '������������ +2',
		onlyOnce: false,
		upgType: 24,
		quantity: 2,
		neededAbilities: [24],
		components: [24],
		quantities: [2]
	},
	662: {
		id: 662,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 335,
		components: [189, 313],
		quantities: [335, 1]
	},
	663: {
		id: 663,
		name: '�������� ������� �����',
		onlyOnce: true,
		upgType: 2242,
		quantity: 2,
		components: [2242, 10, 909, 123],
		quantities: [2, 1, 2, 1]
	},
	664: {
		id: 664,
		name: '������������',
		onlyOnce: true,
		upgType: 2059,
		quantity: 4,
		components: [2059],
		quantities: [4]
	},
	665: {
		id: 665,
		name: 'Ҹ���� ����������',
		onlyOnce: true,
		upgType: 2338,
		quantity: 1,
		components: [2338],
		quantities: [1]
	},
	666: {
		id: 666,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2339,
		quantity: 2,
		components: [2339],
		quantities: [2]
	},
	667: {
		id: 667,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2340,
		quantity: 3,
		components: [2340],
		quantities: [3]
	},
	668: {
		id: 668,
		name: '���������� � ����������',
		onlyOnce: true,
		upgType: 115,
		quantity: 341,
		components: [115],
		quantities: [341]
	},
	669: {
		id: 669,
		name: '������',
		onlyOnce: true,
		upgType: 2341,
		quantity: 1,
		components: [2341],
		quantities: [1]
	},
	670: {
		id: 670,
		name: '������� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 342,
		components: [189, 316],
		quantities: [342, 1]
	},
	671: {
		id: 671,
		name: '�������� ����',
		onlyOnce: true,
		upgType: 2343,
		quantity: 2,
		components: [2343],
		quantities: [2]
	},
	672: {
		id: 672,
		name: '��� ׸����� ����',
		onlyOnce: true,
		upgType: 8,
		quantity: 2,
		components: [8, 23],
		quantities: [2, 3]
	},
	673: {
		id: 673,
		name: '������������� +2',
		onlyOnce: true,
		upgType: 32,
		quantity: 2,
		components: [32, 33, 34],
		quantities: [2, 2, 2]
	},
	674: {
		id: 674,
		name: '������� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 344,
		components: [189, 317],
		quantities: [344, 1]
	},
	675: {
		id: 675,
		name: '������� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 345,
		components: [189, 318, 22],
		quantities: [345, 1, 1]
	},
	676: {
		id: 676,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2346,
		quantity: 4,
		components: [2346],
		quantities: [4]
	},
	677: {
		id: 677,
		name: '������� �����',
		onlyOnce: true,
		upgType: 189,
		quantity: 347,
		components: [189, 319, 22],
		quantities: [347, 1, 1]
	},
	678: {
		id: 678,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 132,
		quantity: 263,
		components: [132, 303],
		quantities: [263, 1]
	},
	679: {
		id: 679,
		name: '�����������',
		onlyOnce: false,
		upgType: 123,
		quantity: 1,
		components: [123, 908],
		quantities: [1, 1]
	},
	680: {
		id: 680,
		name: '������� �� ������ +1',
		onlyOnce: false,
		upgType: 202,
		quantity: 1,
		components: [202],
		quantities: [1]
	},
	681: {
		id: 681,
		name: '������� �� ������ +2',
		onlyOnce: false,
		upgType: 202,
		quantity: 2,
		components: [202],
		quantities: [2]
	},
	682: {
		id: 682,
		name: '������� �� ������� +1',
		onlyOnce: false,
		upgType: 203,
		quantity: 1,
		components: [203],
		quantities: [1]
	},
	683: {
		id: 683,
		name: '������� �� ������� +2',
		onlyOnce: false,
		upgType: 203,
		quantity: 2,
		components: [203],
		quantities: [2]
	},
	684: {
		id: 684,
		name: '���������� ������',
		onlyOnce: false,
		upgType: 2348,
		quantity: 2,
		components: [2348],
		quantities: [2]
	},
	685: {
		id: 685,
		name: '��������� � ������������',
		onlyOnce: true,
		upgType: 109,
		quantity: 1,
		components: [109],
		quantities: [1]
	},
	686: {
		id: 686,
		name: '��������� ���� +1',
		onlyOnce: false,
		upgType: 127,
		quantity: 1,
		neededAbilities: [127],
		components: [127],
		quantities: [1]
	},
	687: {
		id: 687,
		name: '���������� ����� +1',
		onlyOnce: false,
		upgType: 143,
		quantity: 1,
		components: [143],
		quantities: [1]
	},
	688: {
		id: 688,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 132,
		quantity: 41,
		components: [132, 356, 147],
		quantities: [41, 1, 1]
	},
	689: {
		id: 689,
		name: '������� ����',
		onlyOnce: true,
		upgType: 133,
		quantity: 349,
		components: [133, 235],
		quantities: [349, 1]
	},
	690: {
		id: 690,
		name: '����� ����',
		onlyOnce: true,
		upgType: 133,
		quantity: 1,
		neededAbilities: [235],
		components: [133, 235, 236],
		quantities: [1, -1, 1]
	},
	691: {
		id: 691,
		name: '����������� �������',
		onlyOnce: true,
		upgType: 78,
		quantity: 3,
		components: [78],
		quantities: [3]
	},
	692: {
		id: 692,
		name: '������� ������ +1',
		onlyOnce: false,
		upgType: 154,
		quantity: 1,
		neededAbilities: [154],
		components: [154],
		quantities: [1]
	},
	693: {
		id: 693,
		name: '�� �������',
		onlyOnce: true,
		upgType: 150,
		quantity: 1,
		components: [150],
		quantities: [1]
	},
	694: {
		id: 694,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2288,
		quantity: 3,
		components: [2288],
		quantities: [3]
	},
	695: {
		id: 695,
		name: '���������� (2)',
		onlyOnce: true,
		upgType: 146,
		quantity: 2,
		components: [146],
		quantities: [2]
	},
	696: {
		id: 696,
		name: '���������� +1',
		onlyOnce: false,
		upgType: 146,
		quantity: 1,
		neededAbilities: [146],
		components: [146],
		quantities: [1]
	},
	697: {
		id: 697,
		name: '������� �����',
		onlyOnce: true,
		upgType: 170,
		quantity: 1,
		components: [170, 174, 305],
		quantities: [1, 1, 1]
	},
	698: {
		id: 698,
		name: '������ �������',
		onlyOnce: true,
		upgType: 79,
		quantity: 3,
		components: [79],
		quantities: [3]
	},
	699: {
		id: 699,
		name: '������������',
		onlyOnce: true,
		upgType: 24,
		quantity: 6,
		components: [24, 9],
		quantities: [6, 1]
	},
	700: {
		id: 700,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2310,
		quantity: 4,
		components: [2310, 10],
		quantities: [4, 1]
	},
	701: {
		id: 701,
		name: 'Ҹ���� ����������� +1',
		onlyOnce: false,
		upgType: 123,
		quantity: 1,
		components: [123],
		quantities: [1]
	},
	702: {
		id: 702,
		name: '�������� ������ +1',
		onlyOnce: false,
		upgType: 144,
		quantity: 1,
		components: [144],
		quantities: [1]
	},
	703: {
		id: 703,
		name: '����������',
		onlyOnce: true,
		upgType: 179,
		quantity: 1,
		components: [179],
		quantities: [1]
	},
	704: {
		id: 704,
		name: '������ ���� +1',
		onlyOnce: false,
		upgType: 157,
		quantity: 1,
		components: [157],
		quantities: [1]
	},
	705: {
		id: 705,
		name: '������ ���� +2',
		onlyOnce: false,
		upgType: 157,
		quantity: 2,
		components: [157],
		quantities: [2]
	},
	706: {
		id: 706,
		name: '������ �����������',
		onlyOnce: false,
		upgType: 161,
		quantity: 1,
		components: [161],
		quantities: [1]
	},
	707: {
		id: 707,
		name: '����� � �������',
		onlyOnce: true,
		upgType: 37,
		quantity: 3,
		components: [37],
		quantities: [3]
	},
	708: {
		id: 708,
		name: '����� � ������� +1',
		onlyOnce: false,
		upgType: 37,
		quantity: 1,
		neededAbilities: [37],
		components: [37],
		quantities: [1]
	},
	709: {
		id: 709,
		name: '������� ����',
		onlyOnce: true,
		upgType: 136,
		quantity: 1,
		components: [136],
		quantities: [1]
	},
	710: {
		id: 710,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 137,
		quantity: 4,
		components: [137],
		quantities: [4]
	},
	711: {
		id: 711,
		name: '������� ��� +2',
		onlyOnce: false,
		upgType: 137,
		quantity: 2,
		neededAbilities: [137],
		components: [137],
		quantities: [2]
	},
	712: {
		id: 712,
		name: '������ +10',
		onlyOnce: false,
		upgType: 204,
		quantity: 10,
		components: [204],
		quantities: [10]
	},
	713: {
		id: 713,
		name: '���������� +10',
		onlyOnce: false,
		upgType: 206,
		quantity: 10,
		components: [206],
		quantities: [10]
	},
	714: {
		id: 714,
		name: '���������� +20',
		onlyOnce: false,
		upgType: 206,
		quantity: 20,
		components: [206],
		quantities: [20]
	},
	715: {
		id: 715,
		name: '������� �����',
		onlyOnce: false,
		upgType: 170,
		quantity: 1,
		components: [170, 171, 172, 306],
		quantities: [1, 3, 1, 1]
	},
	716: {
		id: 716,
		name: '������� �����',
		onlyOnce: false,
		upgType: 170,
		quantity: 1,
		components: [170, 171, 172, 173, 307],
		quantities: [1, 10, 2, 1, 1]
	},
	717: {
		id: 717,
		name: '������� ������ +1',
		onlyOnce: false,
		upgType: 165,
		quantity: 1,
		components: [165],
		quantities: [1]
	},
	718: {
		id: 718,
		name: '���������� +1',
		onlyOnce: false,
		upgType: 166,
		quantity: 1,
		neededAbilities: [166],
		components: [166],
		quantities: [1]
	},
	719: {
		id: 719,
		name: '��������� +1',
		onlyOnce: false,
		upgType: 167,
		quantity: 1,
		neededAbilities: [167],
		components: [167],
		quantities: [1]
	},
	720: {
		id: 720,
		name: '����� �������� +2',
		onlyOnce: false,
		upgType: 44,
		quantity: 2,
		neededAbilities: [44],
		components: [44],
		quantities: [2]
	},
	721: {
		id: 721,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 44,
		quantity: 4,
		components: [44],
		quantities: [4]
	},
	722: {
		id: 722,
		name: '�������� �������',
		onlyOnce: true,
		upgType: 2014,
		quantity: 2,
		neededAbilities: [309],
		components: [2014],
		quantities: [2]
	},
	723: {
		id: 723,
		name: '���� ������� +2',
		onlyOnce: false,
		upgType: 907,
		quantity: 2,
		neededAbilities: [2.019],
		components: [907],
		quantities: [2]
	},
	724: {
		id: 724,
		name: '������ ������ ������',
		onlyOnce: true,
		upgType: 188,
		quantity: 1,
		components: [188],
		quantities: [1]
	},
	725: {
		id: 725,
		name: '��������� ����',
		onlyOnce: true,
		upgType: 2260,
		quantity: 1,
		neededAbilities: [366],
		components: [2260],
		quantities: [1]
	},
	726: {
		id: 726,
		name: '���� ����� (2)',
		onlyOnce: true,
		upgType: 186,
		quantity: 2,
		components: [186],
		quantities: [2]
	},
	727: {
		id: 727,
		name: '���� ����� +1',
		onlyOnce: false,
		upgType: 186,
		quantity: 1,
		neededAbilities: [186],
		components: [186],
		quantities: [1]
	},
	728: {
		id: 728,
		name: '���� ����� (3)',
		onlyOnce: true,
		upgType: 186,
		quantity: 3,
		components: [186],
		quantities: [3]
	},
	729: {
		id: 729,
		name: '������ �����',
		onlyOnce: true,
		upgType: 60,
		quantity: 4,
		components: [60, 981, 996],
		quantities: [4, 1, 10]
	},
	730: {
		id: 730,
		name: '������ ���������',
		onlyOnce: true,
		upgType: 60,
		quantity: 8,
		components: [60, 981, 996],
		quantities: [8, 2, 20]
	},
	731: {
		id: 731,
		name: '����� �����',
		onlyOnce: true,
		upgType: 17,
		quantity: 1,
		components: [17],
		quantities: [1]
	},
	732: {
		id: 732,
		name: '��������',
		onlyOnce: true,
		upgType: 217,
		quantity: 1,
		components: [217],
		quantities: [1]
	},
	733: {
		id: 733,
		name: '���� �����',
		onlyOnce: true,
		upgType: 218,
		quantity: 3,
		components: [218],
		quantities: [3]
	},
	734: {
		id: 734,
		name: '�������� ����� �����',
		onlyOnce: true,
		upgType: 320,
		quantity: 1,
		components: [320, 197, 50],
		quantities: [1, 2, 1]
	},
	735: {
		id: 735,
		name: '�������� ����� �����',
		onlyOnce: true,
		upgType: 320,
		quantity: -1,
		neededAbilities: [320],
		components: [320, 321, 51],
		quantities: [-1, 2, 1]
	},
	736: {
		id: 736,
		name: '�������� ����� �����',
		onlyOnce: true,
		upgType: 321,
		quantity: -2,
		neededAbilities: [321],
		components: [321, 322, 197, 50],
		quantities: [-2, 3, 2, 1]
	},
	737: {
		id: 737,
		name: '�������� ���������',
		onlyOnce: true,
		upgType: 323,
		quantity: 1,
		components: [323, 101, 143, 144],
		quantities: [1, 1, 1, 1]
	},
	738: {
		id: 738,
		name: '�������� ���������',
		onlyOnce: true,
		upgType: 323,
		quantity: -1,
		neededAbilities: [323],
		components: [323, 324, 143, 144],
		quantities: [-1, 2, 1, 1]
	},
	739: {
		id: 739,
		name: '�������� ���������',
		onlyOnce: true,
		upgType: 324,
		quantity: -2,
		neededAbilities: [324],
		components: [324, 325, 143, 144],
		quantities: [-2, 3, 1, 1]
	},
	740: {
		id: 740,
		name: '������ ���� ����',
		onlyOnce: true,
		upgType: 2369,
		quantity: 1,
		components: [2369],
		quantities: [1]
	},
	741: {
		id: 741,
		name: '�������� ���������',
		onlyOnce: true,
		upgType: 326,
		quantity: 1,
		components: [326, 68],
		quantities: [1, 35]
	},
	742: {
		id: 742,
		name: '�������� ���������',
		onlyOnce: true,
		upgType: 326,
		quantity: -1,
		neededAbilities: [326],
		components: [326, 327, 68],
		quantities: [-1, 2, 30]
	},
	743: {
		id: 743,
		name: '�������� ���������',
		onlyOnce: true,
		upgType: 327,
		quantity: -2,
		neededAbilities: [327],
		components: [327, 328, 68],
		quantities: [-2, 3, 35]
	},
	744: {
		id: 744,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2363,
		quantity: 2,
		components: [2363],
		quantities: [2]
	},
	745: {
		id: 745,
		name: '����� ������',
		onlyOnce: true,
		upgType: 2364,
		quantity: 2,
		components: [2364],
		quantities: [2]
	},
	746: {
		id: 746,
		name: '����� ����',
		onlyOnce: true,
		upgType: 2365,
		quantity: 3,
		components: [2365],
		quantities: [3]
	},
	747: {
		id: 747,
		name: '����� �����',
		onlyOnce: true,
		upgType: 2366,
		quantity: 3,
		components: [2366],
		quantities: [3]
	},
	748: {
		id: 748,
		name: '������ �������',
		onlyOnce: false,
		upgType: 2,
		quantity: 1,
		components: [2, 8],
		quantities: [1, 1]
	},
	749: {
		id: 749,
		name: '�������� ������',
		onlyOnce: false,
		upgType: 143,
		quantity: 2,
		components: [143, 144],
		quantities: [2, 1]
	},
	750: {
		id: 750,
		name: '������ ����',
		onlyOnce: true,
		upgType: 189,
		quantity: 380,
		components: [189, 360],
		quantities: [380, 1]
	},
	751: {
		id: 751,
		name: '������������� 20',
		onlyOnce: false,
		upgType: 268,
		quantity: 5,
		components: [268, 269],
		quantities: [5, 20]
	},
	752: {
		id: 752,
		name: '������������� +10',
		onlyOnce: false,
		upgType: 269,
		quantity: 10,
		neededAbilities: [269],
		components: [269],
		quantities: [10]
	},
	753: {
		id: 753,
		name: '������������� +5',
		onlyOnce: false,
		upgType: 268,
		quantity: 5,
		neededAbilities: [268],
		components: [268],
		quantities: [5]
	},
	754: {
		id: 754,
		name: '���������� ����',
		onlyOnce: true,
		upgType: 189,
		quantity: 1,
		neededAbilities: [360],
		components: [189, 360, 361],
		quantities: [1, -1, 1]
	},
	755: {
		id: 755,
		name: '������������� ����',
		onlyOnce: true,
		upgType: 189,
		quantity: 1,
		neededAbilities: [361],
		components: [189, 361, 362],
		quantities: [1, -1, 1]
	},
	756: {
		id: 756,
		name: '���� ����',
		onlyOnce: true,
		upgType: 162,
		quantity: 283,
		neededAbilities: [357],
		components: [162, 357, 358],
		quantities: [283, -1, 1]
	},
	757: {
		id: 757,
		name: '���� ��������',
		onlyOnce: true,
		upgType: 162,
		quantity: 284,
		neededAbilities: [357],
		components: [162, 357, 359],
		quantities: [284, -1, 1]
	},
	758: {
		id: 758,
		name: '�������� ���',
		onlyOnce: true,
		upgType: 2374,
		quantity: 2,
		components: [2374],
		quantities: [2]
	},
	759: {
		id: 759,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 16,
		quantity: 1,
		components: [16, 76, 357],
		quantities: [1, 3, 1]
	},
	760: {
		id: 760,
		name: '���� ����',
		onlyOnce: false,
		upgType: 144,
		quantity: 2,
		components: [144, 363],
		quantities: [2, 1]
	},
	761: {
		id: 761,
		name: '���� �����',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		neededAbilities: [364],
		components: [8, 9, 364],
		quantities: [1, 1, 1]
	},
	762: {
		id: 762,
		name: '����������� �������',
		onlyOnce: true,
		upgType: 78,
		quantity: 3,
		neededAbilities: [363],
		components: [78],
		quantities: [3]
	},
	763: {
		id: 763,
		name: '���� ����',
		onlyOnce: true,
		upgType: 8,
		quantity: 10,
		components: [8, 9, 28, 365],
		quantities: [10, 5, 1, 1]
	},
	764: {
		id: 764,
		name: '���� �����',
		onlyOnce: true,
		upgType: 907,
		quantity: 6,
		components: [907, 911, 2275, 366],
		quantities: [6, 2, 2, 1]
	},
	765: {
		id: 765,
		name: '������������� ����� +1',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		neededAbilities: [365],
		components: [8],
		quantities: [1]
	},
	766: {
		id: 766,
		name: '������������� ����� +2',
		onlyOnce: false,
		upgType: 8,
		quantity: 2,
		neededAbilities: [365],
		components: [8],
		quantities: [2]
	},
	767: {
		id: 767,
		name: '�������� ������� +2',
		onlyOnce: false,
		upgType: 95,
		quantity: 2,
		neededAbilities: [365],
		components: [95],
		quantities: [2]
	},
	768: {
		id: 768,
		name: '���� ������� +4',
		onlyOnce: false,
		upgType: 907,
		quantity: 4,
		neededAbilities: [366],
		components: [907, 911],
		quantities: [4, 1]
	},
	769: {
		id: 769,
		name: '������������ ���������� +1',
		onlyOnce: false,
		upgType: 906,
		quantity: 1,
		neededAbilities: [366],
		components: [906],
		quantities: [1]
	},
	770: {
		id: 770,
		name: '��������� �������� +1',
		onlyOnce: false,
		upgType: 9,
		quantity: 1,
		neededAbilities: [365],
		components: [9],
		quantities: [1]
	},
	771: {
		id: 771,
		name: '�������� � �������',
		onlyOnce: true,
		upgType: 2383,
		quantity: 3,
		neededAbilities: [366],
		components: [2383],
		quantities: [3]
	},
	772: {
		id: 772,
		name: '�������������� �������',
		onlyOnce: true,
		upgType: 20,
		quantity: 4,
		neededAbilities: [365],
		components: [20],
		quantities: [4]
	},
	773: {
		id: 773,
		name: '���� ������',
		onlyOnce: true,
		upgType: 8,
		quantity: 8,
		components: [8, 9, 95, 368],
		quantities: [8, 1, 3, 1]
	},
	774: {
		id: 774,
		name: '������������� ����� +1',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		neededAbilities: [367],
		components: [8],
		quantities: [1]
	},
	775: {
		id: 775,
		name: '������������� ����� +2',
		onlyOnce: false,
		upgType: 8,
		quantity: 2,
		neededAbilities: [367],
		components: [8],
		quantities: [2]
	},
	776: {
		id: 776,
		name: '���� ���������� +1',
		onlyOnce: false,
		upgType: 905,
		quantity: 1,
		neededAbilities: [367],
		components: [905],
		quantities: [1]
	},
	777: {
		id: 777,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 147,
		quantity: 1,
		neededAbilities: [368],
		components: [147],
		quantities: [1]
	},
	778: {
		id: 778,
		name: '����� �������� ���',
		onlyOnce: true,
		upgType: 2375,
		quantity: 3,
		neededAbilities: [367],
		components: [2375],
		quantities: [3]
	},
	779: {
		id: 779,
		name: '������������� ����� +2',
		onlyOnce: false,
		upgType: 8,
		quantity: 2,
		neededAbilities: [368],
		components: [8],
		quantities: [2]
	},
	780: {
		id: 780,
		name: '��������� �������� +1',
		onlyOnce: false,
		upgType: 9,
		quantity: 1,
		neededAbilities: [368],
		components: [9],
		quantities: [1]
	},
	781: {
		id: 781,
		name: '���� �����',
		onlyOnce: true,
		upgType: 1,
		quantity: 4,
		components: [1, 35, 369],
		quantities: [4, 2, 1]
	},
	782: {
		id: 782,
		name: '���� ���������',
		onlyOnce: true,
		upgType: 4,
		quantity: 2,
		components: [4, 5, 21, 370],
		quantities: [2, 2, 2, 1]
	},
	783: {
		id: 783,
		name: '���� �����',
		onlyOnce: true,
		upgType: 8,
		quantity: 1,
		components: [8, 77, 371],
		quantities: [1, 2, 1]
	},
	784: {
		id: 784,
		name: '���� ��������',
		onlyOnce: true,
		upgType: 35,
		quantity: 2,
		components: [35, 207, 208, 372],
		quantities: [2, 40, 40, 1]
	},
	785: {
		id: 785,
		name: '���������� -40%',
		onlyOnce: false,
		upgType: 207,
		quantity: 40,
		neededAbilities: [372],
		components: [207, 208, 372, 373],
		quantities: [40, 40, -1, 1]
	},
	786: {
		id: 786,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 2384,
		quantity: 2,
		neededAbilities: [370],
		components: [2384],
		quantities: [2]
	},
	787: {
		id: 787,
		name: '���� �����',
		onlyOnce: true,
		upgType: 2385,
		quantity: 1,
		neededAbilities: [369],
		components: [2385],
		quantities: [1]
	},
	788: {
		id: 788,
		name: '���������� -20%',
		onlyOnce: true,
		upgType: 207,
		quantity: 20,
		components: [207, 208, 374],
		quantities: [20, 20, 20]
	},
	789: {
		id: 789,
		name: '��������� ���',
		onlyOnce: true,
		upgType: 2386,
		quantity: 1,
		components: [2386],
		quantities: [1]
	},
	790: {
		id: 790,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 287,
		quantity: 1,
		components: [287],
		quantities: [1]
	},
	791: {
		id: 791,
		name: '����� ������',
		onlyOnce: true,
		upgType: 288,
		quantity: 1,
		components: [288],
		quantities: [1]
	},
	792: {
		id: 792,
		name: '����� �������',
		onlyOnce: true,
		upgType: 289,
		quantity: 1,
		components: [289],
		quantities: [1]
	},
	793: {
		id: 793,
		name: '������� �����!',
		onlyOnce: true,
		upgType: 294,
		quantity: 1,
		components: [294],
		quantities: [1]
	},
	794: {
		id: 794,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 2017,
		quantity: 2,
		neededAbilities: [370],
		components: [2017],
		quantities: [2]
	},
	795: {
		id: 795,
		name: '���������',
		onlyOnce: true,
		upgType: 2091,
		quantity: 2,
		neededAbilities: [369],
		components: [2091],
		quantities: [2]
	},
	796: {
		id: 796,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 291,
		quantity: 1,
		neededAbilities: [189],
		components: [291],
		quantities: [1]
	},
	797: {
		id: 797,
		name: '�����������',
		onlyOnce: true,
		upgType: 275,
		quantity: 1,
		components: [275],
		quantities: [1]
	},
	798: {
		id: 798,
		name: '����� �������� ������',
		onlyOnce: true,
		upgType: 62,
		quantity: 146,
		components: [62],
		quantities: [146]
	},
	799: {
		id: 799,
		name: '����� ���� +20',
		onlyOnce: false,
		upgType: 232,
		quantity: 20,
		components: [232],
		quantities: [20]
	},
	800: {
		id: 800,
		name: '�������� �����',
		onlyOnce: true,
		upgType: 282,
		quantity: 1,
		components: [282],
		quantities: [1]
	},
	801: {
		id: 801,
		name: '��������� �����',
		onlyOnce: true,
		upgType: 283,
		quantity: 1,
		components: [283],
		quantities: [1]
	},
	802: {
		id: 802,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 400,
		quantity: 1,
		components: [400],
		quantities: [1]
	},
	803: {
		id: 803,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 402,
		quantity: 1,
		components: [402],
		quantities: [1]
	},
	804: {
		id: 804,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 403,
		quantity: 1,
		components: [403],
		quantities: [1]
	},
	805: {
		id: 805,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 404,
		quantity: 1,
		components: [404],
		quantities: [1]
	},
	806: {
		id: 806,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 405,
		quantity: 1,
		components: [405],
		quantities: [1]
	},
	807: {
		id: 807,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 406,
		quantity: 1,
		components: [406],
		quantities: [1]
	},
	808: {
		id: 808,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 408,
		quantity: 1,
		components: [408],
		quantities: [1]
	},
	809: {
		id: 809,
		name: '�������� +1',
		onlyOnce: false,
		upgType: 5,
		quantity: 1,
		components: [5],
		quantities: [1]
	},
	810: {
		id: 810,
		name: '������ +1',
		onlyOnce: false,
		upgType: 930,
		quantity: 1,
		components: [930],
		quantities: [1]
	},
	811: {
		id: 811,
		name: '������� +10',
		onlyOnce: false,
		upgType: 931,
		quantity: 10,
		components: [931],
		quantities: [10]
	},
	812: {
		id: 812,
		name: '��������� +4',
		onlyOnce: false,
		upgType: 932,
		quantity: 4,
		components: [932],
		quantities: [4]
	},
	813: {
		id: 813,
		name: '����� +4',
		onlyOnce: false,
		upgType: 933,
		quantity: 4,
		components: [933],
		quantities: [4]
	},
	814: {
		id: 814,
		name: '������������',
		onlyOnce: true,
		upgType: 935,
		quantity: 1,
		components: [935],
		quantities: [1]
	},
	815: {
		id: 815,
		name: '�������� +20',
		onlyOnce: false,
		upgType: 81,
		quantity: 20,
		components: [81],
		quantities: [20]
	},
	816: {
		id: 816,
		name: '������� +7',
		onlyOnce: false,
		upgType: 82,
		quantity: 7,
		components: [82],
		quantities: [7]
	},
	817: {
		id: 817,
		name: '��������� +1',
		onlyOnce: false,
		upgType: 970,
		quantity: 1,
		components: [970],
		quantities: [1]
	},
	818: {
		id: 818,
		name: '���������� (1)',
		onlyOnce: true,
		upgType: 999,
		quantity: 1,
		components: [999],
		quantities: [1]
	},
	819: {
		id: 819,
		name: '��� ����� (1)',
		onlyOnce: true,
		upgType: 6,
		quantity: 1,
		components: [6],
		quantities: [1]
	},
	820: {
		id: 820,
		name: '�������� ������ +1',
		onlyOnce: false,
		upgType: 143,
		quantity: 1,
		components: [143],
		quantities: [1]
	},
	821: {
		id: 821,
		name: '�����',
		onlyOnce: true,
		upgType: 220,
		quantity: 23,
		components: [220, 311],
		quantities: [23, 1]
	},
	822: {
		id: 822,
		name: '�����',
		onlyOnce: true,
		upgType: 27,
		quantity: 1,
		components: [27, 101, 105, 106, 107],
		quantities: [1, 1, 1, 1, 1]
	},
	823: {
		id: 823,
		name: '�������� ����������',
		onlyOnce: true,
		upgType: 2265,
		quantity: 2,
		components: [2265, 10],
		quantities: [2, 2]
	},
	824: {
		id: 824,
		name: '����� �����������������',
		onlyOnce: true,
		upgType: 2264,
		quantity: 2,
		components: [2264, 10],
		quantities: [2, 1]
	},
	825: {
		id: 825,
		name: '��������� ���� 2',
		onlyOnce: true,
		upgType: 6,
		quantity: 2,
		components: [6],
		quantities: [2]
	},
	826: {
		id: 826,
		name: '��������� ���� +1',
		onlyOnce: false,
		upgType: 6,
		quantity: 1,
		neededAbilities: [3.006],
		components: [6],
		quantities: [1]
	},
	827: {
		id: 827,
		name: '����������� ������',
		onlyOnce: true,
		upgType: 2081,
		quantity: 2,
		neededAbilities: [309],
		components: [2081],
		quantities: [2]
	},
	828: {
		id: 828,
		name: '���� ������� +1',
		onlyOnce: false,
		upgType: 277,
		quantity: 1,
		neededAbilities: [277],
		components: [277],
		quantities: [1]
	},
	829: {
		id: 829,
		name: '���� ������� 2',
		onlyOnce: true,
		upgType: 277,
		quantity: 2,
		components: [277],
		quantities: [2]
	},
	830: {
		id: 830,
		name: '���� ������� 3',
		onlyOnce: true,
		upgType: 277,
		quantity: 3,
		components: [277],
		quantities: [3]
	},
	831: {
		id: 831,
		name: '���� ������� 4',
		onlyOnce: true,
		upgType: 277,
		quantity: 4,
		components: [277],
		quantities: [4]
	},
	832: {
		id: 832,
		name: '�������� ����',
		onlyOnce: true,
		upgType: 2379,
		quantity: 2,
		neededAbilities: [366],
		components: [2379],
		quantities: [2]
	},
	833: {
		id: 833,
		name: '������� ������',
		onlyOnce: true,
		upgType: 273,
		quantity: 1,
		components: [273],
		quantities: [1]
	},
	834: {
		id: 834,
		name: '������� �������',
		onlyOnce: true,
		upgType: 274,
		quantity: 1,
		components: [274],
		quantities: [1]
	},
	835: {
		id: 835,
		name: '������������ � ������',
		onlyOnce: true,
		upgType: 281,
		quantity: 1,
		components: [281],
		quantities: [1]
	},
	836: {
		id: 836,
		name: '���������� �������',
		onlyOnce: true,
		upgType: 8,
		quantity: 1,
		components: [8, 9, 10, 22, 158],
		quantities: [1, 2, 1, 1, 2]
	},
	837: {
		id: 837,
		name: '��������� �������� +1',
		onlyOnce: false,
		upgType: 9,
		quantity: 1,
		neededAbilities: [158],
		components: [9],
		quantities: [1]
	},
	838: {
		id: 838,
		name: '�������� ������',
		onlyOnce: true,
		upgType: 147,
		quantity: 1,
		components: [147],
		quantities: [1]
	},
	839: {
		id: 839,
		name: '������ �����',
		onlyOnce: true,
		upgType: 2389,
		quantity: 2,
		components: [2389],
		quantities: [2]
	},
	840: {
		id: 840,
		name: '������� �� ������ +1',
		onlyOnce: false,
		upgType: 202,
		quantity: 1,
		neededAbilities: [202],
		components: [202],
		quantities: [1]
	},
	841: {
		id: 841,
		name: '������� �� ������� +1',
		onlyOnce: false,
		upgType: 203,
		quantity: 1,
		neededAbilities: [203],
		components: [203],
		quantities: [1]
	},
	842: {
		id: 842,
		name: '������ ����',
		onlyOnce: true,
		upgType: 292,
		quantity: 480,
		components: [292, 380],
		quantities: [480, 1]
	},
	843: {
		id: 843,
		name: '�������������� ��������',
		onlyOnce: true,
		upgType: 293,
		quantity: 12,
		components: [293, 390],
		quantities: [12, 1]
	},
	844: {
		id: 844,
		name: '���� � ��������',
		onlyOnce: true,
		upgType: 292,
		quantity: 12,
		components: [292, 381],
		quantities: [12, 1]
	},
	845: {
		id: 845,
		name: '���������� ���������',
		onlyOnce: true,
		upgType: 390,
		quantity: -1,
		neededAbilities: [390],
		components: [390, 391],
		quantities: [-1, 1]
	},
	846: {
		id: 846,
		name: '���� �������',
		onlyOnce: false,
		upgType: 2,
		quantity: 1,
		components: [2, 3, 452],
		quantities: [1, 1, 1]
	},
	847: {
		id: 847,
		name: '��������� �������',
		onlyOnce: false,
		upgType: 4,
		quantity: 1,
		components: [4, 5, 454],
		quantities: [1, 1, 1]
	},
	848: {
		id: 848,
		name: '�������� ������� +1',
		onlyOnce: false,
		upgType: 6,
		quantity: 1,
		components: [6],
		quantities: [1]
	},
	849: {
		id: 849,
		name: '�������� �������',
		onlyOnce: false,
		upgType: 8,
		quantity: 1,
		components: [8, 458],
		quantities: [1, 1]
	},
	850: {
		id: 850,
		name: '���������� �������',
		onlyOnce: false,
		upgType: 2,
		quantity: 1,
		components: [2, 4, 5, 8, 460],
		quantities: [1, 1, 1, 1, 1]
	},
	851: {
		id: 851,
		name: '����� ��������',
		onlyOnce: true,
		upgType: 276,
		quantity: 1,
		components: [276],
		quantities: [1]
	},
	852: {
		id: 852,
		name: '��� �������',
		onlyOnce: true,
		upgType: 8,
		quantity: 6,
		components: [8, 9, 10, 23, 77],
		quantities: [6, 3, 1, 2, 6]
	}
}