duplicateUpgrades = [
	[1, 2, 3, 25, 36, 37], //life
	[4, 5, 6, 38, 39], //attack
	[7, 8, 9, 38, 39], //counterattack
	[10, 11, 12, 40, 41], //defence
	[13, 14, 15, 40, 41], //ranged defence
	[16, 17, 18, 42, 43], //resist
	[20, 21, 22], //ranged attack
	[23, 24], //distance
	[26, 54, 66, 94, 291, 529, 661, 699], //������������
	[27, 28, 29], //ammo
	[30, 31, 32, 36, 37], //stamina
	[33, 34, 35, 42, 43], //morale
	[51, 387, 423], //�������������� ���
	[52, 432], //���������
	[55, 93, 95], //������ �������
	[62, 257, 486, 591, 673], //������ ����
	[63, 257, 487, 593, 673], //������ ������
	[64, 257, 488, 595, 673], //������ �����
	[65, 96, 110, 177], //������ ������
	[71, 78, 133, 140, 174], //�������� �����
	[76, 145, 146], //����������
	[79, 178, 535], //�����������
	[80, 113, 463, 464], //����������
	[81, 686], //��������� ����
	[83, 85, 163, 590, 625, 634], //�����������
	[86, 87], //������
	[90, 91, 603, 605, 606], //������������ ������
	[97, 98, 475, 476], //�����������
	[99, 100, 101, 183, 461], //�����
	[102, 131, 356, 602], //��������� ����
	[103, 278, 279], //���������� ���
	[104, 105, 111, 346, 347, 528], //������������
	[112, 631], //����������
	[114, 506, 507], //������ �������
	[127, 129], //������ ����
	[128, 130], //������ �������
	[358, 359], //���������������
	[256, 364, 373, 526], //���� �������
	[160, 301, 413], //�������� ������
	[170, 171], //���������
	[132, 151, 173], //�������� �������
	[147, 148], //�������� �����
	[149, 150, 425, 426, 427], //�������� �������
	[157, 360, 414], //������� �����
	[344, 345], //���������
	[466, 468, 469], //������
	[500, 501, 502, 503, 504, 505, 515], //�����
	[508, 509, 513], //���� � �����
	[267, 527], //����� ����
	[679, 549, 598], //�����������
	[570, 571, 572], //Ҹ���� �������������
	[573, 574, 575], //���� ������� ������
	[581, 630, 633], //������ �������
	[73, 624], //�����
	[626, 627], //��������� �������
	[637, 638, 639], //����������
	[641, 642], //���������� �������
	[644, 645, 646], //������ ������
	[647, 648, 649], //������ ������
	[650, 651], //������������ ������
	[652, 653], //����� �����
	[654, 655], //����������� ����
	[656, 657], //���������� �������
	[659, 660], //�������� ��������
	[680, 681], //������� �� ������
	[682, 683], //������� �� �������
	[695, 696], //����������
	[704, 705], //������ ����
	[67, 324, 707, 708], //����� � �������
	[710, 711], //�������� �������
	[713, 714], //����������
	[74, 720, 721], //����� ��������
	[726, 727, 728], //���� �����
	[751, 752, 753], //�������������
];

var duplicateUpgradeMap;

var upgrades;

function onReady() {
	duplicateUpgradeMap = createDuplicateUpgradeMap();
	//testChances();
}

/*function testChances() {
	var count = 0;
	var attempts = 150;
	var chance = 0.08;
	for (var i = 0; i < attempts; i++) {
		var rnd = Math.random();
		if (rnd <= chance) {
			count++;
		}
	}
	var res = count/attempts;
	console.log(res);
}*/

function analyzer(data) {
	console.log("Unit analyzer: start");
	console.log(data);
	var cnt = $("#result");
	cnt.empty();
	var unit = unitMap[data.unitId];
	var upgrades = unit.upgrades;
	var maxLevel = parseInt(data.level);
	var upgradeId = parseInt(data.upgradeId);
	
	var usedUpgrades = [];
	for (var i = 0; i < maxLevel; i++) {
		var level = i+1;
		var proposedUpgradeIds = upgradeProposals(unit, level, usedUpgrades);
		usedUpgrades.push(proposedUpgradeIds[0]);
		console.log("level: " + level);
		console.log(proposedUpgradeIds);
	}

	return;
	
	
	
	
	var weightUpgrade = 0;
	var weightOther = 0;
	var countOther = 0;
	var missChance = 1;
	var weights =  new Array(50);
	for (var i = 0; i < weights.length; i++) weights[i] = 0;
	for (var i = 0; i < maxLevel; i++) {
		var level = i+1;
		if (i < upgrades.length) {
			var lvlUpgrades = upgrades[i];
			for (var j = 0; j < lvlUpgrades.length; j++) {
				var upgrade = lvlUpgrades[j];
				var upgId = upgrade[0];
				var weight = upgrade[1];
				if (upgId == upgradeId) {
					weightUpgrade += weight;
				} else {
					weightOther += weight;
					countOther++;
					weights[weight]++;
				}
			}
		} 
		
		if (level > 10) {
			weightUpgrade += 1;
		}
		
		var avg = weightOther/countOther;
		var avgWeightOther = getAvgWeightOther(weights, weightOther);
		//avgWeightOther = Math.round(avgWeightOther);
		avgWeightOther = avg;
		var penalty = weightPenaltyPerUpgrade(unit, level, upgradeId);
		console.log("penalty: " + penalty);
		
		var missChanceSlot1 = 1 - weightUpgrade/(weightUpgrade + weightOther);
		var missChanceSlot2 = 1 - weightUpgrade/(weightUpgrade + weightOther - avgWeightOther - penalty);
		if (missChanceSlot2 < 1) {
			//missChanceSlot2 *= (1 - 0.0005*countOther);
		}
		missChance *= missChanceSlot1*missChanceSlot2;
		
		var successChance = (1 - missChance)*100;
		console.log("level: " + level + " chance: " + successChance + " %");
		cnt.append("level: " + level + " chance: " + Math.round(successChance) + " %<br/>");
		
		weightOther = Math.max(0, weightOther - avgWeightOther);
		countOther = Math.max(0, countOther - 1);
		decrementWeight(weights, avgWeightOther);
	}
}

function upgradeProposals(unit, level, usedUpgrades) {
	var slot1UpgId = upgradeProposal(unit, level, usedUpgrades, 0);
	var slot2UpgId = upgradeProposal(unit, level, usedUpgrades, slot1UpgId);
	return [slot1UpgId, slot2UpgId];
}

function upgradeProposal(unit, level, usedUpgrades, exceptUpgId) {
	var poolSize = 1000;
	// 1
	var missedUpgrades = _.fill(Array(poolSize), 0);
	// 2
	var roller = new Array(poolSize);
	_.each(roller, function(upg, x) {
		roller[x] = {id: 0, weight: 0};
	});
	//_.fill(Array(poolSize), function() { return {id: 0, weight: 0} });
	var upgrades = unit.upgrades;
	// 3
	for (var i = 0; i < level; i++) {
		// 3.1
		if (i > 0) {
			var prevUsedUpgradeId = usedUpgrades[i-1];
			var x = _.findIndex(roller, { 'id': prevUsedUpgradeId });
			if (x != -1) {
				if (missedUpgrades[x] == 1) {
					roller[x].weight = 0;
				} else if (missedUpgrades[x] >= 2) {
					roller[x].weight -= roller[x].weight / missedUpgrades[x];
				}
				missedUpgrades[x] -= 1;
			}
		}
		
		// 3.2
		_.each(roller, function(upg, x) {
			if (upg.id > 0 && upg.weight > 0) {
				upg.weight += missedUpgrades[x];
			}
		});
		
		// 3.3
		if (i < 20) {
			var lvlUpgrades = upgrades[i];
			for (var j = 0; j < lvlUpgrades.length; j++) {
				var upgrade = lvlUpgrades[j];
				var upgId = upgrade[0];
				var weight = upgrade[1];
				var x = _.findIndex(roller, function(upg) { return upg.id == 0 || upg.id == upgId; });
				roller[x].id = upgId;
				roller[x].weight = weight;
				missedUpgrades[x] += 1;
			}
		}
	}

	// 4
	if (exceptUpgId > 0) {
		var exceptUpgType = upgradeMap[exceptUpgId].upgType;
		_.each(roller, function(upg, x) {
			if (upg.id > 0 && upgradeMap[upg.id].upgType == exceptUpgType) {
				upg.weight = 0;
			}
		});
	}
	
	// 5
	var upgrade = randomUpgrade(roller);
	
	return upgrade.id;
	
	/*
	1) ��������� � ���������� ������ MissedUpgrades; ����������� = 1000 (��� � Roller). ���� ������ �������� �� "��������" ����� � ���������, ���������� � ��������� Roller.
	2) ���������� ��� �������� � Roller.

	3) ����������� ����, ������������ ��� ������ "Lvl XX upgrades" �� unit.var, ����������� � �����, ��� ������� �� 0 �� ToLevel-1 (������ ������ ������ "Lvl 01 upgrades" � ���� = 0, �� ���� ������ ������� �� -1). ������� ����� - LevelCnt: 
	
	3.1) ���� LevelCnt > 0, ��:
	3.1.1) ���� � Roller ������ ������� X, � �������� Roller[X].Type == ID ��������, ������� ������ �����, ��� ��������� ������ LevelCnt-1;
	3.1.2) ���� ����� ������� ������, ��:
	3.1.2.1) ���� MissedUpgrades[X] ==1, �� Roller[X].Weight = 0;
	3.1.2.2) ���� MissedUpgrades[X] >=2, �� Roller[X].Weight -= Roller[X].Weight / MissedUpgrades[X];
	3.1.2.3) MissedUpgrades[X] -= 1
	
	3.2) ��� ������� �������� X �� Roller, � �������� Roller[X].Type > 0 � Roller[X].Weight > 0 ����������� ��������: Roller[X].Weight += MissedUpgrades[X];

	3.3) ���� LevelCnt < 20, ��:
	3.3.1) ����������� ����, ������������ ��� �������� �� unit.var, ������� ��������� �� ������, ����������� � ������ ����� = LevelCnt. ������� �������������� ������� - CurUpg; ��� ��������� � ������ UpgType � UpgWeight.
	3.3.1.1) �����������, ��� ��� ����������� ��� ��������� �������� CurUpg.UpgType ������ ��� ���� � ����� (���������� ��� �� ���������� ��������� ��� ����� ������). ��� ������ �������� ����� ���� Need � unit_upg.var.
	3.3.1.2) ���� ���� ��� ��������� ������, ��:
	3.3.1.2.1) ���� � Roller ������ ������� X � Roller[X].Type == 0 ��� Roller[X].Type == CurUpg.UpgType;
	3.3.1.2.2) ����������� ���������� �������� Roller[X] �������������� CurUpg: Roller[X].Type = CurUpg.UpgType; Roller[X].Weight = CurUpg.Weight;
	3.3.1.2.3) MissedUpgrades[X] += 1;

	4) �������� Roller[X].Weight ��� ������� X, � �������� ��� ������ ������ �������� Roller[X].Type ����� ���� ������ ������ �������� � ������� ExceptUpg. ��� "������ ������" �������� � ������ �� ����� "Upg Type" ��� ������� � unit_upg.var.

	5) �� Roller ���������� ��������� ������� � ������������ � "������" (����������� ������ �������� ����� ��������������� ��� ����). ID ����� �������� ������������ ��������. 
	*/
}

function randomUpgrade(roller) {
	var sumOfWeights = roller.reduce(function(memo, creature) {
		return memo + creature.weight;
	}, 0);

	function getRandom(sumOfWeights) {
	  var random = Math.floor(Math.random() * (sumOfWeights + 1));

	  return function(creature) {
		random -= creature.weight;
		return random <= 0;
	  };
	}

	var upgrade = roller.find(getRandom(sumOfWeights));
	return upgrade;
}


function decrementWeight(weights, weight) {
	var w = Math.ceil(weight);
	for (var i = w; i < weights.length; i++) {
		if (weights[i]) {
			weights[i]--;
			break;
		}
	}
}

function getAvgWeightOther(weights, weightTotal) {
	var res = 0;
	var rest = weightTotal;
	for (var i = 0; i < weights.length; i++) {
		var count = weights[i];
		if (count) {
			var w = i*count;
			res += i*w/weightTotal;
			rest -= w;
		}
	}
	if (rest > 0) {
		res += rest*rest/weightTotal;
	}
	return res
}

function weightPenaltyPerUpgrade(unit, level, upgradeId) {
	var upgrades = unit.upgrades;
	var weightPenalty = 0;
	var count = 0;
	
	var flatUpgrades = _.flatten(upgrades.slice(0, level));
	
	for (var i = 0; i < flatUpgrades.length; i++) {
		var upgrade1 = flatUpgrades[i];
		var upgId1 = upgrade1[0];
		var weight1 = upgrade1[1];
		if (upgId1 != upgradeId) {
			count++;
		}
		for (var j = 0; j < flatUpgrades.length; j++) {
			var upgrade2 = flatUpgrades[j];
			var upgId2 = upgrade2[0];
			var weight2 = upgrade2[1];
			if (upgId1 != upgradeId && upgId2 != upgradeId && i != j) {
				if (duplicateUpgradeMap[upgId1] && _.includes(duplicateUpgradeMap[upgId1], upgId2)) {
					weightPenalty += weight2;
				}
			}
		}
	}
	
	var res = weightPenalty/count
	return res
}

function createDuplicateUpgradeMap() {
	var res = {}
	for (var i = 0; i < duplicateUpgrades.length; i++) {
		var similarUpgrades = duplicateUpgrades[i];
		for (var j = 0; j < similarUpgrades.length; j++) {
			var upgradeId = similarUpgrades[j];
			if (!res[upgradeId]) {
				res[upgradeId] = [];
			}
			res[upgradeId] = _.uniq(res[upgradeId].concat(similarUpgrades));
		}
	}
	console.log(res);
	return res
}