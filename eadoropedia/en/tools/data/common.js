upgTypesToExclude = [171, 172, 173, 174, 175, 189, 193, 194, 305, 306, 307, 308, 329];
//[189, 311, 310, 309, 220, 130, 255, 258, 256, 257, 170, 175, 172, 332, 193, 194, 333, 334, 329, 330, 331, 153, 224, 254, 192, 129, 99, 160, 115, 133, 174, 171, 173, 188, 268];

karmaValues = {
	0: 'Evil Incarnate',
	1: 'Evil',
	2: 'Unscrupulous',
	3: 'Neutral',
	4: 'Lawful',
	5: 'Good',
	6: 'Champion of Light'
}

unitSubtypes = {
	1: 'Mortal',
	2: 'Undead',
	3: 'Demon',
	4: 'Mechanical',
	5: 'Hero',
	6: 'Human',
	7: 'Elf',
	8: 'Dwarf',
	9: 'Goblin',
	10: 'Orc',
	11: 'Halfling',
	12: 'Centaur',
	13: 'Lizardman',
	14: 'Drow',
	15: 'Gnoll',
	16: 'Alkaryl',
	17: 'Ratman',
	18: 'Servant of Death',
	19: 'Animal',
	20: 'Trap',
	21: 'Chaos Lord',
	22: 'Elemental Spirit',
	23: 'Golem',
	24: 'Giant',
	25: 'Skeleton',
	26: 'Cultist',
	27: 'Artillery',
	28: 'Natural Weapon',
	29: 'Obstacle'
}

unitClasses = {
	1: 'Infantry',
	2: 'Ranged',
	3: 'Cavalry',
	4: 'Spellcaster',
	5: 'Artillery',
	6: 'Giant',
	7: 'Flying'
}

unitAbilityToAttrMap = {
	1: 'life',
	2: 'attack',
	3: 'counterAttack',
	4: 'defence',
	5: 'rangedDefence',
	6: 'resist',
	7: 'speed',
	8: 'rangedAttack',
	9: 'shootingRange',
	10: 'ammo',
	11: 'stamina',
	12: 'morale'
}

unitClassToUpgWeightMap = {
	// Infantry
	1: {
		1: 1, // life
		2: 1.2, // attack
		3: 1.2, // counterAttack
		4: 1.5, // defence
		5: 1.3, // rangedDefence
		6: 1.4, // resist
		7: 6, // speed
		8: 1.1, // rangedAttack
		9: 2, // shootingRange
		10: 1.1, // ammo
		11: 0.7, // stamina
		12: 0.7, // morale
	},
	// Ranged
	2: {
		1: 1, // life
		2: 0.3, // attack
		3: 0.3, // counterAttack
		4: 0.5, // defence
		5: 0.6, // rangedDefence
		6: 0.6, // resist
		7: 2, // speed
		8: 3, // rangedAttack
		9: 11, // shootingRange
		10: 1.1, // ammo
		11: 0.6, // stamina
		12: 0.6, // morale
	},
	// Spellcaster
	4: {
		1: 1, // life
		2: 0.3, // attack
		3: 0.3, // counterAttack
		4: 0.5, // defence
		5: 0.6, // rangedDefence
		6: 0.6, // resist
		7: 2, // speed
		8: 3, // rangedAttack
		9: 11, // shootingRange
		10: 1.1, // ammo
		11: 0.6, // stamina
		12: 0.6, // morale
	},
}

resourceMap = {
	1: 'Iron', 
	2: 'Redwood', 
	3: 'Horses', 
	4: 'Mandrake', 
	5: 'Arcanite', 
	6: 'Marble', 
	7: 'Mithril', 
	8: 'Dionium', 
	9: 'Black Lotus'
}
