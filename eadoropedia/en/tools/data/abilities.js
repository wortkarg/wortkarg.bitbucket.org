abilityMap = {
	1: {
		id: 1,
		name: 'Hit Points',
		number: 1,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2: {
		id: 2,
		name: 'Attack',
		number: 2,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	3: {
		id: 3,
		name: 'Counterattack',
		number: 3,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	4: {
		id: 4,
		name: 'Defense',
		number: 4,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	5: {
		id: 5,
		name: 'Ranged Defense',
		number: 5,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	6: {
		id: 6,
		name: 'Resistance',
		number: 6,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	7: {
		id: 7,
		name: 'Speed',
		number: 7,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	8: {
		id: 8,
		name: 'Ranged Attack',
		number: 8,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	9: {
		id: 9,
		name: 'Range',
		number: 9,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	10: {
		id: 10,
		name: 'Ammo',
		number: 10,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	11: {
		id: 11,
		name: 'Stamina',
		number: 11,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	12: {
		id: 12,
		name: 'Morale',
		number: 12,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	13: {
		id: 13,
		name: 'Feels No Pain',
		number: 13,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	14: {
		id: 14,
		name: 'Flying',
		number: 14,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	15: {
		id: 15,
		name: 'Floating',
		number: 15,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	16: {
		id: 16,
		name: 'First Strike',
		number: 16,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	31: {
		id: 17,
		name: 'Armor Piercing Shot',
		number: 31,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	18: {
		id: 18,
		name: 'Tireless',
		number: 18,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	19: {
		id: 19,
		name: 'Intrepid',
		number: 19,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	20: {
		id: 20,
		name: 'Double Shot',
		number: 20,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	21: {
		id: 21,
		name: 'Recuperation',
		number: 21,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	22: {
		id: 22,
		name: 'Meditation',
		number: 22,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	23: {
		id: 23,
		name: 'Collect Ammo',
		number: 23,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	24: {
		id: 24,
		name: 'Healing',
		number: 24,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	25: {
		id: 25,
		name: 'Battle Frenzy',
		number: 25,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	26: {
		id: 26,
		name: 'Agility',
		number: 26,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	27: {
		id: 27,
		name: 'Magic Strike',
		number: 27,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	28: {
		id: 28,
		name: 'Magic Shot',
		number: 28,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	29: {
		id: 29,
		name: 'Forced March',
		number: 29,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	110: {
		id: 30,
		name: 'Spell Immunity',
		number: 110,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	213: {
		id: 31,
		name: 'Break spell',
		number: 213,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	32: {
		id: 32,
		name: 'Forest Knowledge',
		number: 32,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	33: {
		id: 33,
		name: 'Hills Knowledge',
		number: 33,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	34: {
		id: 34,
		name: 'Swamp Knowledge',
		number: 34,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	35: {
		id: 35,
		name: 'First Aid',
		number: 35,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	36: {
		id: 36,
		name: 'Immobilized',
		number: 36,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	37: {
		id: 37,
		name: 'Charge',
		number: 37,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	38: {
		id: 38,
		name: 'Does not fight',
		number: 38,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	30: {
		id: 39,
		name: 'Armorpiercing Strike',
		number: 30,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	39: {
		id: 40,
		name: 'Stunning Blow',
		number: 39,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	41: {
		id: 41,
		name: 'Poison Attack',
		number: 41,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	108: {
		id: 42,
		name: 'Poison Immunity',
		number: 108,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	96: {
		id: 43,
		name: 'Hex',
		number: 96,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	44: {
		id: 44,
		name: 'Steal Ammo',
		number: 44,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	45: {
		id: 45,
		name: 'Damage Armor',
		number: 45,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	46: {
		id: 46,
		name: 'Petrification',
		number: 46,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	47: {
		id: 47,
		name: 'Web',
		number: 47,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	48: {
		id: 48,
		name: 'Regeneration',
		number: 48,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	226: {
		id: 49,
		name: 'Necrophage',
		number: 226,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	50: {
		id: 50,
		name: 'Intimidation',
		number: 50,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	127: {
		id: 51,
		name: 'Crippling Strike',
		number: 127,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	52: {
		id: 52,
		name: 'Damage Weapon',
		number: 52,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	53: {
		id: 53,
		name: 'Parry',
		number: 53,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	54: {
		id: 54,
		name: 'Forager',
		number: 54,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	55: {
		id: 55,
		name: 'Siege',
		number: 55,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	56: {
		id: 56,
		name: 'Marauder',
		number: 56,
		numeric: true,
		effect: false,
		percent: true,
		unique: false
	}, 
	57: {
		id: 57,
		name: 'Soul Stealing',
		number: 57,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	58: {
		id: 58,
		name: 'Plunderer',
		number: 58,
		numeric: true,
		effect: false,
		percent: true,
		unique: false
	}, 
	59: {
		id: 59,
		name: 'Crushing Blow',
		number: 59,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	60: {
		id: 60,
		name: 'Enchanted Arrows',
		number: 60,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	120: {
		id: 61,
		name: 'Smite Evil',
		number: 120,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	62: {
		id: 62,
		name: 'Change Type',
		number: 62,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	63: {
		id: 63,
		name: 'Trample',
		number: 63,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	64: {
		id: 64,
		name: 'Lord of the Undead',
		number: 64,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	65: {
		id: 65,
		name: 'Mass Attack',
		number: 65,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	66: {
		id: 66,
		name: 'Round Attack',
		number: 66,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	67: {
		id: 67,
		name: 'Roots',
		number: 67,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	68: {
		id: 68,
		name: 'Bloodsucker',
		number: 68,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	69: {
		id: 69,
		name: 'Heavy Ammo',
		number: 69,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	76: {
		id: 70,
		name: 'Precise Strike',
		number: 76,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	77: {
		id: 71,
		name: 'Precise Shot',
		number: 77,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	42: {
		id: 72,
		name: 'Poison Shot',
		number: 42,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	179: {
		id: 73,
		name: 'Cause Vulnerability',
		number: 179,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	178: {
		id: 74,
		name: 'Cause Atrophy',
		number: 178,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	81: {
		id: 75,
		name: 'Gold Income',
		number: 81,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	82: {
		id: 76,
		name: 'Gem Income',
		number: 82,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	83: {
		id: 77,
		name: 'Learn Spell',
		number: 83,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	84: {
		id: 78,
		name: 'Summon Creature',
		number: 84,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	85: {
		id: 79,
		name: 'Increased Upkeep',
		number: 85,
		numeric: false,
		effect: false,
		percent: true,
		unique: false
	}, 
	88: {
		id: 80,
		name: 'Cannot Move',
		number: 88,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	905: {
		id: 81,
		name: 'Spellpower',
		number: 905,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	906: {
		id: 82,
		name: 'Spell Duration',
		number: 906,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	907: {
		id: 83,
		name: 'Summoning Power',
		number: 907,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	908: {
		id: 84,
		name: 'Resistance Negation',
		number: 908,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	909: {
		id: 85,
		name: 'Undead Summoning Power',
		number: 909,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	980: {
		id: 86,
		name: 'Fast Draw',
		number: 980,
		numeric: false,
		effect: true,
		percent: false,
		unique: false
	}, 
	981: {
		id: 87,
		name: 'Sabotage',
		number: 981,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	982: {
		id: 88,
		name: 'Province Plunder',
		number: 982,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	983: {
		id: 89,
		name: 'Decrease Spell Cost',
		number: 983,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	984: {
		id: 90,
		name: 'Fast Casting',
		number: 984,
		numeric: false,
		effect: true,
		percent: false,
		unique: false
	}, 
	985: {
		id: 91,
		name: 'Gold for Quests',
		number: 985,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	990: {
		id: 92,
		name: 'Army Experience',
		number: 990,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	991: {
		id: 93,
		name: 'Hero Experience',
		number: 991,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	992: {
		id: 94,
		name: 'Looting',
		number: 992,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	993: {
		id: 95,
		name: 'Ranged Weapon',
		number: 993,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	994: {
		id: 96,
		name: 'Wand Firing',
		number: 994,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	995: {
		id: 97,
		name: 'Item Maintenance',
		number: 995,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	996: {
		id: 98,
		name: 'Province Exploration',
		number: 996,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	997: {
		id: 99,
		name: 'Lower Upkeep',
		number: 997,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	998: {
		id: 100,
		name: 'Mobility',
		number: 998,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	999: {
		id: 101,
		name: 'Initiative',
		number: 999,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	90: {
		id: 102,
		name: 'Transformed',
		number: 90,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	100: {
		id: 103,
		name: 'Summoned',
		number: 100,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	2010: {
		id: 104,
		name: 'Hex Spell',
		number: 2010,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2021: {
		id: 105,
		name: 'Vulnerability Spell',
		number: 2021,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2023: {
		id: 106,
		name: 'Blast Spell',
		number: 2023,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2011: {
		id: 107,
		name: 'Bless Spell',
		number: 2011,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2038: {
		id: 108,
		name: 'Exorcism Spell',
		number: 2038,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2009: {
		id: 109,
		name: 'Air Shield Spell',
		number: 2009,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2013: {
		id: 110,
		name: 'Fear Spell',
		number: 2013,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2014: {
		id: 111,
		name: 'Raise Skeleton Spell',
		number: 2014,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2003: {
		id: 112,
		name: 'Inspiration Spell',
		number: 2003,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2019: {
		id: 113,
		name: 'Summon Imp Spell',
		number: 2019,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2016: {
		id: 114,
		name: 'Dispel Spell',
		number: 2016,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2057: {
		id: 115,
		name: 'Life Drain Spell',
		number: 2057,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	116: {
		id: 116,
		name: 'Command',
		number: 116,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	117: {
		id: 117,
		name: 'Magic',
		number: 117,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	118: {
		id: 118,
		name: 'Health',
		number: 118,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	119: {
		id: 119,
		name: 'Experience Level',
		number: 119,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	86: {
		id: 120,
		name: 'Guard Contract',
		number: 86,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	87: {
		id: 121,
		name: 'Building Schematics',
		number: 87,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	2017: {
		id: 122,
		name: 'Astral Energy Spell',
		number: 2017,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2015: {
		id: 123,
		name: 'Raise Zombie Spell',
		number: 2015,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2008: {
		id: 124,
		name: 'Haste Spell',
		number: 2008,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2007: {
		id: 125,
		name: 'Head Wind Spell',
		number: 2007,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	94: {
		id: 126,
		name: 'Ritual Scroll',
		number: 94,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	987: {
		id: 127,
		name: 'Treasure Hunt',
		number: 987,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	2037: {
		id: 128,
		name: 'Restoration Spell',
		number: 2037,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2052: {
		id: 129,
		name: 'Word of Power Spell',
		number: 2052,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2049: {
		id: 130,
		name: 'Resurrection Spell',
		number: 2049,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2056: {
		id: 131,
		name: 'Mass Curse Spell',
		number: 2056,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	95: {
		id: 132,
		name: 'Energy Control',
		number: 95,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	43: {
		id: 133,
		name: 'Poison Flesh',
		number: 43,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	97: {
		id: 134,
		name: 'Reincarnation',
		number: 97,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	98: {
		id: 135,
		name: 'Tolerance',
		number: 98,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	910: {
		id: 136,
		name: 'Necromancer`s Call',
		number: 910,
		numeric: false,
		effect: true,
		percent: false,
		unique: false
	}, 
	2028: {
		id: 137,
		name: 'Raise Ghoul',
		number: 2028,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2055: {
		id: 138,
		name: 'Raise Ghost',
		number: 2055,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2012: {
		id: 139,
		name: 'Cure Wounds Spell',
		number: 2012,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2078: {
		id: 140,
		name: 'Aard Sign',
		number: 2078,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2079: {
		id: 141,
		name: 'Igni Sign',
		number: 2079,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2080: {
		id: 142,
		name: 'Quen Sign',
		number: 2080,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2081: {
		id: 143,
		name: 'Undead Regeneration Spell',
		number: 2081,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2082: {
		id: 144,
		name: 'Stinking Cloud Spell',
		number: 2082,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2083: {
		id: 145,
		name: 'Sinister Howl',
		number: 2083,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2084: {
		id: 146,
		name: 'Mind Control Spell',
		number: 2084,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2085: {
		id: 147,
		name: 'Sacrifice Spell',
		number: 2085,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2086: {
		id: 148,
		name: 'Fireblast Spell',
		number: 2086,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2087: {
		id: 149,
		name: 'Hellgates Spell',
		number: 2087,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2088: {
		id: 150,
		name: 'Suicide Spell',
		number: 2088,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2089: {
		id: 151,
		name: 'Submission Spell',
		number: 2089,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2090: {
		id: 152,
		name: 'Unholy Word Spell',
		number: 2090,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2091: {
		id: 153,
		name: 'Healing Spell',
		number: 2091,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	101: {
		id: 154,
		name: 'Fire Immunity',
		number: 101,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2092: {
		id: 155,
		name: 'Summon Huorn Spell',
		number: 2092,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2093: {
		id: 156,
		name: 'Magic Shield Spell',
		number: 2093,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2094: {
		id: 157,
		name: 'Summon Wasp Swarm Spell',
		number: 2094,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2095: {
		id: 158,
		name: 'Summon Wasp Cloud Spell',
		number: 2095,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2096: {
		id: 159,
		name: 'Life Breath Spell',
		number: 2096,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	140: {
		id: 160,
		name: 'Explosive Shot',
		number: 140,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	141: {
		id: 161,
		name: 'Fire Shot',
		number: 141,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2099: {
		id: 162,
		name: 'Rune of Renewal Spell',
		number: 2099,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2100: {
		id: 163,
		name: 'Rune of Decay Spell',
		number: 2100,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2101: {
		id: 164,
		name: 'Rune of Restoration Spell',
		number: 2101,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2102: {
		id: 165,
		name: 'Rune of Power Spell',
		number: 2102,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2103: {
		id: 166,
		name: 'Stone Spikes Spell',
		number: 2103,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2104: {
		id: 167,
		name: 'Vampire Touch Spell',
		number: 2104,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2105: {
		id: 168,
		name: 'Earth Shield Spell',
		number: 2105,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2106: {
		id: 169,
		name: 'Fire Rage Spell',
		number: 2106,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2107: {
		id: 170,
		name: 'Rage Spell',
		number: 2107,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2367: {
		id: 171,
		name: 'Soothe Spirits Spell',
		number: 2367,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2109: {
		id: 172,
		name: 'Mass Rage Spell',
		number: 2109,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2110: {
		id: 173,
		name: 'Tribe`s Horde Spell',
		number: 2110,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2111: {
		id: 174,
		name: 'Chieftain Order Spell',
		number: 2111,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2112: {
		id: 175,
		name: 'Quagmire Spell',
		number: 2112,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2113: {
		id: 176,
		name: 'Weakness Spell',
		number: 2113,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2114: {
		id: 177,
		name: 'Armor Break Spell',
		number: 2114,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2115: {
		id: 178,
		name: 'Plague Spell',
		number: 2115,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2116: {
		id: 179,
		name: 'Geyser Spell',
		number: 2116,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2117: {
		id: 180,
		name: 'Power Word - Curse',
		number: 2117,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2118: {
		id: 181,
		name: 'Power Word - Hold',
		number: 2118,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2119: {
		id: 182,
		name: 'Power Word - Pain',
		number: 2119,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2120: {
		id: 183,
		name: 'Summon Water Elemental Spell',
		number: 2120,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2121: {
		id: 184,
		name: 'Glacial Heart Spell',
		number: 2121,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2122: {
		id: 185,
		name: 'Freeze Spell',
		number: 2122,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2123: {
		id: 186,
		name: 'Ice Chunk Spell',
		number: 2123,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2124: {
		id: 187,
		name: 'Armor Piercing Shot',
		number: 2124,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2125: {
		id: 188,
		name: 'Thick Fog Spell',
		number: 2125,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2126: {
		id: 189,
		name: 'Double Shot Spell',
		number: 2126,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2127: {
		id: 190,
		name: 'Enchanted Arrow Spell',
		number: 2127,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2128: {
		id: 191,
		name: 'Ancestral Rage Spell',
		number: 2128,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2129: {
		id: 192,
		name: 'Ancestral Shield Spell',
		number: 2129,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2130: {
		id: 193,
		name: 'Sparks Spell',
		number: 2130,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2131: {
		id: 194,
		name: 'Electric Shock Spell',
		number: 2131,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2132: {
		id: 195,
		name: 'Storm Spell',
		number: 2132,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2137: {
		id: 196,
		name: 'Hornet Swarm Spell',
		number: 2137,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2133: {
		id: 197,
		name: 'Poison Claw Spell',
		number: 2133,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2370: {
		id: 198,
		name: 'Set Trap Spell',
		number: 2370,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2371: {
		id: 199,
		name: 'Set Snare Spell',
		number: 2371,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2372: {
		id: 200,
		name: 'Set Hive Spell',
		number: 2372,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2134: {
		id: 201,
		name: 'Swamp Blood Spell',
		number: 2134,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2138: {
		id: 202,
		name: 'Voracity Spell',
		number: 2138,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2140: {
		id: 203,
		name: 'Devour Spell',
		number: 2140,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2039: {
		id: 204,
		name: 'Word of Life Spell',
		number: 2039,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2051: {
		id: 205,
		name: 'Mass Heal Spell',
		number: 2051,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2050: {
		id: 206,
		name: 'Mass Defense Spell',
		number: 2050,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2058: {
		id: 207,
		name: 'White Magic Spell',
		number: 2058,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2141: {
		id: 208,
		name: 'Swiftness Spell',
		number: 2141,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2142: {
		id: 209,
		name: 'Dispel Magic Spell',
		number: 2142,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2143: {
		id: 210,
		name: 'Demon Slayer Spell',
		number: 2143,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2144: {
		id: 211,
		name: 'Grace Spell',
		number: 2144,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2145: {
		id: 212,
		name: 'Divine Seal Spell',
		number: 2145,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2146: {
		id: 213,
		name: 'Armor of Faith Spell',
		number: 2146,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2147: {
		id: 214,
		name: 'True Resurrection Spell',
		number: 2147,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2148: {
		id: 215,
		name: 'Sanctuary Spell',
		number: 2148,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2149: {
		id: 216,
		name: 'Divine Warp Spell',
		number: 2149,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2150: {
		id: 217,
		name: 'Divine Light Spell',
		number: 2150,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2151: {
		id: 218,
		name: 'Holy Word Spell',
		number: 2151,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2152: {
		id: 219,
		name: 'Divine Vengeance Spell',
		number: 2152,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2153: {
		id: 220,
		name: 'Summon Skeleton Spell',
		number: 2153,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2074: {
		id: 221,
		name: 'Enslave Mind Spell',
		number: 2074,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2154: {
		id: 222,
		name: 'Summon Wyvern Spell',
		number: 2154,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2155: {
		id: 223,
		name: 'Fire Storm Spell',
		number: 2155,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2156: {
		id: 224,
		name: 'Chilling Lightning Spell',
		number: 2156,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2157: {
		id: 225,
		name: 'Order Spell',
		number: 2157,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	102: {
		id: 226,
		name: 'Ice Immunity',
		number: 102,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	103: {
		id: 227,
		name: 'Air Immunity',
		number: 103,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	139: {
		id: 228,
		name: 'Bombardment',
		number: 139,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	104: {
		id: 229,
		name: 'Mind Control Immunity',
		number: 104,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2159: {
		id: 230,
		name: 'Celestial Thunder Spell',
		number: 2159,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2160: {
		id: 231,
		name: 'Chain Lightning Spell',
		number: 2160,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	49: {
		id: 232,
		name: 'Swamp Rage',
		number: 49,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2162: {
		id: 233,
		name: 'Sluggishness Spell',
		number: 2162,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2163: {
		id: 234,
		name: 'Quicksand Spell',
		number: 2163,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2164: {
		id: 235,
		name: 'Earthquake Spell',
		number: 2164,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2165: {
		id: 236,
		name: 'Fire Rain Spell',
		number: 2165,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2166: {
		id: 237,
		name: 'Mass Fire Immunity Spell',
		number: 2166,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2167: {
		id: 238,
		name: 'Cleansing Flame Spell',
		number: 2167,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2168: {
		id: 239,
		name: 'Summon Air Elemental Spell',
		number: 2168,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2169: {
		id: 240,
		name: 'Summon Earth Elemental Spell',
		number: 2169,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2170: {
		id: 241,
		name: 'Summon Fire Elemental Spell',
		number: 2170,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2171: {
		id: 242,
		name: 'Summon Living Armor Spell',
		number: 2171,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2172: {
		id: 243,
		name: 'Summon Lord of Abyss Spell',
		number: 2172,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2173: {
		id: 244,
		name: 'Anthem Of The Damned',
		number: 2173,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2035: {
		id: 245,
		name: 'Summon Hellhound Spell',
		number: 2035,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2174: {
		id: 246,
		name: 'Skeleton Transformation Spell',
		number: 2174,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2175: {
		id: 247,
		name: 'Fiend Transformation Spell',
		number: 2175,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2176: {
		id: 248,
		name: 'Cleric Transformation Spell',
		number: 2176,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2024: {
		id: 249,
		name: 'Fire Blade Spell',
		number: 2024,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2048: {
		id: 250,
		name: 'Mass Dispel Spell',
		number: 2048,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2177: {
		id: 251,
		name: 'Summon Phantom Spell',
		number: 2177,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2178: {
		id: 252,
		name: 'Fanaticism Spell',
		number: 2178,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2179: {
		id: 253,
		name: 'Deadly Terror Spell',
		number: 2179,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2180: {
		id: 254,
		name: 'Panic Spell',
		number: 2180,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2181: {
		id: 255,
		name: 'Imaginary Assassin Spell',
		number: 2181,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2182: {
		id: 256,
		name: 'Bind Will Spell',
		number: 2182,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2183: {
		id: 257,
		name: 'Strong Mind Spell',
		number: 2183,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	260: {
		id: 258,
		name: 'DELETED',
		number: 260,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	261: {
		id: 259,
		name: 'DELETED',
		number: 261,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	262: {
		id: 260,
		name: 'DELETED',
		number: 262,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	263: {
		id: 261,
		name: 'DELETED',
		number: 263,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	264: {
		id: 262,
		name: 'DELETED',
		number: 264,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	265: {
		id: 263,
		name: 'DELETED',
		number: 265,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	266: {
		id: 264,
		name: 'DELETED',
		number: 266,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	267: {
		id: 265,
		name: 'DELETED',
		number: 267,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	2184: {
		id: 266,
		name: 'Arrow Shower Spell',
		number: 2184,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2185: {
		id: 267,
		name: 'Control Undead Spell',
		number: 2185,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	142: {
		id: 268,
		name: 'Seismic Charge',
		number: 142,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2187: {
		id: 269,
		name: 'Mockery Spell',
		number: 2187,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2188: {
		id: 270,
		name: 'Levitation Spell',
		number: 2188,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2189: {
		id: 271,
		name: 'Embrace of Darkness Spell',
		number: 2189,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2190: {
		id: 272,
		name: 'Deadly Touch Spell',
		number: 2190,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2191: {
		id: 273,
		name: 'Banshee`s Howl Spell',
		number: 2191,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2047: {
		id: 274,
		name: 'Antimagic Spell',
		number: 2047,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2192: {
		id: 275,
		name: 'Paralyze Spell',
		number: 2192,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2193: {
		id: 276,
		name: 'Mass Sleep Spell',
		number: 2193,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	314: {
		id: 277,
		name: 'Shadow Form Spell',
		number: 314,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	315: {
		id: 278,
		name: 'Shadow Form Spell',
		number: 315,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2196: {
		id: 279,
		name: 'Summon Beholder Spell',
		number: 2196,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2197: {
		id: 280,
		name: 'Dark Crystal Spell',
		number: 2197,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2337: {
		id: 281,
		name: 'Drow Rage Spell',
		number: 2337,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2098: {
		id: 282,
		name: 'Blinding Darkness Spell',
		number: 2098,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2198: {
		id: 283,
		name: 'Kiss of Darkness Spell',
		number: 2198,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	300: {
		id: 284,
		name: 'Astral Parasite Spell',
		number: 300,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2200: {
		id: 285,
		name: 'Hands of Darkness Spell',
		number: 2200,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2097: {
		id: 286,
		name: 'Astral Thief Spell',
		number: 2097,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2201: {
		id: 287,
		name: 'Dark Depletion Spell',
		number: 2201,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2207: {
		id: 288,
		name: 'Dark Resurrection Spell',
		number: 2207,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2208: {
		id: 289,
		name: 'Cloud of Darkness Spell',
		number: 2208,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2203: {
		id: 290,
		name: 'Raise Reaper Spell',
		number: 2203,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2210: {
		id: 291,
		name: 'Cloud of Restoration Spell',
		number: 2210,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2211: {
		id: 292,
		name: 'Mass Undead Restoration Spell',
		number: 2211,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2213: {
		id: 293,
		name: 'Eternal Slave Spell',
		number: 2213,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2224: {
		id: 294,
		name: 'Dissolve Armor Spell',
		number: 2224,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2226: {
		id: 295,
		name: 'Axii Sign',
		number: 2226,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2227: {
		id: 296,
		name: 'Yrden Sign',
		number: 2227,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2204: {
		id: 297,
		name: 'Raise Mummy Spell',
		number: 2204,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2205: {
		id: 298,
		name: 'Raise Cadaver Spell',
		number: 2205,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2061: {
		id: 299,
		name: 'Raise Vampire Spell',
		number: 2061,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2214: {
		id: 300,
		name: 'Word of Death Spell',
		number: 2214,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2229: {
		id: 301,
		name: 'Summon Devil Spell',
		number: 2229,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2206: {
		id: 302,
		name: 'Raise Banshee Spell',
		number: 2206,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2230: {
		id: 303,
		name: 'Death Cloud Spell',
		number: 2230,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2231: {
		id: 304,
		name: 'Mass Life Drain Spell',
		number: 2231,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2034: {
		id: 305,
		name: 'Summon Fiend Spell',
		number: 2034,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2045: {
		id: 306,
		name: 'Summon Demon Spell',
		number: 2045,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2232: {
		id: 307,
		name: 'Demon Blood Spell',
		number: 2232,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2233: {
		id: 308,
		name: 'Mass Bless Spell',
		number: 2233,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2234: {
		id: 309,
		name: 'Healing Sleep',
		number: 2234,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2069: {
		id: 310,
		name: 'Corruption Spell',
		number: 2069,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2043: {
		id: 311,
		name: 'Dark Pact Spell',
		number: 2043,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	165: {
		id: 312,
		name: 'Skillful Healer',
		number: 165,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2237: {
		id: 313,
		name: 'Chaos Distortion Spell',
		number: 2237,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2238: {
		id: 314,
		name: 'Chaos Surge Spell',
		number: 2238,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2244: {
		id: 315,
		name: 'Fearless Spell',
		number: 2244,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2245: {
		id: 316,
		name: 'Rune Shot Spell',
		number: 2245,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2252: {
		id: 317,
		name: 'Containment Spell',
		number: 2252,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2253: {
		id: 318,
		name: 'Dark Redemption Spell',
		number: 2253,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	166: {
		id: 319,
		name: 'Ammo Bearer',
		number: 166,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2254: {
		id: 320,
		name: 'Ice Snake Spell',
		number: 2254,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2255: {
		id: 321,
		name: 'Lightning Ball Spell',
		number: 2255,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2256: {
		id: 322,
		name: 'Scorching Ground Spell',
		number: 2256,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2257: {
		id: 323,
		name: 'Solar Blast Spell',
		number: 2257,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2258: {
		id: 324,
		name: 'Necromancer`s Cry Spell',
		number: 2258,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2261: {
		id: 325,
		name: 'Demonic Howl',
		number: 2261,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2356: {
		id: 326,
		name: 'Infernal Howl',
		number: 2356,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	122: {
		id: 327,
		name: 'Blood Frenzy',
		number: 122,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	125: {
		id: 328,
		name: 'Manic Rage',
		number: 125,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	210: {
		id: 329,
		name: 'Cold Rage',
		number: 210,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2355: {
		id: 330,
		name: 'Healing Circle Spell',
		number: 2355,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	158: {
		id: 331,
		name: 'Entangling Shot',
		number: 158,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	205: {
		id: 332,
		name: 'Skillful Looter',
		number: 205,
		numeric: false,
		effect: true,
		percent: true,
		unique: true
	}, 
	2354: {
		id: 333,
		name: 'Summon Mocker Spell',
		number: 2354,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2352: {
		id: 334,
		name: 'Abysmal Joke Spell',
		number: 2352,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2353: {
		id: 335,
		name: 'Abysmal Swallow Spell',
		number: 2353,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	346: {
		id: 336,
		name: 'Troll Brand',
		number: 346,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	347: {
		id: 337,
		name: 'Wolf Brand',
		number: 347,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	348: {
		id: 338,
		name: 'Cerberus Brand',
		number: 348,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2042: {
		id: 339,
		name: 'Mass Slow Spell',
		number: 2042,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	215: {
		id: 340,
		name: 'Avatar of Light',
		number: 215,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2064: {
		id: 341,
		name: 'Armageddon Spell',
		number: 2064,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	206: {
		id: 342,
		name: 'Workaholic',
		number: 206,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	305: {
		id: 343,
		name: 'Siege Mode',
		number: 305,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	306: {
		id: 344,
		name: 'Siege Mode',
		number: 306,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	307: {
		id: 345,
		name: 'Siege Mode',
		number: 307,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	308: {
		id: 346,
		name: 'Siege Mode',
		number: 308,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	349: {
		id: 347,
		name: 'Spawn Brand',
		number: 349,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	350: {
		id: 348,
		name: 'Avenger Brand',
		number: 350,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	351: {
		id: 349,
		name: 'Succubus Brand',
		number: 351,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	352: {
		id: 350,
		name: 'Human Brand',
		number: 352,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	353: {
		id: 351,
		name: 'Ogre Brand',
		number: 353,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	354: {
		id: 352,
		name: 'Alkaryl Brand',
		number: 354,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	177: {
		id: 353,
		name: 'Cause Fever',
		number: 177,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	136: {
		id: 354,
		name: 'Festering Wounds',
		number: 136,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	137: {
		id: 355,
		name: 'Cause Putrefaction',
		number: 137,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	143: {
		id: 356,
		name: 'Flaming Blade',
		number: 143,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	144: {
		id: 357,
		name: 'Flaming Arrows',
		number: 144,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	146: {
		id: 358,
		name: 'Scorching',
		number: 146,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	109: {
		id: 359,
		name: 'Immunity to Bleeding',
		number: 109,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	78: {
		id: 360,
		name: 'Sniper Shot',
		number: 78,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	79: {
		id: 361,
		name: 'Powerful Shot',
		number: 79,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	187: {
		id: 362,
		name: 'Chain Lightning',
		number: 187,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	147: {
		id: 363,
		name: 'Explosive Weapon',
		number: 147,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	150: {
		id: 364,
		name: 'Cannot Cast',
		number: 150,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	161: {
		id: 365,
		name: 'Skillful Spellcaster',
		number: 161,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	2158: {
		id: 366,
		name: 'Warp Spell',
		number: 2158,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	235: {
		id: 367,
		name: 'Darkness Surge',
		number: 235,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	236: {
		id: 368,
		name: 'Storm Shadow',
		number: 236,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2288: {
		id: 369,
		name: 'Cocoon of Darkness Spell',
		number: 2288,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	157: {
		id: 370,
		name: 'Wounding Spikes',
		number: 157,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2348: {
		id: 371,
		name: 'Astral Schism Spell',
		number: 2348,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2269: {
		id: 372,
		name: 'Summon Hnara Spell',
		number: 2269,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2270: {
		id: 373,
		name: 'Miracle Spell',
		number: 2270,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	124: {
		id: 374,
		name: 'Fix',
		number: 124,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2273: {
		id: 375,
		name: 'Mobility Spell',
		number: 2273,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2260: {
		id: 376,
		name: 'Enchanted Fang Spell',
		number: 2260,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2274: {
		id: 377,
		name: 'Reload Spell',
		number: 2274,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2275: {
		id: 378,
		name: 'Summon Wolves Spell',
		number: 2275,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2276: {
		id: 379,
		name: 'Summon Dire Bear Spell',
		number: 2276,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	167: {
		id: 380,
		name: 'Supplier',
		number: 167,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	2279: {
		id: 381,
		name: 'Test of Faith Spell',
		number: 2279,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2280: {
		id: 382,
		name: 'Summon Icarus Spell',
		number: 2280,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2281: {
		id: 383,
		name: 'Summon Stalker Spell',
		number: 2281,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2282: {
		id: 384,
		name: 'Summon Luminary Spell',
		number: 2282,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2283: {
		id: 385,
		name: 'Soul Trap Spell',
		number: 2283,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2278: {
		id: 386,
		name: 'Strike of Pain Spell',
		number: 2278,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2284: {
		id: 387,
		name: 'Death Bell Spell',
		number: 2284,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	186: {
		id: 388,
		name: 'Shield Bash',
		number: 186,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2287: {
		id: 389,
		name: 'Concoction Spell',
		number: 2287,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	154: {
		id: 390,
		name: 'Gas Bomb',
		number: 154,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	176: {
		id: 391,
		name: 'Cause Leprosy',
		number: 176,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	355: {
		id: 392,
		name: 'Magic Spark',
		number: 355,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2291: {
		id: 393,
		name: 'Fever Carrier',
		number: 2291,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2292: {
		id: 394,
		name: 'Atrophy Carrier',
		number: 2292,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2293: {
		id: 395,
		name: 'Leprosy Carrier',
		number: 2293,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2294: {
		id: 396,
		name: 'Summon Rat Spell',
		number: 2294,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2295: {
		id: 397,
		name: 'Fever Hotbed',
		number: 2295,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2247: {
		id: 398,
		name: 'Battle Cry Spell',
		number: 2247,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2296: {
		id: 399,
		name: 'Atrophy Hotbed',
		number: 2296,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2297: {
		id: 400,
		name: 'Leprosy Hotbed',
		number: 2297,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2298: {
		id: 401,
		name: 'Curse Spell',
		number: 2298,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	184: {
		id: 402,
		name: 'Self-control',
		number: 184,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	123: {
		id: 403,
		name: 'Dark Replenishment',
		number: 123,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	202: {
		id: 404,
		name: 'Hunter of Undead',
		number: 202,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	203: {
		id: 405,
		name: 'Hunter of Demons',
		number: 203,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	303: {
		id: 406,
		name: 'Stone Crust Spell',
		number: 303,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	317: {
		id: 407,
		name: 'Shadow Form Spell',
		number: 317,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2062: {
		id: 408,
		name: 'Pestilence Spell',
		number: 2062,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	204: {
		id: 409,
		name: 'Scavenger',
		number: 204,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	318: {
		id: 410,
		name: 'Shadow Form Spell',
		number: 318,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2301: {
		id: 411,
		name: 'Blind Faith Spell',
		number: 2301,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2302: {
		id: 412,
		name: 'Poison Rain Spell',
		number: 2302,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	121: {
		id: 413,
		name: 'Dark Dominion',
		number: 121,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	105: {
		id: 414,
		name: 'Web Immunity',
		number: 105,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	106: {
		id: 415,
		name: 'Petrification Immunity',
		number: 106,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	107: {
		id: 416,
		name: 'Entanglement Immunity',
		number: 107,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2303: {
		id: 417,
		name: 'Summon Imp Spell',
		number: 2303,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2304: {
		id: 418,
		name: 'Summon Spawn Spell',
		number: 2304,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2305: {
		id: 419,
		name: 'Summon Fiend Spell',
		number: 2305,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2306: {
		id: 420,
		name: 'Summon Hellhound Spell',
		number: 2306,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2307: {
		id: 421,
		name: 'Summon Fiend Lord Spell',
		number: 2307,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2308: {
		id: 422,
		name: 'Summon Cerberus Spell',
		number: 2308,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2309: {
		id: 423,
		name: 'Summon Succubus Spell',
		number: 2309,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2310: {
		id: 424,
		name: 'Summon Demon Spell',
		number: 2310,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2311: {
		id: 425,
		name: 'Summon Destroyer Spell',
		number: 2311,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2312: {
		id: 426,
		name: 'Summon Devil Spell',
		number: 2312,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	301: {
		id: 427,
		name: 'Black Blood Spell',
		number: 301,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2272: {
		id: 428,
		name: 'Healing Circle Spell',
		number: 2272,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	80: {
		id: 429,
		name: 'Incorporeal',
		number: 80,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	312: {
		id: 430,
		name: 'Gaseous Form Spell',
		number: 312,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2299: {
		id: 431,
		name: 'Dash Spell',
		number: 2299,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2317: {
		id: 432,
		name: 'Scorn Spell',
		number: 2317,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2318: {
		id: 433,
		name: 'Hero`s Ballad Spell',
		number: 2318,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2319: {
		id: 434,
		name: 'Rune Armor Spell',
		number: 2319,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2320: {
		id: 435,
		name: 'Rune Weapon Spell',
		number: 2320,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2321: {
		id: 436,
		name: 'Rune Amulet Spell',
		number: 2321,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	304: {
		id: 437,
		name: 'Caustic Mixture Spell',
		number: 304,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	180: {
		id: 438,
		name: 'Chilling Touch',
		number: 180,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	181: {
		id: 439,
		name: 'Phantom Form',
		number: 181,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	182: {
		id: 440,
		name: 'Dissolve Armor',
		number: 182,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	191: {
		id: 441,
		name: 'Reincarnation',
		number: 191,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	319: {
		id: 442,
		name: 'Shadow Form Spell',
		number: 319,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2324: {
		id: 443,
		name: 'Necrosurge Spell',
		number: 2324,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2325: {
		id: 444,
		name: 'Word of the Lord Spell',
		number: 2325,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	302: {
		id: 445,
		name: 'Hunter`s Net',
		number: 302,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2326: {
		id: 446,
		name: 'Mental Support Spell',
		number: 2326,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2327: {
		id: 447,
		name: 'Mental Duel Spell',
		number: 2327,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2328: {
		id: 448,
		name: 'Mental Sermon Spell',
		number: 2328,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2329: {
		id: 449,
		name: 'Mental Resonance Spell',
		number: 2329,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2330: {
		id: 450,
		name: 'Act of Faith Spell',
		number: 2330,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2331: {
		id: 451,
		name: 'Summon Arisen Spell',
		number: 2331,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2332: {
		id: 452,
		name: 'Summon Keeper Spell',
		number: 2332,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2333: {
		id: 453,
		name: 'Mental Shadow Spell',
		number: 2333,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	198: {
		id: 454,
		name: 'Frenzy',
		number: 198,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	128: {
		id: 455,
		name: 'Crippling Shot',
		number: 128,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	196: {
		id: 456,
		name: 'Overwhelming Force',
		number: 196,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	195: {
		id: 457,
		name: 'Surge of Strength',
		number: 195,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	185: {
		id: 458,
		name: 'Shield Wall',
		number: 185,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	168: {
		id: 459,
		name: 'Life Leech',
		number: 168,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	40: {
		id: 460,
		name: 'Stunning Shot',
		number: 40,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	51: {
		id: 461,
		name: 'Intimidating Shot',
		number: 51,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	155: {
		id: 462,
		name: 'Wounding Strike',
		number: 155,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	156: {
		id: 463,
		name: 'Wounding Shot',
		number: 156,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	61: {
		id: 464,
		name: 'Enchanted Blade',
		number: 61,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	197: {
		id: 465,
		name: 'Bloodlust',
		number: 197,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	134: {
		id: 466,
		name: 'Damaged Aura',
		number: 134,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	135: {
		id: 467,
		name: 'Astral Ammo',
		number: 135,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	126: {
		id: 468,
		name: 'Vigilance',
		number: 126,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	979: {
		id: 469,
		name: 'Skillful Diplomat',
		number: 979,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	313: {
		id: 470,
		name: 'Stone Statue Spell',
		number: 313,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2242: {
		id: 471,
		name: 'Raise Skeleton of Doom Spell',
		number: 2242,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	131: {
		id: 472,
		name: 'Switch Weapon',
		number: 131,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	132: {
		id: 473,
		name: 'Switch Ammo',
		number: 132,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	2059: {
		id: 474,
		name: 'Reincarnation Spell',
		number: 2059,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	201: {
		id: 475,
		name: 'Armageddon',
		number: 201,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	2338: {
		id: 476,
		name: 'Dark Balance Spell',
		number: 2338,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2339: {
		id: 477,
		name: 'Blood Shadow Spell',
		number: 2339,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2340: {
		id: 478,
		name: 'Summon Slave Spell',
		number: 2340,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2341: {
		id: 479,
		name: 'Stimulus Spell',
		number: 2341,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	316: {
		id: 480,
		name: 'Shadow Form Spell',
		number: 316,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2343: {
		id: 481,
		name: 'Trembling Shadow Spell',
		number: 2343,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2346: {
		id: 482,
		name: 'Shadow Dance Spell',
		number: 2346,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	17: {
		id: 483,
		name: 'Pike Wall',
		number: 17,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	217: {
		id: 484,
		name: 'Interception',
		number: 217,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	218: {
		id: 485,
		name: 'Ghost Wrath',
		number: 218,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	320: {
		id: 486,
		name: 'Mass Bloodlust',
		number: 320,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	321: {
		id: 487,
		name: 'Mass Bloodlust',
		number: 321,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	322: {
		id: 488,
		name: 'Mass Bloodlust',
		number: 322,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	323: {
		id: 489,
		name: 'Mass Combustion',
		number: 323,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	324: {
		id: 490,
		name: 'Mass Combustion',
		number: 324,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	325: {
		id: 491,
		name: 'Mass Combustion',
		number: 325,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	326: {
		id: 492,
		name: 'Mass Vampirism',
		number: 326,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	327: {
		id: 493,
		name: 'Mass Vampirism',
		number: 327,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	328: {
		id: 494,
		name: 'Mass Vampirism',
		number: 328,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2363: {
		id: 495,
		name: 'Earth Totem Spell',
		number: 2363,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2364: {
		id: 496,
		name: 'Rage Totem Spell',
		number: 2364,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2365: {
		id: 497,
		name: 'Fire Totem Spell',
		number: 2365,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2366: {
		id: 498,
		name: 'Blood Totem Spell',
		number: 2366,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2368: {
		id: 499,
		name: 'Elemental Strength Spell',
		number: 2368,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	911: {
		id: 500,
		name: 'Summoning Range',
		number: 911,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2369: {
		id: 501,
		name: 'Summon Fire Elemental Spell',
		number: 2369,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	335: {
		id: 502,
		name: 'Mass Bloodlust',
		number: 335,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	169: {
		id: 503,
		name: 'Alertness',
		number: 169,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	3030: {
		id: 504,
		name: 'Orc Commander',
		number: 3030,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	214: {
		id: 505,
		name: 'Vitality',
		number: 214,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	336: {
		id: 506,
		name: 'Lifeblood',
		number: 336,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	337: {
		id: 507,
		name: 'Lifeblood',
		number: 337,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	338: {
		id: 508,
		name: 'Lifeblood',
		number: 338,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	339: {
		id: 509,
		name: 'Lifeblood',
		number: 339,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	232: {
		id: 510,
		name: 'Armor of Faith',
		number: 232,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	216: {
		id: 511,
		name: 'Panic',
		number: 216,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	340: {
		id: 512,
		name: 'Trap',
		number: 340,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	341: {
		id: 513,
		name: 'Snare',
		number: 341,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	342: {
		id: 514,
		name: 'Hornet`s Nest',
		number: 342,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	249: {
		id: 515,
		name: 'Itch',
		number: 249,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	2139: {
		id: 516,
		name: 'Prod Spell',
		number: 2139,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2373: {
		id: 517,
		name: 'Subdue Beast Spell',
		number: 2373,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	240: {
		id: 518,
		name: 'Hit and Run',
		number: 240,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	228: {
		id: 519,
		name: 'Backstab',
		number: 228,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	2378: {
		id: 520,
		name: 'Rat Swarm Spell',
		number: 2378,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	343: {
		id: 521,
		name: 'Vexillary',
		number: 343,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	344: {
		id: 522,
		name: 'Cause Fever',
		number: 344,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	345: {
		id: 523,
		name: 'Cause Atrophy',
		number: 345,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	227: {
		id: 524,
		name: 'Restore Ammo',
		number: 227,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	221: {
		id: 525,
		name: 'Heavy Armor',
		number: 221,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	222: {
		id: 526,
		name: 'Dodge',
		number: 222,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	356: {
		id: 527,
		name: 'Acid',
		number: 356,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	269: {
		id: 528,
		name: 'Gobble',
		number: 269,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	360: {
		id: 529,
		name: 'Forest Protection Spell',
		number: 360,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	361: {
		id: 530,
		name: 'Forest Might Spell',
		number: 361,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	362: {
		id: 531,
		name: 'Forest Blessing Spell',
		number: 362,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2374: {
		id: 532,
		name: 'Improved Charm Spell',
		number: 2374,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	358: {
		id: 533,
		name: 'Path of Power',
		number: 358,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	359: {
		id: 534,
		name: 'Path of Agility',
		number: 359,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	363: {
		id: 535,
		name: 'Path of Fire',
		number: 363,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	364: {
		id: 536,
		name: 'Path of Wind',
		number: 364,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	365: {
		id: 537,
		name: 'Path of Water',
		number: 365,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	366: {
		id: 538,
		name: 'Path of Earth',
		number: 366,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2383: {
		id: 539,
		name: 'Bear Form Spell',
		number: 2383,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2375: {
		id: 540,
		name: 'Mass Improved Charm Spell',
		number: 2375,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	368: {
		id: 541,
		name: 'Path of the Sun',
		number: 368,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	367: {
		id: 542,
		name: 'Path of the Stars',
		number: 367,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	369: {
		id: 543,
		name: 'Path of Life',
		number: 369,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	370: {
		id: 544,
		name: 'Path of Tenacity',
		number: 370,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	371: {
		id: 545,
		name: 'Path of the Warrior',
		number: 371,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	372: {
		id: 546,
		name: 'Path of the Hunter',
		number: 372,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	373: {
		id: 547,
		name: 'Path of the Hunter',
		number: 373,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2384: {
		id: 548,
		name: 'Spiritual Link Spell',
		number: 2384,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2385: {
		id: 549,
		name: 'Life Bond Spell',
		number: 2385,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	374: {
		id: 550,
		name: 'Decreased Upkeep',
		number: 374,
		numeric: true,
		effect: false,
		percent: true,
		unique: true
	}, 
	2386: {
		id: 551,
		name: 'Soul Sipping Spell',
		number: 2386,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	287: {
		id: 552,
		name: 'Bane of Mortals',
		number: 287,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	288: {
		id: 553,
		name: 'Bane of Undead',
		number: 288,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	289: {
		id: 554,
		name: 'Bane of Demons',
		number: 289,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	284: {
		id: 555,
		name: 'Decreased Upkeep',
		number: 284,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	291: {
		id: 556,
		name: 'Wizard Mastery',
		number: 291,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	275: {
		id: 557,
		name: 'Mobility',
		number: 275,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	282: {
		id: 558,
		name: 'Heavenly Armor',
		number: 282,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	283: {
		id: 559,
		name: 'Cursed Armor',
		number: 283,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	400: {
		id: 560,
		name: 'Commander',
		number: 400,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	402: {
		id: 561,
		name: 'Commander',
		number: 402,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	403: {
		id: 562,
		name: 'Commander',
		number: 403,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	404: {
		id: 563,
		name: 'Commander',
		number: 404,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	405: {
		id: 564,
		name: 'Commander',
		number: 405,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	406: {
		id: 565,
		name: 'Commander',
		number: 406,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	408: {
		id: 566,
		name: 'Commander',
		number: 408,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	930: {
		id: 567,
		name: 'Blacksmith',
		number: 930,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	931: {
		id: 568,
		name: 'Doctor',
		number: 931,
		numeric: true,
		effect: true,
		percent: true,
		unique: false
	}, 
	932: {
		id: 569,
		name: 'Authority',
		number: 932,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	933: {
		id: 570,
		name: 'Leader',
		number: 933,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	935: {
		id: 571,
		name: 'Inspired',
		number: 935,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	3006: {
		id: 572,
		name: 'Magic Shield',
		number: 3006,
		numeric: true,
		effect: true,
		percent: false,
		unique: true
	}, 
	970: {
		id: 573,
		name: 'Saboteur',
		number: 970,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	295: {
		id: 574,
		name: 'Spark',
		number: 295,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	311: {
		id: 575,
		name: 'Blast',
		number: 311,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	2265: {
		id: 576,
		name: 'Mass Grounding Spell',
		number: 2265,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	2264: {
		id: 577,
		name: 'Mass Cold Immunity Spell',
		number: 2264,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	277: {
		id: 578,
		name: 'Gnoll Might',
		number: 277,
		numeric: true,
		effect: false,
		percent: false,
		unique: false
	}, 
	2379: {
		id: 579,
		name: 'Summon Eagle Spell',
		number: 2379,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	273: {
		id: 580,
		name: 'Lord of the Dead',
		number: 273,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	274: {
		id: 581,
		name: 'Lord of Demons',
		number: 274,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	281: {
		id: 582,
		name: 'Resistance to Fear',
		number: 281,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	2389: {
		id: 583,
		name: 'Summon Naiad Spell',
		number: 2389,
		numeric: true,
		effect: false,
		percent: false,
		unique: true
	}, 
	380: {
		id: 584,
		name: 'Wolf Pack',
		number: 380,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	390: {
		id: 585,
		name: 'Ranged Support',
		number: 390,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	381: {
		id: 586,
		name: 'Strength in Unity',
		number: 381,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	391: {
		id: 587,
		name: 'Ranged Support',
		number: 391,
		numeric: false,
		effect: true,
		percent: false,
		unique: true
	}, 
	452: {
		id: 588,
		name: 'Tribe Power',
		number: 452,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	454: {
		id: 589,
		name: 'Tribe Resilience',
		number: 454,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	458: {
		id: 590,
		name: 'Tribe Marksmanship',
		number: 458,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	460: {
		id: 591,
		name: 'Tribe Might',
		number: 460,
		numeric: true,
		effect: true,
		percent: false,
		unique: false
	}, 
	310: {
		id: 10310,
		name: 'Vulnerability Spell',
		number: 310,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	309: {
		id: 10309,
		name: 'Fear',
		number: 309,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	220: {
		id: 10220,
		name: 'Special ability',
		number: 220,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	130: {
		id: 10130,
		name: 'Special ability',
		number: 130,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	255: {
		id: 10255,
		name: 'Special ability',
		number: 255,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	258: {
		id: 10258,
		name: 'Special ability',
		number: 258,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	256: {
		id: 10256,
		name: 'Special ability',
		number: 256,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	257: {
		id: 10257,
		name: 'Special ability',
		number: 257,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	189: {
		id: 10189,
		name: 'Special ability',
		number: 189,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	170: {
		id: 10170,
		name: 'Special ability',
		number: 170,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	175: {
		id: 10175,
		name: 'Special ability',
		number: 175,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	172: {
		id: 10172,
		name: 'Special ability',
		number: 172,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	332: {
		id: 10332,
		name: 'Special ability',
		number: 332,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	193: {
		id: 10193,
		name: 'Special ability',
		number: 193,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	194: {
		id: 10194,
		name: 'Special ability',
		number: 194,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	333: {
		id: 10333,
		name: 'Special ability',
		number: 333,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	334: {
		id: 10334,
		name: 'Special ability',
		number: 334,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	329: {
		id: 10329,
		name: 'Special ability',
		number: 329,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	330: {
		id: 10330,
		name: 'Special ability',
		number: 330,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	331: {
		id: 10331,
		name: 'Special ability',
		number: 331,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	153: {
		id: 10153,
		name: 'Special ability',
		number: 153,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	224: {
		id: 10224,
		name: 'Special ability',
		number: 224,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	254: {
		id: 10254,
		name: 'Special ability',
		number: 254,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	192: {
		id: 10192,
		name: 'Special ability',
		number: 192,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	129: {
		id: 10129,
		name: 'Special ability',
		number: 129,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	99: {
		id: 10099,
		name: 'Special ability ',
		number: 99,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	160: {
		id: 10160,
		name: 'Special ability',
		number: 160,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	115: {
		id: 10115,
		name: 'Special ability',
		number: 115,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	133: {
		id: 10133,
		name: 'Special ability',
		number: 133,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	174: {
		id: 10174,
		name: 'Special ability',
		number: 174,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	171: {
		id: 10171,
		name: 'Special ability',
		number: 171,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	173: {
		id: 10173,
		name: 'Special ability',
		number: 173,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	188: {
		id: 10188,
		name: 'Special ability',
		number: 188,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	268: {
		id: 10268,
		name: 'Special ability',
		number: 268,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	357: {
		id: 10357,
		name: 'Special ability',
		number: 357,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	162: {
		id: 10162,
		name: 'Special ability',
		number: 162,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	207: {
		id: 10207,
		name: 'Special ability',
		number: 207,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	208: {
		id: 10208,
		name: 'Special ability',
		number: 208,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	294: {
		id: 10294,
		name: 'Special ability',
		number: 294,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	209: {
		id: 10209,
		name: 'Special ability',
		number: 209,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	248: {
		id: 10248,
		name: 'Special ability',
		number: 248,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1019: {
		id: 1019,
		name: 'Skeleton',
		number: 1019,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1020: {
		id: 1020,
		name: 'Zombie',
		number: 1020,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1021: {
		id: 1021,
		name: 'Imp',
		number: 1021,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	244: {
		id: 10244,
		name: 'Special ability',
		number: 244,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	243: {
		id: 10243,
		name: 'Special ability',
		number: 243,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	70: {
		id: 10070,
		name: 'Special ability ',
		number: 70,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1036: {
		id: 1036,
		name: 'Ghoul',
		number: 1036,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1039: {
		id: 1039,
		name: 'Gargoyle',
		number: 1039,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	247: {
		id: 10247,
		name: 'Special ability',
		number: 247,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1037: {
		id: 1037,
		name: 'Fiend',
		number: 1037,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1038: {
		id: 1038,
		name: 'Hellhound',
		number: 1038,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	93: {
		id: 10093,
		name: 'Special ability ',
		number: 93,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	233: {
		id: 10233,
		name: 'Special ability',
		number: 233,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	91: {
		id: 10091,
		name: 'Special ability ',
		number: 91,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	242: {
		id: 10242,
		name: 'Special ability',
		number: 242,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1056: {
		id: 1056,
		name: 'Demon',
		number: 1056,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	89: {
		id: 10089,
		name: 'Special ability ',
		number: 89,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	73: {
		id: 10073,
		name: 'Special ability ',
		number: 73,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1055: {
		id: 1055,
		name: 'Ghost',
		number: 1055,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	71: {
		id: 10071,
		name: 'Special ability ',
		number: 71,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	72: {
		id: 10072,
		name: 'Special ability ',
		number: 72,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	74: {
		id: 10074,
		name: 'Special ability ',
		number: 74,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1064: {
		id: 1064,
		name: 'Vampire',
		number: 1064,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1065: {
		id: 1065,
		name: 'Devil',
		number: 1065,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	245: {
		id: 10245,
		name: 'Special ability',
		number: 245,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	75: {
		id: 10075,
		name: 'Special ability ',
		number: 75,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1060: {
		id: 1060,
		name: 'Dragon',
		number: 1060,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1260: {
		id: 1260,
		name: 'Mule',
		number: 1260,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	92: {
		id: 10092,
		name: 'Special ability ',
		number: 92,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1071: {
		id: 1071,
		name: 'Stone Golem',
		number: 1071,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	149: {
		id: 10149,
		name: 'Special ability',
		number: 149,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1070: {
		id: 1070,
		name: 'Phoenix',
		number: 1070,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	145: {
		id: 10145,
		name: 'Special ability',
		number: 145,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1108: {
		id: 1108,
		name: 'Huorn',
		number: 1108,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	298: {
		id: 10298,
		name: 'Special ability',
		number: 298,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	148: {
		id: 10148,
		name: 'Special ability',
		number: 148,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	151: {
		id: 10151,
		name: 'Special ability',
		number: 151,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1118: {
		id: 1118,
		name: 'Water Elemental',
		number: 1118,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	241: {
		id: 10241,
		name: 'Special ability',
		number: 241,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	164: {
		id: 10164,
		name: 'Special ability',
		number: 164,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1150: {
		id: 1150,
		name: 'Wyvern',
		number: 1150,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	246: {
		id: 10246,
		name: 'Special ability',
		number: 246,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1153: {
		id: 1153,
		name: 'Air Elemental',
		number: 1153,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1154: {
		id: 1154,
		name: 'Earth Elemental',
		number: 1154,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1155: {
		id: 1155,
		name: 'Fire Elemental',
		number: 1155,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1159: {
		id: 1159,
		name: 'Living Armor',
		number: 1159,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1098: {
		id: 1098,
		name: 'Lord of Abyss',
		number: 1098,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1045: {
		id: 1045,
		name: 'Priest',
		number: 1045,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1163: {
		id: 1163,
		name: 'Phantom',
		number: 1163,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	183: {
		id: 10183,
		name: 'Special ability',
		number: 183,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	212: {
		id: 10212,
		name: 'Special ability',
		number: 212,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	211: {
		id: 10211,
		name: 'Special ability',
		number: 211,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1182: {
		id: 1182,
		name: 'Beholder',
		number: 1182,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1186: {
		id: 1186,
		name: 'Dark Crystal',
		number: 1186,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1170: {
		id: 1170,
		name: 'Reaper',
		number: 1170,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1092: {
		id: 1092,
		name: 'Mummy',
		number: 1092,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1171: {
		id: 1171,
		name: 'Cadaver',
		number: 1171,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1172: {
		id: 1172,
		name: 'Banshee',
		number: 1172,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	159: {
		id: 10159,
		name: 'Special ability',
		number: 159,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1097: {
		id: 1097,
		name: 'Destroyer',
		number: 1097,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	252: {
		id: 10252,
		name: 'Special ability',
		number: 252,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1193: {
		id: 1193,
		name: 'Skeleton of Doom',
		number: 1193,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1194: {
		id: 1194,
		name: 'Spectre',
		number: 1194,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	200: {
		id: 10200,
		name: 'Special ability',
		number: 200,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1207: {
		id: 1207,
		name: 'Lord of Darkness',
		number: 1207,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	225: {
		id: 10225,
		name: 'Special ability',
		number: 225,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1208: {
		id: 1208,
		name: 'Hnara',
		number: 1208,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1083: {
		id: 1083,
		name: 'Wolf',
		number: 1083,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	251: {
		id: 10251,
		name: 'Special ability',
		number: 251,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1210: {
		id: 1210,
		name: 'Dire Bear',
		number: 1210,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1212: {
		id: 1212,
		name: 'Icarus',
		number: 1212,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1214: {
		id: 1214,
		name: 'Stalker',
		number: 1214,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1213: {
		id: 1213,
		name: 'Luminary',
		number: 1213,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1216: {
		id: 1216,
		name: 'Trapped Soul',
		number: 1216,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1218: {
		id: 1218,
		name: 'Mephit',
		number: 1218,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1224: {
		id: 1224,
		name: 'Magic Crystal',
		number: 1224,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1222: {
		id: 1222,
		name: 'Giant Rat',
		number: 1222,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	297: {
		id: 10297,
		name: 'Special ability',
		number: 297,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	234: {
		id: 10234,
		name: 'Special ability',
		number: 234,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1099: {
		id: 1099,
		name: 'Spawn',
		number: 1099,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1191: {
		id: 1191,
		name: 'Fiend Lord',
		number: 1191,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1094: {
		id: 1094,
		name: 'Cerberus',
		number: 1094,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1095: {
		id: 1095,
		name: 'Succubus',
		number: 1095,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	230: {
		id: 10230,
		name: 'Special ability',
		number: 230,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1279: {
		id: 1279,
		name: 'Arisen Luminary',
		number: 1279,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1280: {
		id: 1280,
		name: 'Memory Keeper',
		number: 1280,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	152: {
		id: 10152,
		name: 'Special ability',
		number: 152,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1187: {
		id: 1187,
		name: 'Kobold-slave',
		number: 1187,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1205: {
		id: 1205,
		name: 'Mocker',
		number: 1205,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1229: {
		id: 1229,
		name: 'Earth Totem',
		number: 1229,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1230: {
		id: 1230,
		name: 'Rage Totem',
		number: 1230,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1231: {
		id: 1231,
		name: 'Fire Totem',
		number: 1231,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1234: {
		id: 1234,
		name: 'Blood Totem',
		number: 1234,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1227: {
		id: 1227,
		name: 'Trap',
		number: 1227,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1169: {
		id: 1169,
		name: 'Snare',
		number: 1169,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1228: {
		id: 1228,
		name: 'Hornet`s Nest',
		number: 1228,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	223: {
		id: 10223,
		name: 'Special ability',
		number: 223,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	296: {
		id: 10296,
		name: 'Special ability',
		number: 296,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1082: {
		id: 1082,
		name: 'Giant Eagle',
		number: 1082,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	285: {
		id: 10285,
		name: 'Special ability',
		number: 285,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1287: {
		id: 1287,
		name: 'Quasit',
		number: 1287,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1288: {
		id: 1288,
		name: 'Ifrit',
		number: 1288,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	1289: {
		id: 1289,
		name: 'Naiad',
		number: 1289,
		numeric: false,
		effect: false,
		percent: false,
		unique: false
	}, 
	292: {
		id: 1290,
		name: 'Wolf Pack',
		number: 292,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}, 
	293: {
		id: 1291,
		name: 'Ranged Support',
		number: 293,
		numeric: false,
		effect: false,
		percent: false,
		unique: true
	}
}