var imgBaseUrl = 'https://wortkarg.bitbucket.io/eadoropedia/images/';
var bMap = innerBuildingMap;
var ids = [];
for (var id in bMap) {
	ids.push(parseInt(id));
}
var sortedIds = _.sortBy(ids, [function(id) { return bMap[id].name; }]);

var layouts = {
	breadthfirst: {
		name: 'breadthfirst',
		directed: true,
		padding: 10
	},
	dagre: {
		name: 'dagre',
		rankDir: 'BT'
	}
}

function param(name) {
    return (location.search.split(name + '=')[1] || '').split('&')[0];
}

function updateCost() {
	var gold = 0;
	var gems = 0;
	var resources = []
    for (const id of selected) {
		var b = bMap[id];
		gold += b.goldCost;
		gems += b.gemCost;
		resources = _.concat(resources, b.resources).filter(r => !!r);
	}
	var goldTotal = gold + _.sumBy(resources, r => resourceCostMap[r][0]);
	var gemsTotal = gems + _.sumBy(resources, r => resourceCostMap[r][1]);
	$("#gold").text(gold);
	$("#gems").text(gems);
	$("#gold-total").text(goldTotal);
	$("#gems-total").text(gemsTotal);
	for (var i = 0; i < 9; i++) {
		var c = resources.filter(r => r == (i+1)).length;
		$("#resource" + (i+1)).text(c);
		if (c == 0) {
			$("#rescnt" + (i+1)).hide();
		} else {
			$("#rescnt" + (i+1)).show();
		}
	}
	$("#count").text(selected.length);
}

var selected = [];
var buildingId = param('id') ? parseInt(param('id')) : null;
for (const id of sortedIds) {
	var b = bMap[id];
	$("#building").append("<option value='" + id + "'>" + b.name + "</option>");
}

if (buildingId) {
	$("#building").val(buildingId);
}
$("#building").on("change", function() {
	var bId = $(this).val();
	cy.remove('*');
	if (bId != '') {
		cy.add(getElements(parseInt(bId)));
	}
	updateTooltips();
	$("#layout").trigger("change");
});

$("#layout").on("change", function() {
	var layoutName = $(this).val();
	var layout = cy.layout(layouts[layoutName]);
    layout.run();
});

$("#select-all").on("click", function() {
	selected = [];
	cy.nodes().forEach(function(n) {
		n.addClass("selected");
		selected.push(n[0].data("oid"));
	});
	updateCost();
});

$("#deselect-all").on("click", function() {
	selected = [];
	cy.nodes().forEach(function(n) {
		n.removeClass("selected");
	});
	updateCost();
});

function addNodeIds(id, nodeIds) {
	if (!_.includes(nodeIds, id)) {
		nodeIds.push(id);
		bMap[id].deps.forEach((d) => {
			addNodeIds(d, nodeIds)
		});
	}
}

function getElements(bId) {
	var nodes = [];
	var edges = [];
	if (bId) {
		var nodeIds = getNodeIds(bId);
		for (const id of ids) {
			var b = bMap[id];
			if (nodeIds.includes(id)) {
				nodes.push({
					data: { id: 'b' + id, oid: id, name: b.name },
					classes: 'bbl' + b.block
				});
				for (d of b.deps) {
					edges.push({ data: { source: 'b' + d, target: 'b' + id } });
				}
			}
		}
	}
	var elements = {
		nodes: nodes,
		edges: edges
	}
	return elements;
}

function getNodeIds(bId, excludeRoot) {
	var nodeIds = [];
	addNodeIds(bId, nodeIds);
	return excludeRoot ? _.pull(nodeIds, bId) : nodeIds;
}

function getNodes(bId, excludeRoot) {
	var nodeIds = getNodeIds(bId, excludeRoot);
	return cy.nodes().filter(function(n){
		return _.includes(nodeIds, n.data('oid'));
	});
}

function styles(stylesheet) {
	stylesheet
	.selector('node')
		.css({
			'height': 52,
			'width': 52,
			'shape': 'square',
			'background-fit': 'cover',
			'border-color': '#000',
			'border-width': 3,
			'border-opacity': 1.0,
			'font-size': 10,
			'text-wrap': 'wrap',
			'text-max-width': 100
		})
	.selector('node[label]')
		.css({
			'text-margin-y': "20px",
		})
	.selector('.selected')
		.css({
			'opacity': 0.5
		})
	.selector('edge')
		.css({
			'curve-style': 'bezier',
			'width': 4,
			'target-arrow-shape': 'triangle',
			'line-color': '#3b392d',
			'target-arrow-color': '#3b392d'
		})
	.selector('.eating')
		.css({
			'border-color': 'red'
		})
	.selector('.eater')
		.css({
			'border-width': 9
		});

	var quarters = ['#d42127', '#d87808', '#046bb6', '#a3655a', '#99799e', '#6391a8', '#16a8d9', '#047a3e', '#71be4a'];
	for (var i = 0; i < quarters.length; i++) {
		stylesheet.selector('.bbl' + (i+1)).css({
			'border-color': quarters[i]
		});
	}

	for (const id of ids) {
		var b = bMap[id];
		stylesheet.selector('#b' + id)
			.css({
				label: "data(name)",
				'background-image': imgBaseUrl + (id > 1000 ? 'group' : 'innbuilds/' + id) + '.gif'
			});
	}
	return stylesheet;
}

var cy = cytoscape({
  container: document.getElementById('cy'),

  boxSelectionEnabled: false,
  autounselectify: true,
  wheelSensitivity: 0.4,

  style: styles(cytoscape.stylesheet()),

  elements: getElements(buildingId),

  layout: layouts['dagre']
});

cy.on('tap', 'node', function(){
  var nodes = this;
  var tapped = nodes;
  var node = nodes[0];
  var id = parseInt(node.data().id.substring(1));
  var b = bMap[id];
  var cl = 'selected';
  if (_.includes(selected, id)) {
	node.removeClass(cl);
  } else {
	node.addClass(cl);
  }
  selected = _.xor(selected, [id]);
  updateCost();
});

cy.on('cxttap', 'node', function(){
  var nodes = this;
  var tapped = nodes;
  var food = [];
  var oId = nodes.data('oid');
  var expand = nodes.hasClass('eater');
  nodes.toggleClass('eater');

  if (expand) {
	getNodes(oId, true).style('display', 'element').removeClass('eater');
  } else {
	getNodes(oId, true).style('display', 'none');
  }
});


function makePopper(ele) {
	let ref = ele.popperRef();
	ele.tippy = tippy(ref, {
		content: () => {
			let content = document.createElement('div');
			let bId = ele[0].data('oid');
			var b = bMap[bId];
			content.innerHTML = b.description;
			return content;
		},
		theme: 'light left-align',
		trigger: 'manual'
	});
}

function updateTooltips() {
	cy.nodes().forEach(function(ele) {
		makePopper(ele);
	});

	cy.nodes().unbind('mouseover');
	cy.nodes().bind('mouseover', (e) => e.target.tippy.show());

	cy.nodes().unbind('mouseout');
	cy.nodes().bind('mouseout', (e) => e.target.tippy.hide());
}

updateTooltips();

tippy('#help', {
	theme: 'light left-align',
	content: $('#help-text').html(),
});