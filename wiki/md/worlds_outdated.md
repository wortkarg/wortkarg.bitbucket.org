### Богатый мир
ID: gold\_rich\_v1.0.0

Базовый доход золота увеличен в два раза


### Больной мир
ID: sick\_v1.0.0

Базовое здоровье бойцов уменьшено на 30%


### Быстрый мир
ID: fast\_v1.0.0

Скорость бойцов увеличена на 1


### Демонический мир
ID: demon\_v1.0.0

Много провинций с демонами. 
Увеличена вероятность выпадения следующих сайтов (вес в 20 раз): 
Алтарь Хаоса
Чёрный омут
Старый ветряк


### Добрый мир
ID: good\_v1.0.0

Злые бойцы становятся добрыми, т.е. все бойцы теперь добрые


### Злой мир
ID: evil\_v1.0.0

Добрые бойцы становятся злыми, т.е. все бойцы теперь злые


### Кристаллический мир
ID: gem\_rich\_v1.0.0

Базовый доход кристаллов увеличен в два раза


### Медленный мир
ID: slow\_v1.0.0

Скорость бойцов уменьшена на 1 (у тех, у кого она больше 1)


### Мир арены
ID: arena\_v1

Мир для тестирования героев и армий (например в хотсите). 
Игрок в начале игры получает доступ ко всем юнитам, предметам и заклинаниям, а также может получить сколько угодно опыта и денег/кристаллов (всё это доступно через ритуалы).
При отстройке в замке алтаря герой получает доступ к особым ритуалам. Можно использовать до 100 ритуалов в ход, начиная со следующего хода после отстройки алтаря.
Список ритуалов:

1) Создатель сайтов
Строит лавки в выбранной провинции. Опции: 
- Лавки с оружием и украшениями
- Лавки с одеждой, доспехами и заклинаниями
- Перестроить лавку знаний (если нужного заклинания не нашлось или они закончились)

В лавках есть все предметы соответствующего типа (кроме лавки знаний, где 108 случайных свитков, но эту лавку можно перестраивать). 

2) Вызов наёмников
Трехуровневый диалог для покупки юнитов и в конце стандартный найм одного из десяти типов юнитов (как на сайтах).
Юнитов прийдется выбирать по номерам, но номера можно посмотреть в Эадоропедии.

3) Вызов джина
Джин предлагает на выбор:
- Деньги и кристаллы
- Мобильность на ход (практически неограниченная)
- Излечение

4) Тренировка
Аналог "Боя с иллюзией", но с выбором противника (количества опыта). 200, 2000 или 20000 опыта соответственно.

Дополнительно:
- никаких магазинов в провинциях (только строить через ритуал)
- магазинный уровень и редкость всех предметов равны 1, нет зависимости от ресурсов
- цена юнитов не зависит от ресурсов
- охрана в лавке знаний всегда уровня 2 (гномы, эльфы, гоблины, орки, нежить и т.п.)
- 0 случайных сайтов в родовой провинции (место для магазинов и т.п.)
- в родовой провинции обязательно есть следующие сайты:
Драконье логово
Арена
Тайник Древних
Усыпальница
Оплот Света
Слияние Стихий
Алтарь Дракона
Мистическая башня
Чёрный замок
Твердыня Хаоса
Логово чудовищ
Гномья крепость
Логово Зверя
Конклав
Древнее святилище
Город крысолюдов
Башня Тьмы
Проклятый храм



### Мир артефактов
ID: artifacts\_v1.0.0

Редкость артефактов уменьшена на 2


### Мир артефактов
ID: artifacts\_v1.0.1

Редкость артефактов уменьшена на 2


### Мир без дроу
ID: no\_drow\_v1

Много лет назад один амбициозный правитель Дроу решил захватить весь осколок, но после кровопролитной войны потерпел поражение. Князь, возглавлявший победившую сторону и потерявший на войне всю семью, поклялся, что не успокоится, пока не истребит всех дроу до последнего. Ему понадобилось на это много лет, но он выполнил своё обещание. 

Эффекты:
Мир, в котором отсутствует раса "Дроу"

Совместимость: НГ 14.0331 - 16.0901


### Мир без крыс
ID: no\_rats\_v1.0.0

Мир, в котором отсутствует раса "Крысы"


### Мир без крыс
ID: no\_rats\_v1

На этом осколке крысы так и не смогли ужиться с остальными расами и были истреблены полностью.

Эффекты:
Мир, в котором отсутствует раса "Крысы"

Совместимость: НГ 14.0331 - 16.0901


### Мир дешевых стрелков
ID: cheap\_rangers\_v1.0.0

Цена юнитов с дистанционной атакой снижена на 30%, остальных увеличена на 20%


### Мир дешевых стрелков
ID: cheap\_rangers\_v1.0.1

Цена юнитов с дистанционной атакой снижена на 30%, остальных увеличена на 20%


### Мир командования
ID: command\_v1.0.0

Стоимость покупки и оплата бойцов уменьшается на 25%, а их прокачка становится на 30% быстрее


### Мир мощных стрелков
ID: power\_rangers\_v1.0.0

Дистанционная атака увеличена на 2


### Мир нежити
ID: undead\_v1.0.0

Много провинций с нежитью


### Мир нежити
ID: undead\_v1.0.1

Много провинций с нежитью. 
Увеличена вероятность выпадения следующих сайтов (вес в 20 раз): 
Древний склеп
Древние руины
Усыпальница
Чёрный замок
Старый особняк
Проклятый храм


### Мир нежити
ID: undead\_v1.0.2

Много провинций с нежитью. 
Увеличена вероятность выпадения следующих сайтов (вес в 20 раз): 
Древний склеп
Древние руины
Усыпальница
Чёрный замок
Старый особняк
Проклятый храм

Добавлен новый сайт (Кладбище) с новым юнитом (Посланник смерти)

В охрану типа Нежить добавлено по одному дополнительному юниту (кроме самой слабой охраны этого типа)


### Мир нежити
ID: undead\_v3

Когда магам нечего делать, они начинают войны. И хуже всего война между некромантом и мастером демонологии. В это мире недавно такая война отгремела. Оба мага пали. Вконец зарвавшегося демонолога уничтожил один из призванных им Разрушителей, а некромант выморил все свои земли, обратился в лича и рассылает посланников смерти на ещё неискаженные погосты собирать под свои знамена новую армию из зомби и скелетов. Он мнит себя единственным хозяином этого мира, и ваша задача доказать ему, что это не так.

Эффекты:
- Много провинций с нежитью. 
- Увеличена вероятность выпадения следующих сайтов: 
Древний склеп
Древние руины
Усыпальница
Чёрный замок
Старый особняк
Проклятый храм
- Добавлен новый сайт (Кладбище) с новым юнитом (Посланник смерти)
- В охрану типа Нежить добавлено по одному дополнительному юниту (кроме самой слабой охраны этого типа)


### Мир нежити
ID: undead\_v4

Когда магам нечего делать, они начинают войны. И хуже всего война между некромантом и мастером демонологии. В это мире недавно такая война отгремела. Оба мага пали. Вконец зарвавшегося демонолога уничтожил один из призванных им Разрушителей, а некромант выморил все свои земли, обратился в лича и рассылает посланников смерти на ещё неискаженные погосты собирать под свои знамена новую армию из зомби и скелетов. Он мнит себя единственным хозяином этого мира, и ваша задача доказать ему, что это не так.

Эффекты:
- Много провинций с нежитью. 
- Увеличена вероятность выпадения следующих сайтов: 
Древний склеп
Древние руины
Усыпальница
Чёрный замок
Старый особняк
Проклятый храм
- Добавлен новый сайт (Кладбище) с новым юнитом (Посланник смерти)
- В охрану типа Нежить добавлено по одному дополнительному юниту (кроме самой слабой охраны этого типа)

Совместимость: НГ 15.1231 - 16.0901


### Мир с перевернутой кармой
ID: inverted\_karma\_v1.0.0

У бойцов карма становится прямо противоположной (добрые становятся злыми и наоборот)


### Мир с перевернутой кармой
ID: inverted\_karma\_v1.0.1

У бойцов карма становится прямо противоположной 
(добрые становятся злыми и наоборот).
"Сокрушение зла" и "Тёмное превосходство" меняются местами


### Мир со случайной кармой
ID: random\_karma\_v1.0.0

Карма бойцов случайна


### Мир страха
ID: horror\_v1.0.0

Мораль всех бойцов занижена на 30%


### Мир трупоедов
ID: cannibal\_v1.0.0

У всех бойцов есть способность "трупоедство". У уровней 1-2 "трупоедство 5", 3+ "трупоедство 10"


### Нейтральный мир
ID: neutral\_v1.0.0

Все бойцы становятся нейтральными


### Религиозный мир
ID: inquisition\_v1.0.0

Много провинций с инквизицией (святые земли)


### Усталый мир
ID: tired\_v1.0.0

Базовая выносливость бойцов уменьшена на 30%


